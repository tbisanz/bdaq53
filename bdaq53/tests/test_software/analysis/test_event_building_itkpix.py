#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import unittest

import tables as tb
import numpy as np

import bdaq53
import bdaq53.analysis.rd53b_analysis as anb

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))


def get_hists(rawdata, ptot_table_stretched, trig_pattern=0b11111111111111111111111111111111, align_method=2):
    ''' Helper function to create hists important for event building'''

    data = anb.analyze_chunk(rawdata=rawdata,
                             return_hists=('rel_bcid',
                                           'event_status',
                                           'bcid_error'),
                             trig_pattern=trig_pattern,
                             align_method=align_method,
                             rx_id=3,
                             ptot_table_stretched=ptot_table_stretched)
    data['rel_bcid'] = data['rel_bcid'].sum(axis=(0, 1, 2))  # 3D -> 1D
    return data


class TestAnalysis(unittest.TestCase):
    @unittest.skip('Not working yet')
    def test_all_ok(self):
        ''' Test event building with good data '''
        raw_data_file = os.path.join(data_folder, 'digital_scan_ptot_few_hits.h5')

        n_hits = 400 * 384 * 2  # digital scan with two injections
        n_triggers = int(n_hits / 200)  # 200 pixels active per mask step
        with tb.open_file(raw_data_file) as in_file:
            raw_data = in_file.root.raw_data[:]
            ptot_table_stretched = np.array([(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1)], dtype=np.dtype(
                {'names': ['cmd_number_start', 'cmd_number_stop', 'cmd_length', 'scan_param_id', 'hit_or_1_col',
                           'hit_or_1_row', 'hit_or_2_col', 'hit_or_2_row', 'hit_or_3_col', 'hit_or_3_row',
                           'hit_or_4_col', 'hit_or_4_row'],
                 'formats': [np.uint32, np.uint32, np.uint32, np.uint32, np.uint16, np.uint16, np.uint16, np.uint16,
                             np.uint16, np.uint16, np.uint16, np.uint16]}))
            try:
                in_file.root._v_children['ptot_table']
                ptot_table = in_file.root.ptot_table[:]
                if len(ptot_table) > 0:
                    ptot_table_stretched = np.array(anb.stretch_ptot_data(ptot_table))
            except KeyError:
                ptot_table_stretched = np.array([-1])
            data = get_hists(raw_data, ptot_table_stretched)
            self.assertTrue(data['rel_bcid'][11] == n_hits)
            self.assertTrue(np.where(data['rel_bcid'] != 0)[0][0] == 11)
            self.assertTrue(data['event_status'][1] == n_triggers - 1)  # This fails, due to bug in storing of event status (?). Last event has wrong status
            self.assertFalse(np.any(data['bcid_error']))

    @unittest.skip('Not working yet')
    def test_event_header_missing(self):
        ''' Test analysis with missing event header(s).

            Parameters:
                - 32 consecutive Trigger

            Expectation:
                - The event is flagged to have a Trigger ID error & BCID error & Event structure error
                - The following event has a Trigger ID error
                - The relative BCID histogram is unaltered
                - Other events are OK
         '''
        raw_data_file = os.path.join(data_folder, 'digital_scan_ptot_few_hits.h5')
        n_hits = 400 * 384 * 2  # digital scan with two injections
        n_triggers = int(n_hits / 200)  # 200 pixels active per mask step

        # One event header missing
        with tb.open_file(raw_data_file) as in_file:
            # Delete event header
            raw_data = np.delete(in_file.root.raw_data[:], [13, 14, 15, 16])
            ptot_table_stretched = np.array([(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1)], dtype=np.dtype(
                {'names': ['cmd_number_start', 'cmd_number_stop', 'cmd_length', 'scan_param_id', 'hit_or_1_col',
                           'hit_or_1_row', 'hit_or_2_col', 'hit_or_2_row', 'hit_or_3_col', 'hit_or_3_row',
                           'hit_or_4_col', 'hit_or_4_row'],
                 'formats': [np.uint32, np.uint32, np.uint32, np.uint32, np.uint16, np.uint16, np.uint16, np.uint16,
                             np.uint16, np.uint16, np.uint16, np.uint16]}))
            try:
                in_file.root._v_children['ptot_table']
                ptot_table = in_file.root.ptot_table[:]
                if len(ptot_table) > 0:
                    ptot_table_stretched = np.array(anb.stretch_ptot_data(ptot_table))
            except KeyError:
                ptot_table_stretched = np.array([-1])
            data = get_hists(raw_data, ptot_table_stretched)
            self.assertTrue(data['rel_bcid'][11] == n_hits - 200)
            self.assertTrue(np.where(data['rel_bcid'] != 0)[0][0] == 11)
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, n_triggers - 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])

        # Three event header missing
        with tb.open_file(raw_data_file) as in_file:
            # Delete event header
            raw_data = np.delete(in_file.root.raw_data[:], [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24])
            ptot_table_stretched = np.array([(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1)], dtype=np.dtype(
                {'names': ['cmd_number_start', 'cmd_number_stop', 'cmd_length', 'scan_param_id', 'hit_or_1_col',
                           'hit_or_1_row', 'hit_or_2_col', 'hit_or_2_row', 'hit_or_3_col', 'hit_or_3_row',
                           'hit_or_4_col', 'hit_or_4_row'],
                 'formats': [np.uint32, np.uint32, np.uint32, np.uint32, np.uint16, np.uint16, np.uint16, np.uint16,
                             np.uint16, np.uint16, np.uint16, np.uint16]}))
            try:
                in_file.root._v_children['ptot_table']
                ptot_table = in_file.root.ptot_table[:]
                if len(ptot_table) > 0:
                    ptot_table_stretched = np.array(anb.stretch_ptot_data(ptot_table))
            except KeyError:
                ptot_table_stretched = np.array([-1])
            data = get_hists(raw_data, ptot_table_stretched)
            self.assertTrue(data['rel_bcid'][11] == n_hits - 200)
            self.assertTrue(np.where(data['rel_bcid'] != 0)[0][0] == 11)
            self.assertListEqual(data['event_status'].tolist(),
                                 [0, n_triggers - 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  0, 0])


if __name__ == '__main__':
    unittest.main()
