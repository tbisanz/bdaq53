#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
from os.path import dirname, join, abspath
import yaml
import unittest

import tables as tb
import numpy as np

from bdaq53.scans import test_registers

logger = logging.getLogger(__file__)


class TestScanScripts(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestScanScripts, cls).setUpClass()

        ''' Enable periphery and powercycle SCC '''
        with open(join(abspath(join(dirname(__file__), '..', '..')), 'testbench.yaml'), 'r') as tbf:
            tb = yaml.full_load(tbf)
        tb['periphery']['enable_periphery'] = True
        tb['modules']['module_0']['powersupply'] = {'lv_name': 'LV-1', 'lv_voltage': 1.21, 'lv_current_limit': 4.0}
        tb['modules']['module_0']['power_cycle'] = True
        tb['modules']['module_0']['chip_0']['chip_config_file'] = join(abspath(join(dirname(__file__), '..', '..', 'chips')), 'ITkPixV1_default.cfg.yaml')
        tb['modules']['module_0']['chip_0']['chip_sn'] = '0x0002'
        tb['modules']['module_0']['chip_0']['chip_id'] = 15
        tb['modules']['module_0']['chip_0']['chip_type'] = "itkpixv1"
        tb['modules']['module_0']['chip_0']['receiver'] = "rx0"

        with open(join(abspath(join(dirname(__file__), '..', '..')), 'testbench.yaml'), 'w') as tbf:
            yaml.dump(tb, tbf)

        with open(join(abspath(join(dirname(__file__), '..', '..')), 'periphery.yaml'), 'r') as tbf:
            tb = yaml.full_load(tbf)
        tb['transfer_layer'][0]['init']['port'] = '/dev/power_supply'
        logger.info(tb['registers'])
        tb['registers'][-1]['name'] = 'LV-1'
        tb['registers'][-1]['arg_add']['channel'] = 2
        # logger.info(tb['registers'])
        with open(join(abspath(join(dirname(__file__), '..', '..')), 'periphery.yaml'), 'w') as tbf:
            yaml.dump(tb, tbf)

        ''' Run this test first and do not assert anything, because for the first test after flashing a new firmware, there will never be aurora sync '''
        try:
            with test_registers.RegisterTest(scan_config=test_registers.scan_configuration) as test:
                test.configure()
                test.scan()
        except RuntimeError:
            pass

    def test_digital_scan(self):
        ''' Test digital scan '''
        from bdaq53.scans import scan_digital
        scan_digital.scan_configuration['stop_row'] = 384
        with scan_digital.DigitalScan(scan_config=scan_digital.scan_configuration) as scan:
            scan.start()
            filename = scan.output_filename + '_interpreted.h5'

        with tb.open_file(filename) as in_file:
            enable_mask = in_file.root.configuration_out.chip.use_pixel[:]
            # logger.error(np.count_nonzero((in_file.root.HistOcc[:].sum(axis=2)[enable_mask] != 100)))
            logger.info('Integrated occupancy: %s' % np.sum(in_file.root.HistOcc[:].sum(axis=2)[enable_mask]))
            self.assertTrue(np.all(in_file.root.HistOcc[:].sum(axis=2)[enable_mask] == 100))
            # FIXME: Skip these two for now until shift in BCID and ToT is unterstood
            self.assertTrue(np.all(in_file.root.HistBCIDError[:] == 0))

    def test_analog_scan(self):
        ''' Test analog scan '''
        from bdaq53.scans import scan_analog
        scan_analog.scan_configuration['stop_row'] = 384
        with scan_analog.AnalogScan(scan_config=scan_analog.scan_configuration) as scan:
            scan.start()
            filename = scan.output_filename + '_interpreted.h5'

        with tb.open_file(filename) as in_file:
            logger.error(np.count_nonzero((in_file.root.HistOcc[:].sum(axis=2) != 100)))
            self.assertTrue(np.count_nonzero(in_file.root.HistOcc[:].sum(axis=2) == 100) > 0.99 * 400 * 384)
            self.assertTrue(np.any(in_file.root.HistRelBCID[:]))
            self.assertTrue(np.any(in_file.root.HistTot[:]))

    def test_fast_threshold_scan(self):
        ''' Test fast threshold scan and results '''
        from bdaq53.scans.ITkPixV1 import scan_in_time_threshold_fast
        with scan_in_time_threshold_fast.FastThresholdScanInTime(scan_config=scan_in_time_threshold_fast.scan_configuration) as scan:
            scan.start()

    def test_register_test(self):
        ''' Test if registers writing and reading still works still work '''
        from bdaq53.scans import test_registers
        scan_configuration = {
            'ignore': ['PIX_PORTAL',
                       'GLOBAL_PULSE_ROUTE',
                       'SER_SEL_OUT',
                       'ServiceDataConf',
                       'DAC_CML_BIAS_2',
                       'DAC_CML_BIAS_1',
                       'DAC_CML_BIAS_0',
                       'SEU53']
        }
        with test_registers.RegisterTest(scan_config=scan_configuration) as test:
            test.start()

    def test_data_mergign(self):
        ''' Test if data merging firm and software still works '''
        from bdaq53.scans.ITkPixV1 import test_data_merging
        with test_data_merging.DataMergingTest() as test:
            test.start()
            result = test._analyze()
        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()
