#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan injects a digital pulse into
    enabled pixels to test the digital part of the chip.
'''

import os
import numpy as np
from numba import njit
import tables as tb
import bdaq53
from bdaq53.system.fifo_readout import FifoReadout, ReadoutChannel
from bdaq53.system.bdaq53_base import BDAQ53
from tqdm import tqdm
import time
from bdaq53.system.scan_base import FILTER_RAW_DATA, FILTER_TABLES, MetaTable

scan_configuration = {
    'spf_file': '/faust/user/mstandke/RD53_env/RD53B_SIM/bdaq53/bdaq53/waferprobing/itkpixv1_scans/rd53b_patterns_0end.spf'
}

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config_file = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))


class ScanChain():
    scan_id = 'scan_chain'

    def __init__(self, bdaq_conf=None):
        self.bdaq = BDAQ53(conf=bdaq_conf)
        self.bdaq.init()
        self.bdaq['system']['WAFER_PROBING'] = 1
        # self.card = Needlecard(self.bdaq['i2c'])
        # self.card.init()
        # self.card.enable_scan_chain_mode()
        # self.card.write_gpio_expander('CHIP_ID', 0)
        # self.card.write_gpio_expander('TEST', 1)
        # self.card.write_gpio_expander('BYPASS', 1)
        # # self.card.write_gpio_expander('BYPASS', 0)
        # self.card.write_gpio_expander('IREF_TRIM', 7)

    def _configure(self, chain_in_file='/faust/user/mstandke/Downloads/rd53b_patterns_0end', **_):
        output_filename = '/faust/user/mstandke/RD53_env/RD53B_SIM/bdaq53/bdaq53/waferprobing/itkpixv1_scans/rd53b_patterns'
        self.output_filename = output_filename + '.h5'
        self.h5_file = tb.open_file(output_filename + '.h5', mode='w', title=self.scan_id)

        # Create data nodes
        self.raw_data_earray = self.h5_file.create_earray(self.h5_file.root, name='raw_data', atom=tb.UIntAtom(),
                                                          shape=(0,), title='raw_data', filters=FILTER_RAW_DATA)
        self.meta_data_table = self.h5_file.create_table(self.h5_file.root, name='meta_data', description=MetaTable,
                                                         title='meta_data', filters=FILTER_TABLES)

        self.chain_in_file = chain_in_file
        with tb.open_file(chain_in_file + '.h5', 'r') as in_file:
            self.SC_IN0 = in_file.root.SC_IN0[:]
            self.SC_IN1 = in_file.root.SC_IN1[:]
            self.SC_OUT0 = in_file.root.SC_OUT0[:]
            self.SC_OUT_MASK0 = in_file.root.SC_OUT_MASK0[:]
            self.SC_OUT1 = in_file.root.SC_OUT1[:]
            self.SC_OUT_MASK1 = in_file.root.SC_OUT_MASK1[:]
            self.MC_IN = in_file.root.MC_IN[:]
            self.MC_IN_P = in_file.root.MC_IN_P[:]
            self.MC_OUT = in_file.root.MC_OUT[:]
            self.MC_OUT_MASK = in_file.root.MC_OUT_MASK[:]
            self.chain_length = int(in_file.root.SC_IN0.attrs['chain_len'])

        self.bdaq['cmd'].set_output_en(0)
        self.bdaq['scan_chain_rx40'].set_en(1)
        self.bdaq['scan_chain_rx160'].set_en(1)
        self.bdaq['spi_tx_40a'].set_en(1)
        self.bdaq['spi_tx_160a'].set_en(1)
        self.bdaq['scan_chain'].set_scan_chain_mode(val=1)
        self.bdaq['scan_chain'].STATIC_SIGNALS = int(str(str(1) + str(0) + str(0) + str(0) + str(0) + str(0)), 2)
        self.bdaq['scan_chain'].STATIC_SAMPLE = 1
        time.sleep(1)
        self.bdaq['scan_chain'].STATIC_SAMPLE = 0
        self.bdaq['scan_chain'].IN_SIG_INVERT = 0b1100
        self.bdaq['scan_chain'].OUT_SIG_INVERT = 0b11
        self.bdaq['scan_chain'].PULSEWIDTH = 7
        self.bdaq['sc_start_pulse_gen'].set_en(1)
        self.bdaq['sc_start_pulse_gen'].set_width(8)
        self.bdaq['sc_start_pulse_gen'].set_delay(1)
        self.bdaq['sc_start_pulse_gen'].set_repeat(1)

        print(self.bdaq['scan_chain'].IN_SIG_INVERT, self.bdaq['scan_chain'].OUT_SIG_INVERT)
        self.scan_param_id = 0
        self.rx_cnt = 0

    def _scan(self, **_):
        self.sequence = []
        self.load_unload([0x33] * int(self.chain_length / 8 + 10), [0x33] * int(self.chain_length / 8 + 10))
        self.fifo_readout = FifoReadout(self.bdaq)
        self.fifo_readout.attach_channel(ReadoutChannel(receiver='rx0', callback=self.handle_data, clear_buffer=False))
        self.fifo_readout.start(errback=False, reset_rx=False, reset_sram_fifo=False, no_data_timeout=False)
        self.sequence = []
        for cnt in tqdm(range(self.SC_IN0.shape[0] - 1)):
            chain0 = np.array([n for n in self.SC_IN0[cnt]])  # ^ 0xFF
            chain1 = np.array([n for n in self.SC_IN1[cnt]])  # chain f
            self.multiclock_capture(self.MC_IN[cnt], self.MC_IN_P[cnt])

            # self.bdaq['spi_tx_40a'].set_size(1)  # len(ex_data) * 8)
            # self.bdaq['spi_tx_160a'].set_size(1)  # len(ex_data) * 8)
            # self.bdaq['spi_tx_40a'].set_data([0])
            # self.bdaq['spi_tx_160a'].set_data([0])
            # self.bdaq['scan_chain'].MULT_CLK_CAP = int(
            #     str(str(0) + str(0) + str(0) + str(0) + str(0) + '1'), 2)
            # self.bdaq['scan_chain'].PULSE_STARTA = 1
            # self.bdaq['scan_chain'].PULSE_STARTA = 0
            # self.bdaq['scan_chain'].MULT_CLK_CAP = int(
            #     str(str(0) + str(0) + str(0) + str(0) + str(0) + '0'), 2)

            print("cnt", cnt, np.count_nonzero(self.SC_IN0[cnt]), np.count_nonzero(self.SC_IN1[cnt]))
            if np.count_nonzero(self.SC_IN0[cnt]) > 10:
                print(len(chain0), len(chain1))
                self.load_unload(chain0, chain1)
                self.scan_param_id += 1
                self.sequence.append(self.chain_length)
        self.fifo_readout.stop()
        self.meta_data_table.attrs.sequence = np.array(self.sequence)
        self.h5_file.close()

    def multiclock_capture(self, in_fixed, in_pulsed):
        '''
        "SCAN_IN_0(DATA40)" + "SCAN_IN_1(DATA160)" + "SCAN_EN" + "BYPASS_MODE" + "CHIP_ID1" +
        "CHIP_ID2" + "CHIP_ID3" + "PMTM" + "TEST_MODE" + "TEST_CLK40" +
        "TEST_CLK160"
        '''
        DATA160 = (in_fixed >> 9) & 0x1
        PULSE160 = (in_pulsed >> 0) & 0x1
        DATA40 = (in_fixed >> 10) & 0x1
        PULSE40 = (in_pulsed >> 1) & 0x1
        if DATA40 == 1:
            DATA40 = 255
        if DATA160 == 1:
            DATA160 = 255
        SET_EN = (in_fixed >> 8) & 0x1
        BYPASS_MODE = (in_fixed >> 7) & 0x1
        CHIP_ID1 = (in_fixed >> 6) & 0x1
        CHIP_ID2 = (in_fixed >> 5) & 0x1
        CHIP_ID3 = (in_fixed >> 4) & 0x1
        PMTM = (in_fixed >> 3) & 0x1
        TEST_MODE = (in_fixed >> 2) & 0x1
        if PULSE160:
            PULSE_CLK160 = 1
            SET_CLK160 = 0
        else:
            PULSE_CLK160 = 0
            SET_CLK160 = (in_fixed >> 0) & 0x1
        if PULSE40:
            PULSE_CLK40 = 1
            SET_CLK40 = 0
        else:
            PULSE_CLK40 = 0
            SET_CLK40 = (in_fixed >> 1) & 0x1
        """ TEST_MODE_OUT, BYPASS_OUT, CHIPID3_OUT, CHIPID2_OUT, CHIPID1_OUT, CHIPID0_PMTM_OUT"""
        self.bdaq['scan_chain'].STATIC_SIGNALS = int(str(str(TEST_MODE) + str(BYPASS_MODE) + str(CHIP_ID3) + str(CHIP_ID2) + str(CHIP_ID1) + str(PMTM)), 2)
        """ MULT_CLK_CAP_PLS_CLK_160 + MULT_CLK_CAP_PLS_CLK_40 + MULT_CLK_CAP_SET_CLK_160 + MULT_CLK_CAP_SET_CLK_40 + MULT_CLK_CAP_SET_EN + MULT_CLK_CAP_MODE"""
        self.sequence.append(16)
        self.bdaq['spi_tx_40a'].set_size(1)  # len(ex_data) * 8)
        self.bdaq['spi_tx_160a'].set_size(1)  # len(ex_data) * 8) multiclock_capture
        self.bdaq['spi_tx_40a'].set_data([DATA40])
        self.bdaq['spi_tx_160a'].set_data([DATA160])
        self.bdaq['scan_chain'].MULT_CLK_CAP = int(str(str(PULSE_CLK40) + str(PULSE_CLK160) + str(SET_CLK40) + str(SET_CLK160) + str(SET_EN) + '1'), 2)
        self.bdaq['scan_chain'].PULSE_STARTA = 1
        self.bdaq['scan_chain'].PULSE_STARTA = 0
        self.bdaq['scan_chain'].MULT_CLK_CAP = int(str(str(PULSE_CLK40) + str(PULSE_CLK160) + str(SET_CLK40) + str(SET_CLK160) + str(SET_EN) + '0'), 2)

    def load_unload(self, in_data0, in_data1):
        buf_len = 300
        start_idx = 0
        if in_data0[-1] > 0:
            in_data0[-1] = 0xff
        idx = buf_len
        in_data1_copy = np.zeros_like(in_data0)
        max_chain_len = np.max([len(in_data0), len(in_data1)])
        for i in range(len(in_data1_copy)):
            if i >= len(in_data1):
                in_data1_copy[i] = (in_data1[-1] & 0x01) << 7
                break
            if i == 0:
                in_data1_copy[i] = 0 + in_data1[i] >> 1
            else:
                in_data1_copy[i] = (in_data1[i - 1] & 0x1) * 2**8 + in_data1[i] >> 1
        print("aaaaaaaa", bin(in_data1[-1]), bin(in_data1_copy[-1]))
        if in_data1_copy[-1] > 0:
            in_data1_copy[-1] = 0xff
        print("aaaaaaaa", bin(in_data1[-1]), bin(in_data1_copy[-1]))
        self.bdaq['spi_tx_40a'].set_size(buf_len * 8)  # len(ex_data) * 8)
        self.bdaq['spi_tx_160a'].set_size(buf_len * 8)  # len(ex_data) * 8)
        for _ in range(max_chain_len):
            self.bdaq['spi_tx_40a'].set_data(in_data0[start_idx:idx])
            self.bdaq['spi_tx_160a'].set_data(in_data1_copy[start_idx:idx])

            self.bdaq['scan_chain'].PULSE_STARTA = 1
            self.bdaq['scan_chain'].PULSE_STARTA = 0

            start_idx = idx
            if (idx + buf_len) <= max_chain_len:
                idx += buf_len
            else:
                idx = max_chain_len
                last_bit_chain_len = self.chain_length - (start_idx * 8)
                print("hahsad", last_bit_chain_len)
                self.bdaq['spi_tx_40a'].set_size(last_bit_chain_len)
                self.bdaq['spi_tx_160a'].set_size(last_bit_chain_len)

                self.bdaq['spi_tx_40a'].set_data(in_data0[start_idx:idx])
                self.bdaq['spi_tx_160a'].set_data(in_data1_copy[start_idx:idx])

                self.bdaq['scan_chain'].PULSE_STARTA = 1
                self.bdaq['scan_chain'].PULSE_STARTA = 0
                break

    def handle_data(self, data_tuple, receiver=None):
        '''
            Handling of the data.
        '''
        total_words = self.raw_data_earray.nrows
        # print(self.bdaq['scan_chain_rx40'].LOST_COUNT, self.bdaq['scan_chain_rx160'].LOST_COUNT)
        self.raw_data_earray.append(data_tuple[0])
        # print([hex(i) for i in data_tuple[0]])
        self.raw_data_earray.flush()
        len_raw_data = data_tuple[0].shape[0]
        self.rx_cnt += len_raw_data
        self.meta_data_table.row['timestamp_start'] = data_tuple[1]
        self.meta_data_table.row['timestamp_stop'] = data_tuple[2]
        self.meta_data_table.row['error'] = data_tuple[3]
        self.meta_data_table.row['data_length'] = len_raw_data
        self.meta_data_table.row['index_start'] = total_words
        total_words += len_raw_data
        self.meta_data_table.row['index_stop'] = total_words
        self.meta_data_table.row['scan_param_id'] = self.scan_param_id

        self.meta_data_table.row.append()
        self.meta_data_table.flush()

    def analyze_chain_out(self, ana_file):
        with tb.open_file(ana_file, 'r') as in_file:
            raw_data = in_file.root.raw_data[:]
            # meta_data = in_file.root.meta_data[:]
            self.sequence = in_file.root.meta_data.attrs['sequence']
        out_arr_dim = (self.SC_OUT0.shape[0], self.SC_OUT0.shape[1])

        meta_data_f = np.zeros(int(len(raw_data)), dtype=np.dtype(
            {'names': ['start_idx', 'stop_idx', 'length', 'fw_cnt'],
             'formats': [np.uint32, np.uint32, np.uint32, np.uint32]}))
        meta_data_e = np.zeros(int(len(raw_data)), dtype=np.dtype(
            {'names': ['start_idx', 'stop_idx', 'length', 'fw_cnt'],
             'formats': [np.uint32, np.uint32, np.uint32, np.uint32]}))
        full_chain_f, meta_data_f = sort_raw(raw_data, meta_data_f, np.zeros(int(len(raw_data)), dtype=np.uint8), mask=0xf)

        full_chain_e, meta_data_e = sort_raw(raw_data, meta_data_e, np.zeros(int(len(raw_data)), dtype=np.uint8), mask=0xe)
        for f in meta_data_f:
            print("f", f)
        for e in meta_data_e:
            print("e", e)
        print("ffffffffffffffffffffffff")
        sc_f, mc_f = single_chain_analysis(full_chain_f, meta_data_f, np.zeros(self.SC_OUT0.shape[0], dtype=np.uint8), np.zeros(out_arr_dim, dtype=np.uint8), np.max(self.sequence))
        print("eeeeeeeeeeeeeeeeeeeeeeee")
        sc_e, mc_e = single_chain_analysis(full_chain_e, meta_data_e, np.zeros(self.SC_OUT0.shape[0], dtype=np.uint8), np.zeros(out_arr_dim, dtype=np.uint8), np.max(self.sequence))
        return sc_f, mc_f, sc_e, mc_e

    def analyze(self):
        SC1_out, MC1_out, SC0_out, MC0_out = self.analyze_chain_out(self.output_filename)
        true_array = []
        cnt_offset = 0
        failed_array = []
        for x in range(self.SC_OUT0.shape[0] - 1):
            print("fy", ''.join([format(i, '08b') for i in self.SC_IN0[x - 1, -4:]]))
            print("fx", ''.join([format(i, '08b') for i in SC0_out[x, -4:]]))
            print("fz", ''.join([format(i, '08b') for i in self.SC_OUT0[x, -4:]]))
            print("fm", ''.join([format(i, '08b') for i in self.SC_OUT_MASK0[x, -4:]]))
            print(format(MC0_out[x], '01b') + format(MC1_out[x], '01b'), format(self.MC_OUT[x], '02b'), format(self.MC_OUT_MASK[x], '02b'))
            # print("\n")
            print("ey", ''.join([format(i, '08b') for i in self.SC_IN1[x - 1, -4:]]))
            print("ex", ''.join([format(i, '08b') for i in SC1_out[x, -5:-1]]))
            print("ez", ''.join([format(i, '08b') for i in self.SC_OUT1[x, -4:]]))
            print("em", ''.join([format(i, '08b') for i in self.SC_OUT_MASK1[x, -4:]]))
            print("\n")
            # continue
            print("ffffffffffffffffffffffff", x)
            f = compare_measured_with_expected(SC0_out[x, :], self.SC_OUT0[x + cnt_offset, :], self.SC_OUT_MASK0[x + cnt_offset, :])
            print("eeeeeeeeeeeeeeeeeeeeeeee", x)
            e = compare_measured_with_expected(SC1_out[x, :], self.SC_OUT1[x + cnt_offset, :], self.SC_OUT_MASK1[x + cnt_offset, :])
            print(np.count_nonzero(SC0_out[x, :]), np.count_nonzero(self.SC_OUT0[x + cnt_offset, :]))
            print("e", x, e)
            print("f", x, f)
            print("\n")
            if e and f:
                true_array.append(True)
            else:
                true_array.append(False)
                failed_array.append(x)
            if true_array[-1] is False:
                print(x)
        print(true_array)
        '''
        "SCAN_IN_0(DATA40)" + "SCAN_IN_1(DATA160)" + "SCAN_EN" + "BYPASS_MODE" + "CHIP_ID1" +
        "CHIP_ID2" + "CHIP_ID3" + "PMTM" + "TEST_MODE" + "TEST_CLK40" +
        "TEST_CLK160"
        '''
        for xx, elem in enumerate(true_array):
            if not elem:
                print(xx, format(MC0_out[xx], '01b') + format(MC1_out[xx], '01b'), format(self.MC_OUT[xx], '02b'),
                      format(self.MC_OUT_MASK[xx], '02b'), format(self.MC_IN[xx], '011b'), format(self.MC_IN_P[xx], '02b'),
                      format(self.SC_OUT0[xx, 0], '08b'), format(self.SC_OUT1[xx, 0], '08b'), format(self.SC_IN0[xx, 0], '08b'), format(self.SC_IN1[xx, 0], '08b'))
        # print("\ngood")
        # for xx, elem in enumerate(true_array):
        #     if elem:
        #         print(format(xx, '02d'), format(MC0_out[xx], '01b') + format(MC1_out[xx], '01b'), format(self.MC_OUT[xx], '02b'),
        #               format(self.MC_OUT_MASK[xx], '02b'), format(self.MC_IN[xx], '011b'), format(self.MC_IN_P[xx], '02b'),
        #               format(self.SC_OUT0[xx, 0], '08b'), format(self.SC_OUT1[xx, 0], '08b'), format(self.SC_IN0[xx, 0], '08b'), format(self.SC_IN1[xx, 0], '08b'))
        # print(self.MC_IN[np.where(true_array)], self.MC_IN_P[np.where(true_array)])
        # print(np.count_nonzero(true_array))
        # print(len(true_array) - np.count_nonzero(true_array))
        # print(failed_array)


@njit
def sort_raw(raw_data, meta_data, out_data, mask=0xf):
    cnt = 0
    old_idx = 0
    meta_cnt = 0
    start_idx = 0
    for n in raw_data:
        if n >> 28 == mask:
            out_data[cnt] = (n >> 8) & 0xff
            cnt += 1
            out_data[cnt] = n & 0xff
            cnt += 1
            cur_idx = (n >> 16) & 0x3fff
            if cur_idx != old_idx:
                meta_data[meta_cnt]['start_idx'] = start_idx
                meta_data[meta_cnt]['stop_idx'] = cnt - 2
                meta_data[meta_cnt]['length'] = meta_data[meta_cnt]['stop_idx'] - meta_data[meta_cnt]['start_idx']
                meta_data[meta_cnt]['fw_cnt'] = old_idx
                start_idx = meta_data[meta_cnt]['stop_idx']
                meta_cnt += 1
            old_idx = cur_idx
    meta_data = meta_data[:meta_cnt]
    out_data = out_data[:cnt]
    return out_data, meta_data


# @njit
def single_chain_analysis(raw_chain, meta_data, mc_out, sc_out, sc_len):
    sc_cnt_x = 0
    sc_cnt_y = 0
    start_condition = False
    for elem in meta_data:
        if elem['length'] > 2:
            # print(sc_out.shape, sc_cnt_x, sc_cnt_x + elem['length'], elem['length'], elem['start_idx'], elem['stop_idx'])
            sc_out[sc_cnt_y, sc_cnt_x:sc_cnt_x + elem['length']] = raw_chain[elem['start_idx']:elem['stop_idx']]
            sc_cnt_x += elem['length']
            if sc_cnt_x * 8 > sc_len:
                remaining_bits = (sc_cnt_x * 8) - sc_len
                word16 = (sc_out[sc_cnt_y, -2] << 8) + sc_out[sc_cnt_y, -1]
                print("last_idx", remaining_bits, format((sc_out[sc_cnt_y, -4] << 8) + sc_out[sc_cnt_y, -3], '016b'), format(word16, '016b'))
                word16 = word16 << (remaining_bits)
                sc_out[sc_cnt_y, -2] = (word16 >> 8) & 0xff
                sc_out[sc_cnt_y, -1] = ((word16 & 0xff) >> remaining_bits) & 0xff
            start_condition = True
        elif start_condition:
            # sc_cnt_y += 1
            sc_cnt_x = 0
        if elem['length'] == 2 and start_condition:
            mc_out[sc_cnt_y] = raw_chain[elem['stop_idx'] - 1]
            sc_cnt_y += 1
    return sc_out, mc_out


# @njit
def compare_measured_with_expected(measured, expected, mask):
    output = True
    chain_len = len(expected)
    for cnt in range(chain_len):
        # Special treatment to ignore first two bits and last bit due to start-up issues
        # if cnt == 0 and measured[cnt] & mask[cnt] & 0x3f != expected[cnt] & mask[cnt] & 0x3f:
        #     output = False
        # elif cnt == chain_len and measured[cnt] & mask[cnt] & 0x01 != expected[cnt] & mask[cnt] & 0x01:
        #     output = False
        if measured[cnt] & mask[cnt] != expected[cnt] & mask[cnt]:
            print(cnt, bin(measured[cnt]), bin(expected[cnt]), bin(mask[cnt]), len(measured))
            output = False
            break
    return output


if __name__ == '__main__':
    scan = ScanChain()
    scan._configure()
    scan._scan()
    scan.analyze()
