import os
from os.path import join, isfile
import logging
from datetime import datetime
import tables as tb


WORKING_DIR = ''  # A waferprobing output directory with old format (all scan files in root, one sub directory named 'probing_wafer_XX)
DEBUG = False    # If DEBUG is True, no actions are actually performed

logging.basicConfig(level=logging.DEBUG if DEBUG else logging.INFO, format='%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s')

valid_chips = [(1, 6), (1, 7), (1, 8),
               (2, 3), (2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (2, 9), (2, 10),
               (3, 3), (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10), (3, 11),
               (4, 2), (4, 3), (4, 4), (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10), (4, 11), (4, 12),
               (5, 1), (5, 2), (5, 3), (5, 4), (5, 5), (5, 6), (5, 7), (5, 8), (5, 9), (5, 10), (5, 11), (5, 12),
               (6, 1), (6, 2), (6, 3), (6, 4), (6, 5), (6, 6), (6, 7), (6, 8), (6, 9), (6, 10), (6, 11), (6, 12),
               (7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (7, 7), (7, 8), (7, 9), (7, 10), (7, 11), (7, 12),
               (8, 2), (8, 3), (8, 4), (8, 5), (8, 6), (8, 7), (8, 8), (8, 9), (8, 10), (8, 11),
               (9, 3), (9, 4), (9, 5), (9, 6), (9, 7), (9, 8), (9, 9), (9, 10),
               (10, 5), (10, 6), (10, 7), (10, 8), (10, 9)]


def move_file(src, dst):
    logging.debug('Moving file {0} to {1}'.format(src, dst))
    if not DEBUG:
        os.rename(src, dst)
    else:
        if not isfile(src):
            raise OSError('No such file or directory: {}'.format(src))


if __name__ == '__main__':
    # Extract wafer SN
    wafer_sn = None
    for f in os.listdir(WORKING_DIR):
        if 'probing_wafer_' in f:
            analog_dir = f
            wafer_sn = int(f.split('_')[-1])
            break

    if wafer_sn is None:
        raise RuntimeError('Could not determine wafer SN! Are you sure this wafer probing directory is of old format?')

    # Create new directory structure
    chip_ids = []
    for pos in valid_chips:
        chip_sn = '0x{0:02X}{1:01X}{2:01X}'.format(wafer_sn, pos[0], pos[1])
        chip_ids.append(chip_sn)

        logging.debug('Creating chip subdir ' + join(WORKING_DIR, chip_sn))
        if not DEBUG:
            os.mkdir(join(WORKING_DIR, chip_sn))

    # Some files can only be assigned by timestamp. Get all chip tim spans from log file
    try:
        with open(join(WORKING_DIR, join(analog_dir, 'wafer_{0:02d}.log'.format(wafer_sn))), 'r') as log_f:
            wafer_log = list(log_f)
    except FileNotFoundError:
        with open(join(WORKING_DIR, join(analog_dir, 'probing_wafer_{0:02d}.log'.format(wafer_sn))), 'r') as log_f:
            wafer_log = list(log_f)

    chip_times = {}
    for line in wafer_log:
        if 'Start probing chip' in line:
            chip_id = line.strip()[-6:]
            chip_start_timer = datetime.strptime(line.split(' - ')[0], '%Y-%m-%d %H:%M:%S,%f')

        if 'Done! Probing this chip took' in line:
            chip_times[chip_id] = (chip_start_timer, datetime.strptime(line.split(' - ')[0], '%Y-%m-%d %H:%M:%S,%f'))

    # Sort scan files into subdirs
    for f in os.listdir(WORKING_DIR):
        if not isfile(join(WORKING_DIR, f)):
            continue

        if f[-3:] == '.h5' and '_interpreted.h5' not in f and '_probing_results.h5' not in f and 'mask' not in f:
            try:
                with tb.open_file(join(WORKING_DIR, f), 'r') as tf:
                    run_config_enc = dict(tf.root.configuration.run_config[:])
            except tb.exceptions.HDF5ExtError:
                logging.error('Error opening file {}'.format(f))
                continue

            run_config = {key.decode('utf-8'): value.decode('utf-8') for key, value in run_config_enc.items()}

            run_name = run_config['run_name']
            try:
                chip_sn = run_config['chip_id']
            except KeyError:
                chip_sn = run_config['chip_sn']

            files = [run_name + '.h5',
                     run_name + '.log']
            if 'register_test' not in run_name:
                files.append(run_name + '_interpreted.h5')
                files.append(run_name + '_interpreted.pdf')
            for mf in files:
                if not isfile(join(WORKING_DIR, mf)):
                    logging.warning('No such file: {}'.format(mf))
                    continue

                move_file(join(WORKING_DIR, mf), join(WORKING_DIR, join(chip_sn, mf)))

        # Handle orphaned log files
        elif f[-4:] == '.log' and 'analysis.log' not in f:
            log_timer = datetime.strptime('_'.join(f.split('_')[:2]), '%Y%m%d_%H%M%S')

            for chip_sn, times in chip_times.items():
                if log_timer >= times[0] and log_timer <= times[1]:
                    move_file(join(WORKING_DIR, f), join(WORKING_DIR, join(chip_sn, f)))
                    break
            else:
                logging.error('Cannot assign orphaned logfile {}'.format(f))

        # Handle mask files
        elif f[-8:] == '_mask.h5':
            mask_timer = datetime.strptime('_'.join(f.split('_')[:2]), '%Y%m%d_%H%M%S')

            for chip_sn, times in chip_times.items():
                if mask_timer >= times[0] and mask_timer <= times[1]:
                    move_file(join(WORKING_DIR, f), join(WORKING_DIR, join(chip_sn, f)))
                    break
            else:
                logging.error('Cannot assign mask file {}'.format(f))

    # Move wafer log file to root dir
    try:
        move_file(join(WORKING_DIR, join(analog_dir, 'wafer_{0:02d}.log'.format(wafer_sn))), join(WORKING_DIR, 'probing_wafer_{0:01d}.log'.format(wafer_sn)))
    except OSError:
        move_file(join(WORKING_DIR, join(analog_dir, 'probing_wafer_{0:02d}.log'.format(wafer_sn))), join(WORKING_DIR, 'probing_wafer_{0:01d}.log'.format(wafer_sn)))

    # Sort analog files into subdirs
    for f in os.listdir(join(WORKING_DIR, analog_dir)):
        if f[-5:] == '.yaml':
            chip_sn = f.split('.')[0].split('_')[-1]
            move_file(join(WORKING_DIR, join(analog_dir, f)), join(WORKING_DIR, join(chip_sn, 'analog_data_{0}.yaml'.format(chip_sn))))

        if f[-4:] == '.pdf':
            chip_sn = f.split('.')[0].split('_')[-1]
            move_file(join(WORKING_DIR, join(analog_dir, f)), join(WORKING_DIR, join(chip_sn, 'regulator_iv_curves.pdf')))

    # Rename analysis files
    analysis_files = {'wafer_{0:02d}_analysis.log'.format(wafer_sn): 'probing_wafer_{0:02d}_analysis.log'.format(wafer_sn),
                      'wafer_{0:02d}_probing_results.h5'.format(wafer_sn): 'probing_wafer_{0:02d}_results.h5',
                      'wafer_{0:02d}_probing_results.pdf'.format(wafer_sn): 'probing_wafer_{0:02d}_results.pdf'}
    for af, new_af in analysis_files.items():
        try:
            move_file(af, new_af)
        except OSError:
            pass

    # Cleanup
    try:
        logging.debug('Removing {}'.format(join(WORKING_DIR, 'last_scan.pdf')))
        if not DEBUG:
            os.remove(join(WORKING_DIR, 'last_scan.pdf'))
    except OSError:
        pass

    analog_path = join(WORKING_DIR, analog_dir)
    logging.debug('Removing directory {}'.format(analog_path))
    if not DEBUG:
        if len(os.listdir(analog_path)) == 0:
            os.rmdir(analog_path)
        else:
            raise RuntimeError('Analog dir is not empty! Please check.')

    logging.info('All done!')
