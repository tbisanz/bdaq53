from basil.dut import Dut
import bdaq53
import os

ps_path = os.path.join(os.path.dirname(bdaq53.__file__), 'waferprobing/probestation.yaml')
devices = Dut(ps_path)
devices.init()
prober = devices['Prober']

# Test waferprober commands
prober.separate()
# prober.goto_die(9, 1)
# prober.goto_die(1, 7)
# prober.goto_next_die()
print(prober.get_die())
# prober.contact()
