
from bdaq53.waferprobing.probe_chip_RD53A import WaferprobingPeriphery as WaferprobingPeripheryA
from bdaq53.waferprobing.needlecard_RD53B import Needlecard as NeedlecardB

from bdaq53.scans.test_registers import RegisterTest
from bdaq53.scans.scan_pixel_registers import PixelRegisterScan
from bdaq53.scans.scan_digital import DigitalScan
from bdaq53.scans.scan_analog import AnalogScan
from bdaq53.scans.scan_threshold_fast import FastThresholdScan
from bdaq53.scans.ITkPixV1.test_data_merging import DataMergingTest

from bdaq53.waferprobing.itkpixv1_scans.scan_ring_osc import OSCScan
from tqdm import tqdm
from bdaq53 import utils as bu

import time
import copy
from bdaq53.chips import ITkPixV1
import logging
import os
import bdaq53
import yaml
import numpy as np
import itkpix_efuse_codec


bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config_file = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))
default_cfg = os.path.abspath(os.path.join(bdaq53_path, 'chips/ITkPixV1_default.cfg.yaml'))

default_register = os.path.abspath(os.path.join(bdaq53_path, 'chips/ITkPixV1_registers.yaml'))
skip_reg = []
with open(default_register) as f:
    registers = yaml.safe_load(f)
    for n in registers['registers']:
        if 'LIN' not in n['name']:
            skip_reg.append(n['name'])

CHIP_SN = '0x0000'  # Chip SN for calling this script standalone

LOGLEVEL = logging.INFO

configuration = {
    # Powering settings
    'powering': {
        'VAUX1': 1.2,       # Default VDD_PLL
        'VAUX2': 1.2,       # Default VDD_CML
        'LDO_VINA': 1.6,    # Default analog voltage in LDO mode
        'LDO_VIND': 1.6,    # Default digital voltage in LDO mode
        'LDO_IINA': 1.6,    # Analog current limit in LDO mode
        'LDO_IIND': 1.6,    # Digital current limit in LDO mode
        'SHUNT_VINA': 1.6,  # Analog voltage limit in Shunt mode
        'SHUNT_VIND': 1.6,  # Digital voltage limit in Shunt mode
        'SHUNT_IINA': 1.6,  # Default analog working point in Shunt mode
        'SHUNT_IIND': 1.6   # Default digital working point in Shunt mode
    },

    # Analog measurements to be conducted using the external MUX
    'external_mux_measurements': [
        'VINA',
        'VIND',
        'VDDA',
        'VDDD',
        'VREF_ADC_OUT',
        'SLDO_VREFA',
        'SLDO_VREFD'
    ],

    # Analog measurements to be conducted using the internal MUX
    'internal_mux_measurements': [
        'VIN_Ana_SLDO',
        'VOUT_Ana_SLDO',
        'VREF_Ana_SLDO',
        'VOFF_Ana_SLDO',
        'VIN_Dig_SLDO',
        'VOUT_Dig_SLDO',
        'VREF_Dig_SLDO',
        'VOFF_Dig_SLDO'
    ],

    # High level scan configurations
    'register_test': {
        'ignore': ['PIX_PORTAL',
                   'GLOBAL_PULSE_ROUTE',
                   'SER_SEL_OUT',
                   'ServiceDataConf',
                   'DAC_CML_BIAS_2',
                   'DAC_CML_BIAS_1',
                   'DAC_CML_BIAS_0',
                   'SEU53']
    },
    'aurora_register_test': {
        'ignore': skip_reg
    },
    'digital_scan': {
        'start_column': 0,
        'stop_column': 400,
        'start_row': 0,
        'stop_row': 384,
    },
    'aurora_test': {
        'start_column': 0,
        'stop_column': 400,
        'start_row': 0,
        'stop_row': 8,
    },
    'analog_scan': {
        'start_column': 0,
        'stop_column': 400,
        'start_row': 0,
        'stop_row': 384,

        'VCAL_MED': 500,
        'VCAL_HIGH': 2000,
    },
    'threshold_scan': {
        'start_column': 0,
        'stop_column': 400,
        'start_row': 0,
        'stop_row': 384,

        'VCAL_MED': 500,
        'VCAL_HIGH_start': 500,
        'VCAL_HIGH_stop': 2000,
        'VCAL_HIGH_step': 100
    }
}


class WaferprobingPeriphery(WaferprobingPeripheryA):

    def measure_iref(self, iref_trim, offset=0., fast=False):
        self.needle_card.write_gpio_expander('IREF_TRIM', iref_trim, fast=fast)
        self.needle_card.set_adc_mux('IMUX_OUT', fast=fast)
        self.needle_card.write_gpio_expander('EN_R_IREF', 1, fast=fast)
        time.sleep(0.01)
        iref_v = self.get_voltage_from_external_mux('VOFS', offset=offset, fast=fast)
        # resistance = 26145  # 85840 84500 Resistors VREF_ADC
        r = self.needle_card._eeprom_storage_map['RIREF'][0]
        if not fast:
            time.sleep(0.1)
        try:
            iref = (iref_v / r) / 5  # 2.5
        except ValueError as e:
            self.logger.exception(e)
            iref = 0.0
        return iref, offset, iref_v

    def get_voltage_from_external_mux(self, mux, samples=1, offset=0., fast=False):
        self.needle_card.set_adc_mux(mux, fast=fast)
        time.sleep(0.1)
        self._devices['smu'].set_current(0)
        self._devices['smu'].source_current()
        try:
            self._devices['smu'].on()
            time.sleep(0.1)
            vals = []
            for _ in range(samples):
                vals.append(float(self._devices['smu'].get_voltage().split(',')[0]))
            value = float(np.mean(vals)) - offset
        except ValueError as e:
            self.logger.exception(e)
            value = 0.0
        self._devices['smu'].off()
        self.needle_card.set_adc_mux('GND', fast=fast)
        return value

    def get_current_from_external_mux(self, mux, samples=1, offset=0.0, wait_time=0.1, set_gnd=True, fast=False):
        self.needle_card.set_adc_mux(mux, fast=fast)
        # Source reference voltage
        self._devices['smu'].source_volt()
        self._devices['smu'].set_voltage_range(2)
        self._devices['smu'].set_voltage(offset)
        cur_all = 0
        for n in range(samples):
            self._devices['smu'].on()
            time.sleep(wait_time)
            try:
                cur = float(self._devices['smu'].get_current().split(',')[1])
            except ValueError as e:
                self.logger.exception(e)
                cur = 0.0
            cur_all += cur
            self._devices['smu'].off()
        if set_gnd:
            self.needle_card.set_adc_mux('GND', fast=fast)
        return cur_all / samples

    def set_external_voltage(self, voltage):
        self._devices['smu'].source_volt()
        # self._devices['smu'].set_voltage_range(2)
        self._devices['smu'].set_voltage(voltage)

    def get_ext_curr_helper(self):
        self._devices['smu'].get_current()

    def smu_on(self):
        self._devices['smu'].on()

    def smu_off(self):
        self._devices['smu'].off()


class ChipProber(object):
    def __init__(self, chip_sn, configuration, working_dir, external_logfile_handlers=[], restart_bdaq=False, chip_config_file=None, probe_location='', testbench=None):
        self.chip_sn = chip_sn
        self.config = configuration
        self.working_dir = working_dir
        self.direct_working_dir = os.path.join(working_dir, 'module_0', self.chip_sn)
        self.probe_location = probe_location
        if not os.path.exists(self.direct_working_dir):
            os.makedirs(self.direct_working_dir)
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.external_logfile_handlers = external_logfile_handlers
        self.gndd_offset = 0
        self.gnda_offset = 0
        self.iref_trim = 7
        self.VrefA_opt = 7
        self.VrefD_opt = 7

        if testbench is None:
            with open(os.path.join(self.proj_dir, "waferprobing", 'testbench_ITkPixV1.yaml'), 'r') as f:
                self.testbench = yaml.safe_load(f)
        else:
            self.testbench = copy.deepcopy(testbench)

        self.meta_data = {'duration': {}, 'POWER': {}}
        self.general_information = {'configuration': configuration}

        if chip_config_file is None:
            chip_config_file = os.path.join(self.proj_dir, os.path.join('chips', 'ITkPixV1_default.cfg.yaml'))
        self.chip_config_file = chip_config_file
        with open(chip_config_file) as f:
            self.chip_config = yaml.safe_load(f)
            self.chip_config['chip_sn'] = self.chip_sn
            self.chip_config['reset_tot_latches'] = False

        logfile = os.path.join(self.direct_working_dir, 'probing_chip_' + self.chip_sn + '.log')
        self.fh = logging.FileHandler(logfile)
        self.fh.setLevel(logging.INFO)
        self.fh.setFormatter(logging.Formatter("%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s"))

        self.running_analysis_processes = []

        self.logger = logging.getLogger('Prober_{0}'.format(self.chip_sn))
        self.logger.setLevel(LOGLEVEL)
        self.logger.success = lambda msg, *args, **kwargs: self.logger.log(logging.SUCCESS, msg, *args, **kwargs)
        self.logger.notice = lambda msg, *args, **kwargs: self.logger.log(logging.NOTICE, msg, *args, **kwargs)
        self.logger.addHandler(self.fh)
        for handler in self.external_logfile_handlers:
            self.logger.addHandler(handler)

        logging.getLogger('Waferprobing').addHandler(self.fh)

        self.outfile_name = os.path.join(self.direct_working_dir, '_'.join(('analog_data', self.chip_sn)) + '.yaml')
        self.outfile_meta_file = os.path.join(self.direct_working_dir, '_'.join(('meta_data', self.chip_sn)) + '.yaml')

        self.periphery = WaferprobingPeriphery(configuration=self.config['powering'], restart_bdaq=restart_bdaq, Needlecard=NeedlecardB)
        self.periphery.logger.addHandler(self.fh)
        for handler in self.external_logfile_handlers:
            self.periphery.logger.addHandler(handler)

        # Define some general information
        self.general_information['DAQ_fw_version'] = self.periphery.bdaq.fw_version
        self.general_information['DAQ_aurora_lanes'] = self.periphery.bdaq.rx_lanes
        if self.periphery.bdaq['system']['AURORA_RX_640M']:
            speed = '640MBit/s'
        else:
            speed = '1280MBit/s'
        self.general_information['DAQ_rx_speed'] = speed
        pc_config_res = {}
        for keys, values in self.periphery.needle_card._eeprom_storage_map.items():
            pc_config_res[keys] = values[0]
        self.meta_data['PC_Resistors'] = pc_config_res
        self.dump_meta_data('RESISTORS', self.meta_data)
        self.chip = ITkPixV1.ITkPixV1(self.periphery.bdaq, chip_sn=self.chip_sn, chip_id=15, config=self.chip_config)
        logging.getLogger('ITkPixV1').addHandler(self.fh)
        for handler in self.external_logfile_handlers:
            logging.getLogger('ITkPixV1').addHandler(handler)

    def close(self):
        if hasattr(self, "periphery"):
            self.periphery.close()
            del self.periphery

        self.fh.close()
        for lg in logging.Logger.manager.loggerDict.values():
            if isinstance(lg, logging.Logger):
                lg.removeHandler(self.fh)
        self.logger.handlers = []

    ''' HELPER METHODS '''

    def get_meta_data(self):
        if len(self.meta_data.keys()) == 0:
            raise RuntimeError('Meta data is only available after running main().')
        return self.meta_data

    def get_general_information(self):
        return self.general_information

    def dump_analog_data(self, name, data):
        try:
            with open(self.outfile_name, 'r') as yamlfile:
                # results = yaml.safe_load(yamlfile)
                results = yaml.safe_load(yamlfile)
                results[name] = data
        except (IOError, TypeError):
            results = {}
            results[name] = data
        with open(self.outfile_name, 'w') as yamlfile:
            yaml.dump(results, yamlfile)

    def dump_meta_data(self, name, data):
        try:
            with open(self.outfile_meta_file, 'r') as yamlfile:
                results = yaml.safe_load(yamlfile)
                results[name] = data
        except (IOError, TypeError):
            results = {}
            results[name] = data
        with open(self.outfile_meta_file, 'w') as yamlfile:
            yaml.dump(results, yamlfile)

    def get_voltage_from_internal_mux(self, mux, samples=1, offset=0., fast=False):
        if type(mux) == str:
            mux = self.chip.voltage_mux[mux]

        self.chip.registers['MonitorConfig'].write(0x1000 + mux)
        time.sleep(0.1)
        value = self.periphery.get_voltage_from_external_mux('VMUX_OUT', samples=samples, offset=offset, fast=fast)
        return value

    def get_current_from_internal_mux(self, mux, samples=1, offset=0., wait_time=0.1, set_gnd=True, fast=False):
        if type(mux) == str:
            mux = self.chip.current_mux[mux]

        self.chip.registers['MonitorConfig'].write(0x1000 + (mux << 6))
        time.sleep(0.1)
        value = self.periphery.get_current_from_external_mux('IMUX_OUT', samples=samples, offset=offset, wait_time=wait_time, set_gnd=set_gnd, fast=fast)
        return value

    def get_voltage_offsets(self, samples=10, fast=False):
        timestamp_start_test = time.time()
        self.gndd_offset = self.periphery.get_voltage_from_external_mux('GNDD_REF', samples=samples, offset=0, fast=fast)
        self.gnda_offset = self.periphery.get_voltage_from_external_mux('GNDA_REF', samples=samples, offset=0, fast=fast)
        self.meta_data['POWER']['gnd_offset_ldo'] = self.initial_power(fast=fast)
        self.meta_data['duration']['gnd_offset_ldo'] = time.time() - timestamp_start_test
        return self.gndd_offset, self.gnda_offset

    def initial_power(self, dump=False, internals=True, fast=False):
        timestamp_start_test = time.time()
        init_power_results = {}
        if internals:
            vaofs = self.periphery.get_voltage_from_external_mux('GNDA_REF', samples=2, offset=0, fast=fast)
            vdofs = self.periphery.get_voltage_from_external_mux('GNDD_REF', samples=2, offset=0, fast=fast)
            init_vdda = self.periphery.get_voltage_from_external_mux('VDDA', samples=1, offset=vaofs, fast=fast)
            init_vddd = self.periphery.get_voltage_from_external_mux('VDDD', samples=1, offset=vdofs, fast=fast)
            init_power_results['VDDA'] = init_vdda
            init_power_results['VDDD'] = init_vddd
        ia, id = self.periphery.get_chip_current()
        vina, vind = self.periphery.get_chip_voltage()
        init_power_results['VINA'] = vina
        init_power_results['VIND'] = vind
        init_power_results['IINA'] = ia
        init_power_results['IIND'] = id
        if dump:
            self.dump_analog_data('initial_power', init_power_results)
        try:
            self.meta_data['duration']['initial_power'] += time.time() - timestamp_start_test
        except KeyError:
            self.meta_data['duration']['initial_power'] = time.time() - timestamp_start_test
        return init_power_results

    def trim_IREF(self, target_value=1, fast=False):
        timestamp_start_test = time.time()
        postfix = ' (' + self.periphery._powering_mode + ')'
        self.logger.info('Start IREF trimming routine...')
        iref_results = {}
        diffs = {}
        self.write_before_aurora('MonitorConfig', 0)
        # self.chip.init_communication()
        for iref_trim in range(16):
            iref_results[iref_trim] = {}
            vaofs = self.periphery.get_voltage_from_external_mux('GNDA_REF', samples=1, offset=0, fast=fast)
            if not fast:
                time.sleep(0.1)
            iref, v_iref, iref_raw = self.periphery.measure_iref(iref_trim, offset=vaofs, fast=fast)
            # internal_iref = abs(self.get_current_from_internal_mux('IREF', offset=vaofs))
            # print(iref, internal_iref, 2 * iref_raw, 2 * iref_raw/internal_iref)
            self.logger.info("iref: " + str(iref_trim) + ' ' + str(iref))  # + ' ' + str(internal_iref) + ' ' + str(iref_raw/(internal_iref*5)))
            iref_results[iref_trim]['V_IREF'] = v_iref
            iref_results[iref_trim]['IREF'] = iref
            if not fast:
                iref_results[iref_trim]['REXTA'] = self.periphery.get_voltage_from_external_mux('REXTA', offset=vaofs)
                iref_results[iref_trim]['REXTD'] = self.periphery.get_voltage_from_external_mux('REXTD', offset=vaofs)
                iref_results[iref_trim]['VINA'] = self.periphery.get_voltage_from_external_mux('VINA', offset=vaofs)
                iref_results[iref_trim]['VIND'] = self.periphery.get_voltage_from_external_mux('VIND', offset=vaofs)
                iref_results[iref_trim]['VOFS'] = self.periphery.get_voltage_from_external_mux('VOFS', offset=vaofs)
                # mux = self.chip.current_mux['IREF']
                # self.write_before_aurora('MonitorConfig', 0x1000 + (mux << 6))
                # iref_results[iref_trim]['IREF_INT'] = abs(self.get_current_from_internal_mux('IREF', offset=vaofs))
            diffs[iref_trim] = abs(target_value - 2 * iref_raw)
        IREF_opt = min(diffs, key=diffs.get)
        self.periphery.needle_card.write_gpio_expander('IREF_TRIM', IREF_opt, fast=fast)
        iref_results['IREF_OPT'] = IREF_opt
        self.dump_analog_data('IREF Trims' + postfix, iref_results)
        self.logger.success('Optimal IREF_TRIM is {0}'.format(IREF_opt))
        self.meta_data['POWER']['iref_trim' + postfix] = self.initial_power(fast=fast)
        self.meta_data['duration']['iref_trim' + postfix] = time.time() - timestamp_start_test
        return min(diffs, key=diffs.get)

    def write_before_aurora(self, register, value, trys=1):
        repetitions = 1000
        for _ in range(trys):
            self.periphery.bdaq.set_chip_type_ITkPixV1()
            self.periphery.bdaq.disable_auto_sync()
            self.chip._write_reset(write=True, repetitions=repetitions)
            self.chip.write_sync_01(write=True, repetitions=repetitions)
            self.chip.write_sync(write=True, repetitions=repetitions * 2)
            self.periphery.bdaq.enable_auto_sync()
            self.chip.write_ecr()
            self.chip.registers['GCR_DEFAULT_CONFIG'].write(0xac75)  # Write Magic numbers to disable constant reset
            self.chip.registers['GCR_DEFAULT_CONFIG_B'].write(0x538a)  # Write Magic numbers to disable constant reset:
            self.chip.registers[register].write(value)

    def trim_VREF(self, target_value=1.205, fast=False):
        # TODO: implement fast trim routine
        timestamp_start_test = time.time()
        postfix = ' (' + self.periphery._powering_mode + ')'
        self.logger.info('Start VREF trimming routine...')

        VrefD_results, VrefA_results = {}, {}
        diffs_D, diffs_A = {}, {}
        self.write_before_aurora('VOLTAGE_TRIM', 15)
        for Vref_val in range(15, -1, -1):
            VrefD_results[Vref_val], VrefA_results[Vref_val] = {}, {}
            # self.chip.write_sync(write=True, repetitions=repetitions)
            self.chip.registers['VOLTAGE_TRIM'].write(int('00' + format(Vref_val, '04b') + format(Vref_val, '04b'), 2))
            time.sleep(0.2)
            vaofs = self.periphery.get_voltage_from_external_mux('GNDA_REF', samples=1, offset=0, fast=fast)
            vaofs_internal = self.get_voltage_from_internal_mux('GNDA20', samples=1, fast=fast)
            VDDD = self.periphery.get_voltage_from_external_mux('VDDD', offset=vaofs, fast=fast)  # samples=10, offset=self.gndd_offset)
            VDDD_internal = self.get_voltage_from_internal_mux('VDDD_HALF', offset=vaofs_internal, fast=fast) * 2
            # VrefD_results[Vref_val]['VREF_D'] = self.periphery.get_voltage_from_external_mux('SLDO_VREFD', offset=self.gndd_offset)
            # VrefD_results[Vref_val]['VOFF_D'] = self.get_voltage_from_internal_mux('VOFF_Ana_SLDO')
            VrefD_results[Vref_val]['VDDD'] = VDDD
            VrefD_results[Vref_val]['VDDD_internal'] = VDDD_internal
            diffs_D[Vref_val] = abs(target_value - VDDD)
            VDDA = self.periphery.get_voltage_from_external_mux('VDDA', offset=vaofs, fast=fast)  # offset=self.gnda_offset)
            VDDA_internal = self.get_voltage_from_internal_mux('VDDA_HALF', offset=vaofs_internal, fast=fast) * 2
            # VrefA_results[Vref_val]['VREF_A'] = self.periphery.get_voltage_from_external_mux('SLDO_VREFA', offset=self.gnda_offset)
            # VrefA_results[Vref_val]['VOFF_A'] = self.get_voltage_from_internal_mux('VOFF_Dig_SLDO')
            VrefA_results[Vref_val]['VDDA'] = VDDA
            VrefA_results[Vref_val]['VDDA_internal'] = VDDA_internal
            diffs_A[Vref_val] = abs(target_value - VDDA)
            self.logger.info(str(Vref_val) + " " + str(VDDD) + " " + str(VDDA))

        vddds = np.diff([VrefD_results[i]['VDDD'] for i in range(16)])
        vddas = np.diff([VrefA_results[i]['VDDA'] for i in range(16)])
        if abs(VrefD_results[15]['VDDD'] - VrefD_results[0]['VDDD']) > 0.05:
            VrefD_opt = min(diffs_D, key=diffs_D.get)
            if VrefD_opt < len(vddds):
                if vddds[VrefD_opt] < 0.01:
                    VrefD_opt = (np.where(vddds == np.max(vddds))[0][0] + 2) if (np.where(vddas == np.max(vddas))[0][0] + 2) < 15 else 15
                    self.logger.error('VREF_D trimming failed! Optimal VREFD_TRIM is {0}'.format(VrefD_opt))
            self.logger.info('Optimal VREFD_TRIM is {0}'.format(VrefD_opt))
        else:
            VrefD_opt = 12  # 0b1000
            self.logger.error('VREF_D trimming failed!')

        if abs(VrefA_results[15]['VDDA'] - VrefA_results[0]['VDDA']) > 0.05:
            VrefA_opt = min(diffs_A, key=diffs_A.get)
            if VrefA_opt < len(vddas):
                if vddas[VrefA_opt] < 0.01:
                    VrefA_opt = (np.where(vddas == np.max(vddas))[0][0] + 2) if (np.where(vddas == np.max(vddas))[0][0] + 2) < 15 else 15
                    self.logger.error('VREF_A trimming failed! Optimal VREFA_TRIM is {0}'.format(VrefA_opt))
            self.logger.info('Optimal VREFA_TRIM is {0}'.format(VrefA_opt))
        else:
            VrefA_opt = 12  # 0b1000
            self.logger.error('VREF_A trimming failed!')

        VrefA_opt = VrefA_opt if VrefA_opt < 15 else 15
        VrefD_opt = VrefD_opt if VrefD_opt < 15 else 15

        self.chip.registers['VOLTAGE_TRIM'].write('0b{0:05b}{1:05b}'.format(VrefA_opt, VrefD_opt))
        self.chip_config['trim']['VREF_A_TRIM'] = VrefA_opt
        self.chip_config['trim']['VREF_D_TRIM'] = VrefD_opt
        VrefD_results['VREF_D_opt'] = int(VrefD_opt)
        VrefD_results['VREF_A_opt'] = int(VrefA_opt)
        self.dump_analog_data('VREF_D Trim' + postfix, VrefD_results)
        self.dump_analog_data('VREF_A Trim' + postfix, VrefA_results)
        self.meta_data['POWER']['vref_trim' + postfix] = self.initial_power(fast=fast)
        self.meta_data['duration']['vref_trim' + postfix] = time.time() - timestamp_start_test

        return int(VrefA_opt), int(VrefD_opt)

    def measure_pc_voltages(self, fast=False):
        out_put_voltages = {}
        for key, _ in self.periphery.needle_card._adc_mux_map.items():
            try:
                out_put_voltages[key] = self.periphery.get_voltage_from_external_mux(key, fast=fast)
            except Exception:
                self.logger.info("Keys not found in _adc_mux_map")
        self.dump_analog_data('PC_VOLT', out_put_voltages)

    def measure_injection_capacitance(self, fast=False):
        self.chip.init()
        self.chip.registers['VOLTAGE_TRIM'].write(int('00' + format(self.VrefA_opt, '04b') + format(self.VrefD_opt, '04b'), 2))
        self.chip.send_global_pulse('cal_reset_pulse', pulse_width=0x3)
        impedence_frq = 10 * 10 ** 6
        vaofs = self.periphery.get_voltage_from_external_mux('GNDA_REF', samples=10, offset=0, fast=fast)
        sensed_vdda = self.periphery.get_voltage_from_external_mux('VDDA', samples=10, offset=vaofs, fast=fast)
        iref, _, _ = self.periphery.measure_iref(self.iref_trim, offset=vaofs, fast=fast)
        delta_c = 0.48 * 10 ** -15
        cpar = 0
        cmeas = 0
        reps = 10
        cap_results = {}
        cap_results['CAP_VDDA'] = sensed_vdda
        cap_results['CAP_IREF'] = iref
        cap_results['CAP_OFF'] = delta_c
        cap_results['CAP_FREQ'] = impedence_frq

        vaofs = self.periphery.get_voltage_from_external_mux('GNDA_REF', samples=10, offset=0, fast=fast)
        for i in range(reps):
            cap_results[i] = {}
            self.chip.registers['MEAS_CAP'].write(0b010)
            self.chip.send_global_pulse('cal_reset_pulse', pulse_width=0x3)
            time.sleep(0.1)
            cur_cap = self.get_current_from_internal_mux('CAP', offset=vaofs, samples=10, wait_time=0.1, set_gnd=False, fast=fast)
            cap_results[i]['CAP_INJ_TOT'] = cur_cap
            cmeas += abs(cur_cap / (impedence_frq * (sensed_vdda)))
            if not fast:
                time.sleep(0.1)
        cmeas = cmeas / reps
        vaofs = self.periphery.get_voltage_from_external_mux('GNDA_REF', samples=10, offset=0, fast=fast)
        for i in range(reps):
            self.chip.registers['MEAS_CAP'].write(0b100)
            self.chip.send_global_pulse('cal_reset_pulse', pulse_width=0x3)
            time.sleep(0.1)
            cur_para = self.get_current_from_internal_mux('CAP_PARASIT', offset=vaofs, samples=10, wait_time=0.1, set_gnd=False, fast=fast)
            cap_results[i]['CAP_INJ_PAR'] = cur_para
            cpar += abs(cur_para / (impedence_frq * (sensed_vdda)))
            if not fast:
                time.sleep(0.1)
        cpar = cpar / reps
        cpix = (cmeas - cpar) / 100 - delta_c
        cap_results['TRUE_CAP'] = cpix
        self.get_current_from_internal_mux('CAP', offset=vaofs, samples=1, wait_time=0.3, set_gnd=True, fast=fast)
        self.dump_analog_data('CAP_MEAS', cap_results)
        self.logger.info("Injection capacitance is: " + str(cpix))

    def measure_injection_DACs(self, fast=False):
        self.chip.init()
        self.chip.registers['VOLTAGE_TRIM'].write(int('00' + format(self.VrefA_opt, '04b') + format(self.VrefD_opt, '04b'), 2))
        self.logger.info("Starting VCAL measurement")
        VCAL_dict = {}
        vaofs = self.get_voltage_from_internal_mux('GNDA20', samples=10, fast=fast)
        for vcal_setting in tqdm(range(400, 4095, 300)):
            VCAL_dict[vcal_setting] = {'large_range': {}, 'small_range': {}}
            for x in range(2):
                self.chip.registers['MEAS_CAP'].write(x)
                if x == 0:
                    range_name = 'small_range'
                elif x == 1:
                    range_name = 'large_range'
                else:
                    logging.error('VCAL range not implemented.')
                mux = self.chip.voltage_mux['GNDA20']
                self.chip.registers['MonitorConfig'].write(0x1000 + mux)
                self.periphery.needle_card.set_adc_mux('GND', fast=fast)
                self.chip.registers['VCAL_HIGH'].write(vcal_setting)
                self.chip.registers['VCAL_MED'].write(vcal_setting)
                self.chip.write_command(self.chip.write_sync(), repetitions=100)
                self.chip.get_ADC_value('VCAL_HIGH', measure='v')
                HIGH_ADC = self.chip.get_ADC_value('VCAL_HIGH', measure='v')
                VCAL_dict[vcal_setting][range_name]['HIGH'] = self.get_voltage_from_internal_mux('VCAL_HIGH', offset=vaofs, fast=fast)
                self.chip.get_ADC_value('VCAL_MED', measure='v')
                MED_ADC = self.chip.get_ADC_value('VCAL_MED', measure='v')
                VCAL_dict[vcal_setting][range_name]['MED'] = self.get_voltage_from_internal_mux('VCAL_MED', offset=vaofs, fast=fast)
                VCAL_dict[vcal_setting][range_name]['MED_ADC'] = int(MED_ADC[0])
                VCAL_dict[vcal_setting][range_name]['HIGH_ADC'] = int(HIGH_ADC[0])
        self.dump_analog_data('VCAL_MEAS', VCAL_dict)
        self.logger.info("Finished VCAL measurement")

    def measure_ADC_currents(self, fast=False):
        self.chip.init()
        chip_currents = {}
        offset = self.get_voltage_from_internal_mux('GNDA20', samples=10, fast=fast)
        for n in self.chip.current_mux.keys():
            chip_currents[n] = abs(self.get_current_from_internal_mux(n, offset=offset, fast=fast))
        self.dump_analog_data('chip_currents', chip_currents)
        self.logger.info("Finished Current measurement")

    def measure_ADC_voltages(self, fast=False):
        self.chip.init()
        chip_voltages = {}
        offset = self.get_voltage_from_internal_mux('GNDA20', samples=10, fast=fast)
        for n in self.chip.voltage_mux.keys():
            chip_voltages[n] = abs(self.get_voltage_from_internal_mux(n, offset=offset, fast=fast))
        self.dump_analog_data('chip_voltages', chip_voltages)
        self.logger.info("Finished Voltage measurement")
        return chip_voltages

    # def measure_ADC_calibration(self):
    #     self.chip.init()
    #     chip_voltages = {}
    #     offset = self.get_voltage_from_internal_mux('GNDA20', samples=1)
    #     VREF_ADC = self.get_voltage_from_internal_mux('VREF_ADC', samples=1)
    #     bitstring = int('1' + format(32, '06b') + format(45, '06b'), 2)
    #     self.chip.registers['MonitorConfig'].write(bitstring)  # Select MUX before all were 4
    #     cnt = 0
    #     for n in np.arange(0, VREF_ADC, 0.1):
    #         chip_voltages[cnt] = {}
    #         self.periphery.set_external_voltage(n)
    #         self.periphery.smu_on()
    #         time.sleep(0.1)
    #         self.chip.send_global_pulse(bitnames='reset_monitor_data', pulse_width=8)  # Reset Monitor Data
    #         self.chip.send_global_pulse(bitnames='reset_adc', pulse_width=8)  # Reset ADC
    #         self.chip.registers['MonitorConfig'].write(bitstring)  # Select MUX before all were 4
    #         self.chip.send_global_pulse(bitnames='adc_start_conversion', pulse_width=8)  # Start ADC
    #         self.chip.write_command(self.chip.write_sync(write=False), repetitions=10)
    #         val_adc = self.chip.registers['MonitoringDataADC'].read()
    #         self.periphery.smu_off()
    #         print("adc", cnt, n, n-offset, val_adc)
    #         chip_voltages[cnt]['ADC'] = int(val_adc)
    #         chip_voltages[cnt]['smu'] = float(n - offset)
    #         cnt += 1
    #     self.dump_analog_data('adc_calibration', chip_voltages)
    #     self.logger.info("Finished adc calibration")
    #     return chip_voltages

    def read_efuses_chip(self):
        self.chip.init()
        self.chip.registers['EfusesConfig'].write(0x0f0f)
        self.chip.registers['EfusesReadData0'].read()
        self.chip.registers['EfusesReadData1'].read()
        self.chip.send_global_pulse(bitnames=['reset_efuses'], pulse_width=0xff)
        efuse0 = self.chip.registers['EfusesReadData0'].read()
        efuse1 = self.chip.registers['EfusesReadData1'].read()
        return (efuse1 << 16) + efuse0

    def write_efuses_chip(self, value):
        self.chip.init()
        self.chip.registers['EfusesWriteData0'].write(value & 0xffff)
        self.chip.registers['EfusesWriteData1'].write(value >> 16)
        self.chip.registers['EfusesConfig'].write(0xf0f0)
        self.chip.send_global_pulse(bitnames=['reset_efuses'], pulse_width=0xff)
        self.chip.send_global_pulse(bitnames=['start_efuses_programmer'], pulse_width=21)

    def check_chip_addresses(self, fast=False):
        original_chip_id = self.chip.chip_id
        chip_ids = {}
        for n in range(5):
            self.chip.chip_id = 2**n
            self.periphery.needle_card.write_gpio_expander('CHIP_ID', 2**n, fast=fast)
            try:
                self.chip.init_communication()
                chip_ids[2**n] = True
            except Exception:
                chip_ids[2**n] = False
        self.chip.chip_id = original_chip_id
        self.periphery.needle_card.write_gpio_expander('CHIP_ID', original_chip_id, fast=fast)
        self.dump_analog_data('chip_ids', chip_ids)

    def write_efuses(self):
        efuse_out_dict = {}
        data = int(itkpix_efuse_codec.encode(self.probe_location.upper(), int(self.chip_sn, 16)), 2)
        self.periphery.needle_card.write_gpio_expander('EN_VDD_EFUSE', 0)
        efuse_out_dict['read_before'] = hex(self.read_efuses_chip())
        print("efuses_before", efuse_out_dict['read_before'], hex(data))
        self.periphery.needle_card.write_gpio_expander('EN_VDD_EFUSE', 1)
        self.write_efuses_chip(data)
        self.periphery.needle_card.write_gpio_expander('EN_VDD_EFUSE', 0)
        efuse_out_dict['data'] = hex(data)
        efuse_out_dict['read'] = hex(self.read_efuses_chip())
        efuse_out_dict['success'] = efuse_out_dict['read'] == efuse_out_dict['data']
        self.dump_analog_data('efuses', efuse_out_dict)
        print("efuses_after", efuse_out_dict['read'], hex(data))

    def measure_regulator_IV_curves(self, start=1.5, stop=1.95, step=0.1, fast=True):
        self.logger.info('Start regulator IV measurement routine...')

        # TODO: optimize this, curently around 6sec
        self.periphery.power_off_chip()
        self.periphery.enable_SHUNT_mode(VINA=start, VIND=start, IINA=2.5, IIND=2.5, fast=fast)
        self.periphery.power_on_chip(fast=fast)

        iv_curves = {}

        for V_IN in np.arange(start, stop, step):
            self.periphery._devices['VIND'].set_voltage(V_IN)  # + 0.001)
            self.periphery._devices['VINA'].set_voltage(V_IN)  # + 0.001)
            time.sleep(0.1)
            V_IN_i = round(float(V_IN), 2)
            try:
                self.chip.init_communication()
                self.chip.registers.reset_all()
                self.chip.enable_core_col_clock()
            except Exception:
                pass
            gndd, gnda = self.get_voltage_offsets(samples=3, fast=fast)
            iv_curves[V_IN_i] = {}
            iv_curves[V_IN_i]['GNDA'] = gnda
            iv_curves[V_IN_i]['GNDD'] = gndd
            iv_curves[V_IN_i]['VIN_D_tti_cur'] = self.periphery._devices['VIND'].get_current()
            iv_curves[V_IN_i]['VIN_A_tti_cur'] = self.periphery._devices['VINA'].get_current()
            iv_curves[V_IN_i]['VOFS'] = self.periphery.get_voltage_from_external_mux('VOFS', offset=0, fast=fast)
            iv_curves[V_IN_i]['VIN_D'] = self.periphery.get_voltage_from_external_mux('VIND', offset=0, fast=fast)
            iv_curves[V_IN_i]['VIN_A'] = self.periphery.get_voltage_from_external_mux('VINA', offset=0, fast=fast)
            iv_curves[V_IN_i]['VIN_REXTD'] = self.periphery.get_voltage_from_external_mux('VIN_REXTD', offset=0, fast=fast)
            iv_curves[V_IN_i]['VIN_REXTA'] = self.periphery.get_voltage_from_external_mux('VIN_REXTA', offset=0, fast=fast)
            iv_curves[V_IN_i]['REXTD'] = self.periphery.get_voltage_from_external_mux('REXTD', offset=0, fast=fast)
            iv_curves[V_IN_i]['REXTA'] = self.periphery.get_voltage_from_external_mux('REXTA', offset=0, fast=fast)
            iv_curves[V_IN_i]['VDDD'] = self.periphery.get_voltage_from_external_mux('VDDD', offset=0, fast=fast)
            iv_curves[V_IN_i]['VDDA'] = self.periphery.get_voltage_from_external_mux('VDDA', offset=0, fast=fast)
            iv_curves[V_IN_i]['VIN_D_int'] = self.get_voltage_from_internal_mux('VIND_HALF', offset=0, fast=fast)  # FIXME: Will this ever yield a useful value?
            iv_curves[V_IN_i]['VIN_A_int'] = self.get_voltage_from_internal_mux('VINA_HALF', offset=0, fast=fast)  # FIXME: Will this ever yield a useful value?
            if not fast:
                iv_curves[V_IN_i]['VIN_D_tti'] = self.periphery._devices['VIND'].get_voltage()
                iv_curves[V_IN_i]['VIN_A_tti'] = self.periphery._devices['VINA'].get_voltage()
                iv_curves[V_IN_i]['VIN_D_cur'] = self.get_current_from_internal_mux('IIND', offset=0)
                iv_curves[V_IN_i]['VIN_A_cur'] = self.get_current_from_internal_mux('IINA', offset=0)
                iv_curves[V_IN_i]['VIN_D_shunt_cur'] = self.get_current_from_internal_mux('I_SHUNT_D', offset=0)
                iv_curves[V_IN_i]['VIN_A_shunt_cur'] = self.get_current_from_internal_mux('I_SHUNT_A', offset=0)
                iv_curves[V_IN_i]['VOFS_int'] = self.get_voltage_from_internal_mux('VOFS_HALF', offset=0)
                iv_curves[V_IN_i]['VREFA'] = self.periphery.get_voltage_from_external_mux('VREFA', offset=0)
                iv_curves[V_IN_i]['VREFA_int'] = self.get_voltage_from_internal_mux('VREFA', offset=0)
                iv_curves[V_IN_i]['VREFD'] = self.periphery.get_voltage_from_external_mux('VREFD', offset=0)
                iv_curves[V_IN_i]['VREFD_int'] = self.get_voltage_from_internal_mux('VREFD', offset=0)
                iv_curves[V_IN_i]['VDD_PRE'] = self.periphery.get_voltage_from_external_mux('VDD_PRE', offset=0)
                iv_curves[V_IN_i]['VREF_PRE_int'] = self.get_voltage_from_internal_mux('VREF_PRE', offset=0)
                iv_curves[V_IN_i]['R_IREF'] = self.periphery.get_voltage_from_external_mux('R_IREF', offset=0)
        self.dump_analog_data('IV curves', iv_curves)

        self.logger.success('IV curve measurement done!')
        self.periphery.power_off_chip()
        self.periphery.reset_SCC()

    def measure_over_voltage_protection(self, start=2.0, stop=2.01, step=0.1, VIN=2.4, fast=True):
        self.logger.info('Start over voltage protection  measurement routine...')

        # TODO: optimize this, curently around 6sec
        self.periphery.power_off_chip()
        self.periphery.enable_SHUNT_mode(VINA=VIN, VIND=VIN, IINA=start, IIND=start, fast=fast)
        self.periphery.power_on_chip(fast=fast)
        self.periphery._devices['smu'].set_voltage_limit(3)

        measurements = {}
        for I_IN in np.arange(start, stop, step):
            self.periphery._devices['VIND'].set_current_limit(I_IN)
            self.periphery._devices['VINA'].set_current_limit(I_IN)
            time.sleep(0.1)
            I_IN_i = round(float(I_IN), 2)

            try:
                self.chip.init_communication()
                self.chip.registers.reset_all()
                self.chip.enable_core_col_clock()
            except Exception:
                pass

            gndd, gnda = self.get_voltage_offsets(samples=3, fast=fast)
            measurements[I_IN_i] = {}
            measurements[I_IN_i]['GNDA'] = gnda
            measurements[I_IN_i]['GNDD'] = gndd
            measurements[I_IN_i]['VIN_D_tti_cur'] = self.periphery._devices['VIND'].get_current()
            measurements[I_IN_i]['VIN_A_tti_cur'] = self.periphery._devices['VINA'].get_current()
            measurements[I_IN_i]['VREF_OVP'] = self.periphery.get_voltage_from_external_mux('VREF_OVP', offset=0, fast=fast)
            measurements[I_IN_i]['VIN_SET'] = VIN
            measurements[I_IN_i]['VIN_D'] = self.periphery.get_voltage_from_external_mux('VIND', offset=0, fast=fast)
            measurements[I_IN_i]['VIN_A'] = self.periphery.get_voltage_from_external_mux('VINA', offset=0, fast=fast)
            measurements[I_IN_i]['VIN_REXTD'] = self.periphery.get_voltage_from_external_mux('VIN_REXTD', offset=0, fast=fast)
            measurements[I_IN_i]['VIN_REXTA'] = self.periphery.get_voltage_from_external_mux('VIN_REXTA', offset=0, fast=fast)
            measurements[I_IN_i]['REXTD'] = self.periphery.get_voltage_from_external_mux('REXTD', offset=0, fast=fast)
            measurements[I_IN_i]['REXTA'] = self.periphery.get_voltage_from_external_mux('REXTA', offset=0, fast=fast)
            measurements[I_IN_i]['VDDD'] = self.periphery.get_voltage_from_external_mux('VDDD', offset=0, fast=fast)
            measurements[I_IN_i]['VDDA'] = self.periphery.get_voltage_from_external_mux('VDDA', offset=0, fast=fast)
        self.dump_analog_data('OVP', measurements)

        self.logger.success('Over voltage protection measurement done!')
        self.periphery.power_off_chip()
        self.periphery.reset_SCC()

    def get_ntc_temperature(self, a, b, c, in_kelvin=False, voltage=1, fast=False):
        voltage_ofs = self.periphery.get_voltage_from_external_mux('GNDA_REF', offset=0, fast=fast)
        current = self.periphery.get_current_from_external_mux('NTC', offset=voltage, fast=fast)
        r_ntc = (voltage - voltage_ofs) / (current)
        logres = np.log(r_ntc)
        tK = 1.0 / (a + b * logres + c * pow(logres, 3))
        if (in_kelvin):
            return r_ntc, tK
        else:
            return r_ntc, tK - 273.15

    def _get_diode_temperature_sensor(self, sensor, fast=False):
        in_dict = {}
        lower, upper = [], []
        lower_keith, upper_keith = [], []
        sensor_id_dict = {'TEMPSENS_A': 0, 'TEMPSENS_D': 1, 'TEMPSENS_C': 2}
        self.chip.get_ADC_value('TEMPSENS_A', measure='v')
        gnda_ofs = self.periphery.get_voltage_from_external_mux('GNDA_REF', offset=0, fast=fast)
        for diode_current in range(0, 16, 1):
            if sensor == 'TEMPSENS_A':
                bitstring_SLDO = int('0' + format(0, '04b') + '01' + format(diode_current, '04b') + '0', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_D':
                bitstring_SLDO = int('1' + format(diode_current, '04b') + '00' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_C':
                bitstring_SLDO = int('0' + format(0, '04b') + '00' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('1' + format(diode_current, '04b') + '0', 2)
            self.chip.registers['MON_SENS_SLDO'].write(bitstring_SLDO)
            self.chip.registers['MON_SENS_ACB'].write(bitstring_ACB)
            self.chip.write_sync()
            lower_keith.append(self.get_voltage_from_internal_mux(sensor, offset=gnda_ofs, fast=fast))
            self.chip.get_ADC_value(sensor, measure='v')
            lower.append(self.chip.get_ADC_value(sensor, measure='v')[0])
            if sensor == 'TEMPSENS_A':
                bitstring_SLDO = int('0' + format(0, '04b') + '01' + format(diode_current, '04b') + '1', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_D':
                bitstring_SLDO = int('1' + format(diode_current, '04b') + '10' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_C':
                bitstring_SLDO = int('0' + format(0, '04b') + '00' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('1' + format(diode_current, '04b') + '1', 2)
            self.chip.registers['MON_SENS_SLDO'].write(bitstring_SLDO)
            self.chip.registers['MON_SENS_ACB'].write(bitstring_ACB)
            self.chip.write_sync()
            upper_keith.append(self.get_voltage_from_internal_mux(sensor, offset=gnda_ofs, fast=fast))
            self.chip.get_ADC_value(sensor, measure='v')
            upper.append(self.chip.get_ADC_value(sensor, measure='v')[0])
        mean_adc = int(np.mean(upper) - np.mean(lower))
        mean_keith = float(np.mean(upper_keith) - np.mean(lower_keith))
        mean_temp = self.chip.calibration.get_temperature_from_ADC(mean_adc, sensor=sensor_id_dict[sensor])
        mean_temp_keith = round((self.chip.calibration._dV_to_T(mean_keith, sensor=sensor_id_dict[sensor])), 3)
        in_dict['mean_adc'] = mean_adc
        in_dict['mean_adc_upper'] = int(np.mean(upper))
        in_dict['mean_adc_lower'] = int(np.mean(lower))
        in_dict['mean_keith'] = float(mean_keith)
        in_dict['mean_keith_upper'] = float(np.mean(upper_keith))
        in_dict['mean_keith_lower'] = float(np.mean(lower_keith))
        in_dict['mean_temp'] = float(mean_temp)
        in_dict['mean_temp_keith'] = float(mean_temp_keith)
        return in_dict

    def _get_resistive_temperature_sensor(self, sensor, samples=1, fast=False):
        lower = []
        keith = []
        out_dict = {}
        # sensor_id_dict = {'TEMPSENS_T': 0, 'TEMPSENS_B': 1}
        before_value = self.chip.registers['MON_ADC'].get()
        gnda_ofs = self.periphery.get_voltage_from_external_mux('GNDA_REF', offset=0, fast=fast)
        self.chip.get_ADC_value(sensor, measure='v')[0]
        for reps in range(samples):
            if sensor == 'TEMPSENS_T':
                self.chip.registers['MON_ADC'].write(0x80 + (before_value & 0x3f))
            elif sensor == 'TEMPSENS_B':
                self.chip.registers['MON_ADC'].write(0x100 + (before_value & 0x3f))
            self.chip.write_sync()
            lower.append(self.chip.get_ADC_value(sensor, measure='v')[0])
            keith.append(self.get_voltage_from_internal_mux(sensor, offset=gnda_ofs, fast=fast))
        self.chip.registers['MON_ADC'].write(0x40 + (before_value & 0x3f))
        mean_adc = int(np.mean(lower))
        mean_keith = float(np.mean(keith))
        # mean_temp = self.chip.calibration._get_temperature_from_ADC_resistive_sensors(mean_adc, sensor=sensor_id_dict[sensor])
        out_dict['mean_adc'] = mean_adc
        out_dict['mean_keith'] = mean_keith
        return out_dict

    def measure_chip_temps(self, fast=False):
        self.periphery.power_off_chip()
        self.periphery.reset_SCC()
        self.periphery.enable_LDO_mode()
        self.periphery.power_on_chip()
        self.chip.init()
        self.chip.enable_core_col_clock()  # Enable clock on full chip
        self.chip.enable_macro_col_cal()  # Enable analog calibration on full chip
        self.chip._enable_core_col_reset()  # Reset core columns
        self.chip.registers['DAC_COMP_DIFF'].write(10)
        self.chip.registers['DAC_LCC_DIFF'].write(10)
        self.chip.registers['DAC_PRECOMP_DIFF'].write(10)
        self.chip.registers['DAC_TH1_L_DIFF'].write(1023)
        self.chip.registers['DAC_TH1_M_DIFF'].write(1023)
        self.chip.registers['DAC_TH1_R_DIFF'].write(1023)
        self.chip.registers['DAC_TH2_DIFF'].write(0)
        self.chip.registers['DAC_VFF_DIFF'].write(10)
        in_dict = {}
        in_dict['a'] = 7.489e-4
        in_dict['b'] = 2.769e-4
        in_dict['c'] = 7.0595e-8
        r_ntc, temp_ntc = self.get_ntc_temperature(a=in_dict['a'], b=in_dict['b'], c=in_dict['c'], fast=fast)
        in_dict['r_pc_ntc'] = float(r_ntc)
        in_dict['temp_ntc'] = float(temp_ntc)
        for sensor in ['TEMPSENS_A', 'TEMPSENS_D', 'TEMPSENS_C']:
            in_dict[sensor] = self._get_diode_temperature_sensor(sensor, fast=fast)
        for sensor in ['TEMPSENS_T', 'TEMPSENS_B']:
            in_dict[sensor] = self._get_resistive_temperature_sensor(sensor, samples=1, fast=fast)

        self.dump_analog_data('TEMP_SNS', in_dict)

    def main(self, check_power=False, chip_type=True, powercycle=True, fast=True):
        '''
        Chip_type true --> ITkPixV1.1
        Chip_type false --> ITkPixV1.0
        '''
        self.logger.info('Start probing chip {0}'.format(self.chip_sn))
        self.meta_data['start_time'] = time.time()
        self.meta_data['fw_version'] = self.periphery.bdaq.fw_version
        self.meta_data['sw_version'] = bu.get_software_version()
        speed = 1280
        if self.periphery.bdaq['system']['AURORA_RX_640M'] == 1:
            speed = 640
        self.meta_data['rx_speed'] = speed
        self.meta_data['lanes'] = self.periphery.bdaq.num_rx_channels

        self.testbench['general']['output_directory'] = self.working_dir
        self.testbench['modules']['module_0'][self.chip_sn] = self.testbench['modules']['module_0'].pop('chip_0')
        self.testbench['modules']['module_0'][self.chip_sn]['chip_sn'] = self.chip_sn
        self.testbench['modules']['module_0'][self.chip_sn]['chip_config_file'] = self.chip_config_file

        power_results = {}
        self.periphery.reset_SCC()
        self.periphery.enable_SHUNT_mode()
        self.periphery.power_on_chip(fast=fast)
        self.initial_power(fast=fast)

        IREF_opt = self.trim_IREF(fast=fast)  # Find optimal IREF_TRIM value
        self.iref_trim = IREF_opt
        self.periphery.needle_card.write_gpio_expander('IREF_TRIM', IREF_opt, fast=fast)

        self.VrefA_opt, self.VrefD_opt = self.trim_VREF(fast=fast)
        self.chip.configuration['trim']['VREF_A_TRIM'] = self.VrefA_opt
        self.chip.configuration['trim']['VREF_D_TRIM'] = self.VrefD_opt

        if chip_type:
            timestamp_start_test = time.time()
            try:
                self.periphery.power_off_chip()
                self.periphery.enable_LDO_mode()
                self.periphery.power_on_chip(fast=fast)
                self.chip.init_communication()
                self.meta_data['POWER']['LDO_mode'] = self.initial_power(fast=fast)
            except Exception as ex:
                self.logger.exception(ex)
                self.periphery.power_off_chip()
            self.meta_data['duration']['LDO_Power'] = time.time() - timestamp_start_test

            timestamp_start_test = time.time()
            self.measure_regulator_IV_curves(fast=fast)
            self.meta_data['duration']['IV_curve'] = time.time() - timestamp_start_test

            timestamp_start_test = time.time()
            self.measure_over_voltage_protection(fast=fast)
            self.meta_data['duration']['OV_protection'] = time.time() - timestamp_start_test

            timestamp_start_test = time.time()
            shunt_mode = {}
            try:
                self.periphery.enable_SHUNT_mode(fast=fast)
                self.periphery.power_on_chip(fast=fast)
                self.chip.init_communication()
                shunt_mode['SHUNT_MODE'] = True
            except Exception as ex:
                self.logger.exception(ex)
                self.periphery.power_off_chip()
                self.periphery.enable_LDO_mode()
                self.periphery.power_on_chip(fast=fast)
                shunt_mode['SHUNT_MODE'] = False
            self.dump_analog_data('SHUNT_MODE', shunt_mode)
            self.meta_data['duration']['SHUNT_MODE'] = time.time() - timestamp_start_test

        timestamp_start_test = time.time()
        self.measure_pc_voltages(fast=fast)
        self.meta_data['POWER']['measure_pc_voltages'] = self.initial_power(fast=fast)
        self.meta_data['duration']['measure_pc_voltages'] = time.time() - timestamp_start_test

        with open(default_cfg, 'r') as f:
            default_chip_cfg = yaml.safe_load(f)
        default_chip_cfg['lane_test'] = True
        default_chip_cfg['trim']['VREF_A_TRIM'] = self.VrefA_opt
        default_chip_cfg['trim']['VREF_D_TRIM'] = self.VrefD_opt
        self.periphery.powercycle(fast=fast)

        timestamp_start_test = time.time()
        self.check_chip_addresses(fast=fast)
        self.meta_data['duration']['chip_id_check'] = time.time() - timestamp_start_test

        with open(default_cfg, 'w') as yamlfile:
            yaml.safe_dump(default_chip_cfg, yamlfile)

        if self.chip_sn != '0x00000':
            timestamp_start_test = time.time()
            try:
                self.write_efuses()
            except Exception as ex:
                self.logger.exception(ex)
                self.logger.info("Efuse writing failed.")
            self.meta_data['POWER']['efuse_write'] = self.initial_power(fast=fast)
            self.meta_data['duration']['efuse_write'] = time.time() - timestamp_start_test

        timestamp_start_test = time.time()
        try:
            self.measure_ADC_currents(fast=fast)
        except Exception as ex:
            self.logger.exception(ex)
            self.logger.info("Measure chip currents failed.")
        self.meta_data['POWER']['measure_chip_currents'] = self.initial_power(fast=fast)
        self.meta_data['duration']['measure_chip_currents'] = time.time() - timestamp_start_test

        timestamp_start_test = time.time()
        try:
            self.measure_ADC_voltages(fast=fast)
        except Exception as ex:
            self.logger.exception(ex)
            self.logger.info("Measure chip voltages failed.")
        self.meta_data['POWER']['measure_chip_voltages'] = self.initial_power(fast=fast)
        self.meta_data['duration']['measure_chip_voltages'] = time.time() - timestamp_start_test

        if not fast:
            timestamp_start_test = time.time()
            try:
                self.measure_injection_capacitance(fast=fast)
            except Exception as ex:
                self.logger.exception(ex)
                self.logger.info("capacitance measurement failed.")
            self.meta_data['POWER']['inj_cap'] = self.initial_power(fast=fast)
            self.meta_data['duration']['inj_cap'] = time.time() - timestamp_start_test

        timestamp_start_test = time.time()
        try:
            self.measure_injection_DACs(fast=fast)
        except Exception as ex:
            self.logger.exception(ex)
            self.logger.info("Injection DAC measurement failed")
        self.meta_data['POWER']['VCAL_meas'] = self.initial_power(fast=fast)
        self.meta_data['duration']['VCAL_meas'] = time.time() - timestamp_start_test

        timestamp_start_test = time.time()
        try:
            self.measure_chip_temps(fast=fast)
        except Exception as ex:
            self.logger.exception(ex)
            self.logger.info("Measure chip temperatures.")
        self.meta_data['POWER']['temperatures'] = self.initial_power(fast=fast)
        self.meta_data['duration']['measure_temperatures'] = time.time() - timestamp_start_test

        try:
            if shunt_mode['SHUNT_MODE']:
                self.periphery.power_off_chip()
                self.periphery.enable_SHUNT_mode(fast=fast)
                self.periphery.power_on_chip(fast=fast)
                self.chip.init_communication()
        except Exception as ex:
            self.logger.exception(ex)

        self.periphery.bdaq.close()

        timestamp_start_test = time.time()
        try:
            if powercycle:
                self.periphery.powercycle(chip=True, bdaq=False, check=check_power, fast=fast)
                time.sleep(1)
            testbench = copy.deepcopy(self.testbench)
            with DataMergingTest(bench_config=testbench) as test:
                test.start()
                power_results['Advanced config'] = self.periphery.get_chip_power()
        except Exception as ex:
            self.logger.exception(ex)
            self.periphery.power_off_chip()
        self.meta_data['POWER']['data_merging'] = self.initial_power(internals=False, fast=fast)
        self.meta_data['duration']['data_merging'] = time.time() - timestamp_start_test

        aurora_register_test = copy.deepcopy(self.chip_config)
        aurora_register_test.update(self.config['aurora_register_test'])
        testbench = copy.deepcopy(self.testbench)
        for n in range(4):
            timestamp_start_test = time.time()
            try:
                if powercycle:
                    self.periphery.powercycle(chip=True, bdaq=False, check=check_power, fast=fast)
                    time.sleep(1)
                testbench['modules']['module_0'][self.chip_sn]['receiver'] = 'rx' + str(n)
                with RegisterTest(scan_config=aurora_register_test, bench_config=testbench, suffix='_rx' + str(n)) as scan:
                    scan.chip.configuration['lane_test'] = True
                    scan.start()
            except Exception as ex:
                self.logger.exception(ex)
                self.periphery.power_off_chip()
            self.meta_data['POWER']['aurora_test_' + str(n)] = self.initial_power(internals=False, fast=fast)
            self.meta_data['duration']['aurora_test_' + str(n)] = time.time() - timestamp_start_test

        with open(default_cfg, 'r') as f:
            default_chip_cfg = yaml.safe_load(f)
        default_chip_cfg['lane_test'] = False
        with open(default_cfg, 'w') as yamlfile:
            yaml.safe_dump(default_chip_cfg, yamlfile)

        timestamp_start_test = time.time()
        try:
            if powercycle:
                self.periphery.powercycle(chip=True, bdaq=False, check=check_power, fast=fast)
                time.sleep(1)
            register_config = copy.deepcopy(self.chip_config)
            register_config.update(self.config['register_test'])
            testbench = copy.deepcopy(self.testbench)
            testbench['modules']['module_0'][self.chip_sn]['use_ptot'] = False
            with RegisterTest(scan_config=register_config, bench_config=testbench) as scan:
                scan.start()
                power_results['Advanced config'] = self.periphery.get_chip_power()
        except Exception as ex:
            self.logger.exception(ex)
            self.periphery.power_off_chip()
        self.meta_data['POWER']['register_test'] = self.initial_power(internals=False, fast=fast)
        self.meta_data['duration']['register_test'] = time.time() - timestamp_start_test

        timestamp_start_test = time.time()
        try:
            if powercycle:
                self.periphery.powercycle(chip=True, bdaq=False, check=check_power, fast=fast)
                time.sleep(1)
            register_config = copy.deepcopy(self.chip_config)
            testbench = copy.deepcopy(self.testbench)
            with OSCScan(scan_config=register_config, bench_config=testbench) as scan:
                scan.start()
                power_results['Advanced config'] = self.periphery.get_chip_power()
        except Exception as ex:
            self.logger.exception(ex)
            self.periphery.power_off_chip()
        self.meta_data['POWER']['osc_scan'] = self.initial_power(internals=False, fast=fast)
        self.meta_data['duration']['osc_scan'] = time.time() - timestamp_start_test

        timestamp_start_test = time.time()
        try:
            if powercycle:
                self.periphery.powercycle(chip=True, bdaq=False, check=check_power, fast=fast)
                time.sleep(1)
            digital_config = copy.deepcopy(self.chip_config)
            digital_config.update(self.config['digital_scan'])
            testbench = copy.deepcopy(self.testbench)
            testbench['modules']['module_0'][self.chip_sn]['use_ptot'] = False
            with DigitalScan(scan_config=digital_config, bench_config=testbench) as scan:
                scan.start()
                power_results['Advanced config'] = self.periphery.get_chip_power()
                if scan.ana_proc:
                    self.running_analysis_processes.append(scan.ana_proc)
        except Exception as ex:
            self.logger.exception(ex)
            self.periphery.power_off_chip()
        self.meta_data['POWER']['digital_scan'] = self.initial_power(internals=False, fast=fast)
        self.meta_data['duration']['digital_scan'] = time.time() - timestamp_start_test

        timestamp_start_test = time.time()
        try:
            if powercycle:
                self.periphery.powercycle(chip=True, bdaq=False, check=check_power, fast=fast)
                time.sleep(1)
            analog_config = copy.deepcopy(self.chip_config)
            analog_config.update(self.config['analog_scan'])
            testbench = copy.deepcopy(self.testbench)
            testbench['modules']['module_0'][self.chip_sn]['use_ptot'] = ~chip_type
            with AnalogScan(scan_config=analog_config, bench_config=testbench) as scan:
                scan.start()
                power_results['Advanced config'] = self.periphery.get_chip_power()
                if scan.ana_proc:
                    self.running_analysis_processes.append(scan.ana_proc)
        except Exception as ex:
            self.logger.exception(ex)
            self.periphery.power_off_chip()
        self.meta_data['POWER']['analog_scan'] = self.initial_power(internals=False, fast=fast)
        self.meta_data['duration']['analog_scan'] = time.time() - timestamp_start_test

        if not fast:
            timestamp_start_test = time.time()
            try:
                if powercycle:
                    self.periphery.powercycle(chip=True, bdaq=False, check=check_power, fast=fast)
                    time.sleep(1)
                pixeltest_config = copy.deepcopy(self.chip_config)
                pixeltest_config.update(self.config['register_test'])
                testbench = copy.deepcopy(self.testbench)
                testbench['modules']['module_0'][self.chip_sn]['use_ptot'] = False
                with PixelRegisterScan(scan_config=pixeltest_config, bench_config=testbench) as scan:
                    scan.start()
                    power_results['Advanced config'] = self.periphery.get_chip_power()
                    if scan.ana_proc:
                        self.running_analysis_processes.append(scan.ana_proc)
            except Exception as ex:
                self.logger.exception(ex)
                self.periphery.power_off_chip()
            self.meta_data['POWER']['pixel_register_scan'] = self.initial_power(internals=False, fast=fast)
            self.meta_data['duration']['pixel_register_scan'] = time.time() - timestamp_start_test

        timestamp_start_test = time.time()
        try:
            if powercycle:
                self.periphery.powercycle(chip=True, bdaq=False, check=check_power, fast=fast)
                time.sleep(1)
            threshold_config = copy.deepcopy(self.chip_config)
            threshold_config.update(self.config['threshold_scan'])
            testbench = copy.deepcopy(self.testbench)
            testbench['modules']['module_0'][self.chip_sn]['use_ptot'] = False
            with FastThresholdScan(scan_config=threshold_config, bench_config=testbench) as scan:
                scan.start()
                power_results['Advanced config'] = self.periphery.get_chip_power()
                if scan.ana_proc:
                    self.running_analysis_processes.append(scan.ana_proc)
        except Exception as ex:
            self.logger.exception(ex)
            self.periphery.power_off_chip()
        self.meta_data['POWER']['threshold_scan'] = self.initial_power(internals=False, fast=fast)
        self.meta_data['duration']['threshold_scan'] = time.time() - timestamp_start_test

        self.meta_data['stop_time'] = time.time()
        self.dump_meta_data('scan_times', self.meta_data)
        self.periphery.power_off_chip()


if __name__ == '__main__':
    CHIP_SN = '0x00000'
    location = 'Bonn'
    working_dir = os.path.join(os.getcwd(), 'output_data')
    chipprober = ChipProber(chip_sn=CHIP_SN, configuration=configuration, working_dir=working_dir, probe_location=location, restart_bdaq=False)

    try:
        chipprober.main()
    except (Exception, KeyboardInterrupt) as e:
        chipprober.close()
        raise e
