#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script generates an SQL command file to export waferprobing data to the RD53 database
'''

from os import path
import xlrd
from dateutil import parser

in_file = '/bdaq53/waferprobing/wafer_data/wafer_summary_0x141.xlsx'
update = False      # Wether to use UPDATE or INSERT commands
thickness = 450
diced_loc = 'DISCO'
diced_dt = parser.parse('08/23/2021')
test_date = parser.parse('2021-07-19')
mon_adc_trim = 5    # Default value since ADC is not yet trimmed in waferprobing
location = 'CERN'
location_date = parser.parse('2021-08-25')


codes = {
    0: 10,
    0.4: 1,
    0.6: 1,
    1: 2
}

workbook = xlrd.open_workbook(in_file, on_demand=True)
worksheet_results = workbook.sheet_by_index(0)

wafer_no = int(worksheet_results.cell_value(1, 0)[:5], 16)

out_file = path.join(path.dirname(in_file), 'wafer_{0}_sql_commands.txt'.format(wafer_no))

scans_dict = {}
for col in range(worksheet_results.ncols):
    val = worksheet_results.cell_value(0, col)
    scans_dict[val] = col

with open(out_file, 'w') as f:
    for row in range(1, worksheet_results.nrows):
        chip_sn = worksheet_results.cell_value(row, 0)
        row_out = int(worksheet_results.cell_value(row, 0)[5:6], 16)
        col_out = int(worksheet_results.cell_value(row, 0)[6:7], 16)
        result = worksheet_results.cell_value(row, scans_dict['Yield'])
        result_code = codes[result]
        vcal_high_slope = worksheet_results.cell_value(row, scans_dict['VCAL HIGH Large Range Slope'])
        vcal_high_offset = worksheet_results.cell_value(row, scans_dict['VCAL HIGH Large Range Offset'])
        iref_trim = worksheet_results.cell_value(row, scans_dict['Iref Trim Bit'])
        vref_a_trim = worksheet_results.cell_value(row, scans_dict['VDDA Trim Bit'])
        vref_d_trim = worksheet_results.cell_value(row, scans_dict['VDDD Trim Bit'])
        cap_val = worksheet_results.cell_value(row, scans_dict['Injection capacitance'])

        if update:  # Use UPDATE commands per value
            data = {'IREF_TRIM': iref_trim,
                    'VREF_A_TRIM': vref_a_trim,
                    'VREF_D_TRIM': vref_d_trim,
                    'CAP_VAL': cap_val,
                    'VCAL_SLOPE': vcal_high_slope,
                    'VCAL_OFFSET': vcal_high_offset}
            for key, var in data.items():
                if type(var) == float:
                    command = "UPDATE RD53CHIPS SET {0}={1:1.15f} WHERE SN='{chip_sn}';".format(key, var, chip_sn=chip_sn)
                else:
                    command = "UPDATE RD53CHIPS SET {0}={1} WHERE SN='{chip_sn}';".format(key, var, chip_sn=chip_sn)
                f.write(command + '\n')
            f.write('\n')
        else:  # Use INSERT commands per chip
            command = "INSERT INTO RD53CHIPS (SN,hexwafer,wafer,col,row,Diced_LOC,Diced_DT,thickness,status,location,IREF_TRIM,VREF_A_TRIM,VREF_D_TRIM,VCAL_SLOPE,VCAL_OFFSET,CAPVALfF) VALUES('{chip_sn}','{hexwafer}',{wafer_no},{col},{row},'{diced_loc}','{diced_dt}',{thickness},'${result_code}@{test_date}$','${location}@{location_date}$',{IREF_TRIM},{VREF_A_TRIM},{VREF_D_TRIM},{VCAL_SLOPE},{VCAL_OFFSET},{CAPVALfF});".format(
                chip_sn=chip_sn,
                hexwafer=hex(wafer_no),
                wafer_no=wafer_no,
                col=col_out,
                row=row_out,
                thickness=thickness,
                diced_loc=diced_loc,
                diced_dt=diced_dt.strftime('%Y-%m-%d'),
                result_code=result_code,
                test_date=test_date.strftime('%Y-%m-%d'),
                location=location,
                location_date=location_date.strftime('%Y-%m-%d'),
                IREF_TRIM=iref_trim,
                VREF_A_TRIM=vref_a_trim,
                VREF_D_TRIM=vref_d_trim,
                VCAL_SLOPE=vcal_high_slope,
                VCAL_OFFSET=vcal_high_offset,
                CAPVALfF=cap_val)
            f.write(command + '\n')
print("Saved to:", out_file)
