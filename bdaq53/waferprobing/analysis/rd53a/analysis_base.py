#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

from collections import OrderedDict


class TestBase(object):
    cuts = OrderedDict()

    def __init__(self, stage):
        self.stage = stage
        self.data, self.results = {}, {}

    def __str__(self):
        txt = ''
        for chip_sn, data in self.data.items():
            txt += str(chip_sn) + ': ' + str(data) + '\n'
        return txt

    def get_chips(self):
        return [key for key in self.data.keys()]

    def add_dataset(self, chip_sn, data):
        self.data[chip_sn] = data
        self.results[chip_sn] = self.evaluate(data, self.cuts)

    def evaluate(self, data, cuts):
        color = 'red'
        for c, ranges in cuts.items():
            for (lower, upper) in ranges:
                if data >= lower and data < upper:
                    color = c
        return color
