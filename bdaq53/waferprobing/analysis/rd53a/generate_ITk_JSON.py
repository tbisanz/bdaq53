#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script generates a JSON file to export waferprobing data to the ITk database
'''

import json
import yaml
import tables as tb
from datetime import datetime
import os
import shutil
from os import path

import bdaq53.analysis.analysis_utils as au

in_dir = ''
out_dir = ''

wafer_number = ''   # RD53 Wafer No.
wafer_id = ''       # TSMC Wafer ID
test_type = 'FECHIP_TEST'
institution = 'UNIBONN'

if int(wafer_number) < 112:
    test_names = ['IREF_TRIM', 'LINK', 'VDDA_TRIM', 'VDDD_TRIM', 'VREF_ADC_TRIM', 'I_VINA_CONF', 'I_VIND_CONF', 'I_TOT_CONF', 'NOCCSCAN', 'DIGITALSCAN', 'ANALOGSCAN', 'THRESHOLDSCAN']
    use_tests = ['IREF', 'VREF_ADC', 'VDDA', 'VDDD', 'I_TOT_conf', 'I_VINA_conf', 'I_VIND_conf', 'Link', 'NOccScan', 'DigitalScan', 'AnalogScan', 'ThresholdScan']
    alt_test_names = {'IREF': 'IREF_TRIM',
                      'VREF_ADC': 'VREF_ADC_TRIM',
                      'VDDA': 'VDDA_TRIM',
                      'VDDD': 'VDDD_TRIM'}
else:
    test_names = ['IREF_TRIM', 'LINK', 'VDDA_TRIM', 'VDDD_TRIM', 'VREF_ADC_TRIM', 'I_VINA_CONF', 'I_VIND_CONF', 'I_TOT_CONF', 'NOCCSCAN', 'DIGITALSCAN', 'ANALOGSCAN', 'THRESHOLDSCAN_MEAN']
    use_tests = ['IREF', 'VREF_ADC', 'VDDA', 'VDDD', 'I_TOT_conf', 'I_VINA_conf', 'I_VIND_conf', 'Link', 'NOccScan', 'DigitalScan', 'AnalogScan', 'ThresholdScan_mean']
    alt_test_names = {'IREF': 'IREF_TRIM',
                      'VREF_ADC': 'VREF_ADC_TRIM',
                      'VDDA': 'VDDA_TRIM',
                      'VDDD': 'VDDD_TRIM'}

if __name__ == '__main__':
    for f in os.listdir(in_dir):
        if '_results.h5' in f:
            in_file = path.join(in_dir, f)
            break

    with tb.open_file(in_file, 'r') as f:
        configuration = {key.decode('utf-8'): value.decode('utf-8') for (key, value) in f.root.configuration[:]}
        global_results = f.root.global_results[:]
        if int(wafer_number) < 112:
            trimbits = {chip_sn.decode('utf-8'): {'IREF': int(iref), 'VREF_A': int(vref_a), 'VREF_D': int(vref_d), 'VREF_ADC': int(vref_adc)} for chip_sn, iref, vref_a, vref_adc, vref_d in f.root.trims[:]}
        else:
            trimbits = {chip_sn.decode('utf-8'): {'IREF': int(iref), 'VREF_A': int(vref_a), 'VREF_D': int(vref_d), 'VREF_ADC': int(vref_adc)} for chip_sn, _, iref, _, vref_adc, _, vref_a, _, vref_d, _ in f.root.trims[:]}
        tests = {}
        for test in f.root.tests:
            tests[test.name] = test[:]

    wafer_no = int(global_results[0][0].decode('utf-8')[2:4], 16)
    out_file = path.join(out_dir, 'wafer_{0}_probing_results.json'.format(wafer_no))
    attachments_path = path.join(out_dir, 'attachments')

    # Get results from result h5 file
    data = {}
    data['testType'] = test_type
    data['institution'] = institution
    data['runNumber'] = wafer_number + '-' + datetime.strftime(datetime.strptime(configuration['Test date'], '%Y-%M-%d'), '%d%M%Y')
    data['date'] = datetime.strftime(datetime.strptime(configuration['Test date'], '%Y-%M-%d'), '%d.%M.%Y')
    data['Wafer_ID'] = wafer_id
    data['DAQ version'] = configuration['DAQ version']
    data['chips'] = {}

    for glob in global_results:
        chip = {}
        chip_sn = glob[0].decode('utf-8')
        res = glob[1].decode('utf-8')
        if res == 'green':
            passed = True
            problems = False
        elif res == 'yellow':
            passed = True
            problems = True
        else:
            passed = False
            problems = True

        chip['testType'] = test_type
        chip['institution'] = institution
        chip['runNumber'] = wafer_number + '-' + datetime.strftime(datetime.strptime(configuration['Test date'], '%Y-%M-%d'), '%d%M%Y')
        chip['date'] = datetime.strftime(datetime.strptime(configuration['Test date'], '%Y-%M-%d'), '%d.%M.%Y')
        chip['passed'] = passed
        chip['problems'] = problems
        chip['properties'] = {}
        chip['properties']['LOCAL_OBJECT_NAME'] = chip_sn
        chip['properties']['DAQ_VERSION'] = configuration['DAQ version']
        chip['results'] = {}
        chip['results']['OVERALL_RESULT'] = res
        if int(wafer_number) < 112:
            chip['results']['FAILED_TESTS'] = int(glob[2].decode('utf-8'))
        else:
            chip['results']['FAILED_TESTS'] = int(glob[2].decode('utf-8').split('(')[0].strip())
        try:
            chip['results']['IREF_TRIM'] = trimbits[chip_sn]['IREF']
            chip['results']['VDDA_TRIM'] = trimbits[chip_sn]['VREF_A']
            chip['results']['VDDD_TRIM'] = trimbits[chip_sn]['VREF_D']
            chip['results']['VREF_ADC_TRIM'] = trimbits[chip_sn]['VREF_ADC']
        except KeyError:
            pass
        data['chips'][chip_sn] = chip

        data['chips'][chip_sn]['attachments'] = []

    for test_name, test in tests.items():
        if test_name not in use_tests:
            continue
        if test_name in alt_test_names.keys():
            test_title = alt_test_names[test_name]
        else:
            test_title = test_name.upper()

        for cp in test:
            res = cp['Result'].decode('utf-8')
            if res == 'green':
                passed = True
                problems = False
            elif res == 'yellow':
                passed = True
                problems = True
            else:
                passed = False
                problems = True

            if int(wafer_number) < 112:
                chip_sn = cp['Chip_ID'].decode('utf-8')
            else:
                chip_sn = cp['Chip_SN'].decode('utf-8')
            data['chips'][chip_sn]['results'][test_title + '_RESULT'] = res
            data['chips'][chip_sn]['results'][test_title + '_VALUE'] = float(cp['Value'].decode('utf-8'))

    for f in os.listdir(in_dir):
        if '_interpreted.h5' in f:
            with tb.open_file(path.join(in_dir, f), 'r') as h5_file:
                run_config = au.ConfigDict(h5_file.root.configuration.run_config[:])
                this_chip_sn = '0x' + str(hex(run_config['chip_id']))[2:].upper()
                data['chips'][this_chip_sn]['attachments'].append(f)

    for chip_sn, chip in data['chips'].items():
        res = chip['results']
        was_changed = False
        for test_name in test_names:
            if test_name + '_RESULT' not in res.keys():
                print(test_name + ' is missing for chip ' + chip_sn)
                if not was_changed:
                    if test_name == 'IREF_TRIM':
                        chip['comment'] = 'Chip has a short and was not tested.'
                    elif test_name == 'Link':
                        if int(wafer_number) < 112:
                            if res['LINK_VALUE'] == 15.0:
                                chip['comment'] = 'Aurora sync could not be established. All digital tests failed.'
                        else:
                            if res['Value'] == 6.0:
                                chip['comment'] = 'Aurora sync could not be established. All digital tests failed.'
                    else:
                        chip['comment'] = 'Some tests did not yield results.'
                res[test_name + '_RESULT'] = 'red'
                res['FAILED_TESTS'] += 1
                was_changed = True
        if was_changed:
            res['FAILED_TESTS'] -= 1

        analog_file = path.join(path.join(in_dir, chip_sn), 'analog_data_' + chip_sn + '.yaml')
        try:
            with open(analog_file, 'r') as f:
                analog_data = yaml.safe_load(f)
        except FileNotFoundError as e:
            print('Chip {0} is missing file {1}'.format(chip_sn, e))

        try:
            if int(wafer_number) < 112:
                chip['results']['VDDA_16'] = analog_data['VREF_A Trim'][16]['VDDA']
            else:
                chip['results']['VDDA_16'] = analog_data['VREF_A Trim (LDO)'][16]['VDDA']
        except KeyError as e:
            print('Chip {0} is missing key {1}'.format(chip_sn, e))

        try:
            if int(wafer_number) < 112:
                chip['results']['VDDD_16'] = analog_data['VREF_D Trim'][16]['VDDD']
            else:
                chip['results']['VDDD_16'] = analog_data['VREF_D Trim (LDO)'][16]['VDDD']
        except KeyError as e:
            print('Chip {0} is missing key {1}'.format(chip_sn, e))

        try:
            chip['results']['IV_CURVES'] = {}
            I, VIN_A, VIN_D, VDDA, VDDD = [], [], [], [], []
            for cur, vals in analog_data['IV curves'].items():
                I.append(cur)
                VIN_A.append(vals['VIN_A'])
                VIN_D.append(vals['VIN_D'])
                VDDA.append(vals['VDDA'])
                VDDD.append(vals['VDDD'])

            chip['results']['IV_CURVES'] = {'I': I,
                                            'VIN_A': VIN_A,
                                            'VIN_D': VIN_D,
                                            'VDDA': VDDA,
                                            'VDDD': VDDD}
        except KeyError as e:
            print('Chip {0} is missing key {1}'.format(chip_sn, e))

    with open(out_file, 'w') as f:
        j = json.dumps(data, indent=4)
        print(j, file=f)

    # Collect attachments
    if path.isdir(attachments_path):
        shutil.rmtree(attachments_path)
    os.mkdir(attachments_path)
    for chip_sn, chip in data['chips'].items():
        os.mkdir(path.join(attachments_path, chip_sn))
        chip_dir = path.join(in_dir, chip_sn)
        for f in os.listdir(chip_dir):
            if chip_sn + '.yaml' in f:
                try:
                    shutil.copy(path.join(chip_dir, f), path.join(path.join(attachments_path, chip_sn), chip_sn + '.yaml'))
                except FileNotFoundError:
                    pass
                try:
                    shutil.copy(path.join(chip_dir, 'regulator_iv_curves.pdf'), path.join(path.join(attachments_path, chip_sn), 'iv_curve_' + chip_sn + '.pdf'))
                except FileNotFoundError:
                    pass
                break
        for f in chip['attachments']:
            shutil.copy(path.join(in_dir, f), path.join(path.join(attachments_path, chip_sn), path.basename(f)))
