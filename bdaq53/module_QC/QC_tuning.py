from bdaq53.scans.scan_digital import DigitalScan
from bdaq53.scans.scan_analog import AnalogScan
from bdaq53.scans.scan_threshold_fast import FastThresholdScan

from bdaq53.scans.tune_local_threshold import TDACTuning
from bdaq53.scans.tune_global_threshold import GDACTuning

from bdaq53.system import logger
import time

import yaml
from six import string_types
from bdaq53 import DEFAULT_TESTBENCH

pixel_range = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 384,
}


def QC_tuning(testbench=None, module_name='unknown'):
    log = logger.setup_derived_logger('QC TUNING: MODULE ' + module_name)

    # Use BDAQ default testbench if none is specified
    if testbench is None:
        log.info(f'No testbench specified! Using default: {DEFAULT_TESTBENCH}')
        testbench = DEFAULT_TESTBENCH
    # Open Testbench if it is a path
    if isinstance(testbench, string_types):
        log.info(f'Opening testbench: {DEFAULT_TESTBENCH}')
        with open(testbench) as f:
            testbench = yaml.full_load(f)

    scan_time = []
    scan_time_name = []

    scan_configuration = {
        'VCAL_MED': 500,
        'VCAL_HIGH_ANALOG': 1500,

        # Target threshold
        'VCAL_HIGH_TUNE_1': 1000,
        'VCAL_HIGH_TUNE_2': 740,
        # Prelim Threshold scan
        'threshold_prelim': {
            'VCAL_HIGH_start': 500,
            'VCAL_HIGH_stop': 1000,
            'VCAL_HIGH_step': 10
        },
        'threshold_final': {
            'VCAL_HIGH_start': 500,
            'VCAL_HIGH_stop': 800,
            'VCAL_HIGH_step': 5
        }
    }

    scan_config = pixel_range

    start_time = time.time()

    testbench['analysis']['blocking'] = False
    with DigitalScan(scan_config=scan_config, bench_config=testbench) as digital_scan:
        digital_scan.start()
    end_time = time.time()
    scan_time.append(end_time - start_time)
    scan_time_name.append('Digital Scan')

    scan_config['VCAL_MED'] = scan_configuration['VCAL_MED']
    scan_config['VCAL_HIGH'] = scan_configuration['VCAL_HIGH_ANALOG']

    start_time = time.time()
    testbench['analysis']['blocking'] = False
    with AnalogScan(scan_config=scan_config, bench_config=testbench) as analog_scan:
        analog_scan.start()
    end_time = time.time()
    scan_time.append(end_time - start_time)
    scan_time_name.append('Analog Scan')

    scan_config['VCAL_MED'] = scan_configuration['VCAL_MED']
    scan_config['VCAL_HIGH'] = scan_configuration['VCAL_HIGH_TUNE_1']

    start_time = time.time()
    testbench['analysis']['blocking'] = True
    with GDACTuning(scan_config=scan_config, bench_config=testbench, suffix='prelim') as global_tuning:
        global_tuning.start()

    end_time = time.time()
    scan_time.append(end_time - start_time)
    scan_time_name.append('Prelim Tuning')

    scan_config = pixel_range
    scan_config['VCAL_MED'] = scan_configuration['VCAL_MED']
    scan_config['VCAL_HIGH_start'] = scan_configuration['threshold_prelim']['VCAL_HIGH_start']
    scan_config['VCAL_HIGH_stop'] = scan_configuration['threshold_prelim']['VCAL_HIGH_stop']
    scan_config['VCAL_HIGH_step'] = scan_configuration['threshold_prelim']['VCAL_HIGH_step']

    start_time = time.time()
    testbench['analysis']['blocking'] = False
    with FastThresholdScan(scan_config=scan_config, bench_config=testbench, suffix='hr') as thr_scan:
        thr_scan.start()
    end_time = time.time()
    scan_time.append(end_time - start_time)
    scan_time_name.append('Untuned Threshold Scan')

    scan_config = pixel_range
    scan_config['VCAL_MED'] = scan_configuration['VCAL_MED']
    scan_config['VCAL_HIGH'] = scan_configuration['VCAL_HIGH_TUNE_1']

    start_time = time.time()
    testbench['analysis']['blocking'] = True
    with GDACTuning(scan_config=scan_config, bench_config=testbench, suffix='tune') as global_tuning:
        global_tuning.start()

    testbench['analysis']['blocking'] = True
    with TDACTuning(scan_config=scan_config, bench_config=testbench, suffix='tune') as local_tuning:
        local_tuning.start()
    end_time = time.time()
    scan_time.append(end_time - start_time)
    scan_time_name.append('Tuning 2000e')

    scan_config['VCAL_HIGH'] = scan_configuration['VCAL_HIGH_TUNE_2']

    start_time = time.time()
    testbench['analysis']['blocking'] = True
    with GDACTuning(scan_config=scan_config, bench_config=testbench, suffix='retune') as global_tuning:
        global_tuning.start()

    testbench['analysis']['blocking'] = True
    with TDACTuning(scan_config=scan_config, bench_config=testbench, suffix='retune') as local_tuning:
        local_tuning.start()
    end_time = time.time()
    scan_time.append(end_time - start_time)
    scan_time_name.append('Final Tuning')

    # Calculate scan range for threshold scan
    scan_config['VCAL_HIGH_start'] = scan_configuration['threshold_final']['VCAL_HIGH_start']
    scan_config['VCAL_HIGH_stop'] = scan_configuration['threshold_final']['VCAL_HIGH_stop']
    scan_config['VCAL_HIGH_step'] = scan_configuration['threshold_final']['VCAL_HIGH_step']

    start_time = time.time()
    testbench['analysis']['blocking'] = False
    with FastThresholdScan(scan_config=scan_config, bench_config=testbench, suffix='hd') as thr_scan:
        thr_scan.start()
    end_time = time.time()
    scan_time.append(end_time - start_time)
    scan_time_name.append('Final Threshold Scan')

    for i in range(len(scan_time)):
        log.info("%s: %d min %d s" % (scan_time_name[i], scan_time[i] // 60, scan_time[i] % 60))
