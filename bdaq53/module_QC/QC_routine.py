from bdaq53.scans.scan_efuse import EFuseScan
from bdaq53.scans.scan_analog_readback import AnalogReadbackScan
from bdaq53.scans.scan_trim_vdd import TrimScan
from bdaq53.scans.scan_dac_linearity_itkpixv1 import DACScan
from bdaq53.scans.scan_SLDO_IV import SLDO_IV_Scan
from bdaq53.scans.scan_inj_cap import Inj_Cap_Meas
from bdaq53.scans.calibrate_Vcal import VcalCalibration
from bdaq53.scans.scan_NTC_analog_read import NTCReadout
from bdaq53.scans.scan_LOW_POWER import LOW_POWER_Scan
from bdaq53.scans.scan_digital import DigitalScan
from bdaq53.scans.scan_analog import AnalogScan
from bdaq53.scans.scan_threshold import ThresholdScan
from bdaq53.scans.scan_digital_lp import LpDigitalScan
from bdaq53.scans.scan_OVP import OVP_Scan
from bdaq53.scans.tune_global_threshold import GDACTuning
from bdaq53.scans.tune_local_threshold import TDACTuning
from bdaq53.scans.scan_noise_occupancy import NoiseOccScan


from bdaq53.system.periphery import BDAQ53Periphery
from bdaq53.system import logger

from bdaq53 import DEFAULT_TESTBENCH
from bdaq53 import utils as bu

from bdaq53.module_QC.QC_test import QC_Test
from bdaq53.module_QC.QC_plotting import plot_QC_results

from six import string_types

import yaml
import json
import os
import time
import collections
from colorama import Fore, Style

QC_CONFIG_FILE = os.path.abspath(os.path.join(os.path.dirname(__file__), 'QC_config_files/module_QC_config.yaml'))

# FIXME: use layer dependend input current for LP mode
# Put test type into QC config file, too. Set input current depending on test. Same with nominal input current
LP_MODE_INPUT_CURRENT = 2.012

pass_or_fail_dict = {
    True: [Fore.GREEN, "PASSED" + Style.RESET_ALL],
    False: [Fore.RED, "FAILED" + Style.RESET_ALL]
}

QC_Tasks = {'EFuse': {'scan': EFuseScan},
            'ADC_Cal': {'scan': DACScan, 'config': 'DACScan'},
            'AR_Trim': {'scan': TrimScan, 'config': 'TrimScan'},
            'Analog_Read': {'scan': AnalogReadbackScan, 'config': 'AnalogReadbackScan'},
            'AR_NTC': {'scan': NTCReadout, 'config': 'NTCReadout'},
            'SLDO': {'scan': SLDO_IV_Scan, 'config': 'SLDO_IV'},
            'QC_LP_MODE': {'scan': LOW_POWER_Scan, 'config': 'LOW_POWER'},
            'DIGITAL_LP_MODE': {'scan': DigitalScan, 'config': 'DIGITAL_LOW_POWER'},
            'OVP': {'scan': OVP_Scan, 'config': 'OVERVOLTAGE_PROTECTION'},
            'INJ_CAP': {'scan': Inj_Cap_Meas, 'config': 'Inj_Cap'},
            'VCAL_Cal': {'scan': TrimScan, 'config': 'TrimScan'}}


def _add_scan_time(scan_time, scan_time_name, start_time, test_name):
    end_time = time.time()
    scan_time.append(end_time - start_time)
    scan_time_name.append(test_name)

    return scan_time, scan_time_name


def _update_json_config(filename, config_dict):
    with open(filename, "r+") as json_file:
        data = json.load(json_file)
        bu.recursive_update(data, config_dict)
        json_file.seek(0)
        json.dump(data, json_file)
        json_file.truncate()


def QC_routine(periphery=None, QC_configuration=QC_CONFIG_FILE, module_name='unknown', chip_type='ITkPixV1', test_type='L2_warm', testbench=None):
    log = logger.setup_derived_logger('QC ROUTINE: MODULE ' + module_name)

    QC_results = [[], [], [], []]
    QC_tests = []
    scan_time = []
    scan_time_name = []
    chips = ['GA1 \n', 'GA2 \n', 'GA3 \n', 'GA4 \n']

    scan_time_begin = time.time()
    # Open QC configuration if it is a path
    if isinstance(QC_configuration, string_types):  # FIXME: I think this can be done without string_types (one dependency less). This is needed for python 2 compatibility - is that required?
        log.info(f'Opening QC configuration: {QC_configuration}')
        with open(QC_configuration, 'r') as QC_config_file:
            QC_configuration = yaml.safe_load(QC_config_file)

    # Use BDAQ default testbench if none is specified
    if testbench is None:
        log.info(f'No testbench specified! Using default: {DEFAULT_TESTBENCH}')
        testbench = DEFAULT_TESTBENCH

    # Open Testbench if it is a path
    if isinstance(testbench, string_types):
        log.info(f'Opening testbench: {testbench}')
        with open(testbench) as f:
            testbench = yaml.full_load(f)

    # output folder for config
    CONFIG_PATH = testbench['general']['output_directory']

    # Get module configuration
    if module_name not in testbench['modules']:
        module_name = next(iter(testbench['modules']))

    # Get Yarr config from DB and convert to BDAQ
    module_id = testbench['modules'][module_name]['identifier']
    if os.path.exists(CONFIG_PATH + '/' + module_id):  # config exist, do not generate new
        log.warning('Module configuration exists already ({})!'.format(CONFIG_PATH))
    else:
        os.system(f'generateYARRConfig -sn {module_id} -o {CONFIG_PATH}')
    for k, v in testbench['modules'][module_name].items():  # convert config for each chip
        if isinstance(v, collections.abc.Mapping) and 'chip_sn' in v:
            chip_sn = v['chip_sn']
            config_path = os.path.join(CONFIG_PATH, module_id, test_type, chip_sn.lower() + '_' + test_type)
            if os.system(f'bdaq_config convert -i {config_path}.json --use_trimbit_registers --use_afe_registers --use_parameters') == 0:
                v['chip_config_file'] = config_path + '.cfg.yaml'
                log.info(f'Setting CONFIG FILE for chip {k} to {v["chip_config_file"]}')

    startup_config = QC_configuration['scan_configs']['First_StartUp']

    if not periphery:
        periphery = BDAQ53Periphery(bench_config=testbench, name='Module QC')
        periphery.init()

    if startup_config['periphery_device'] == 'Powersupply':
        dev = periphery.module_devices[module_name]['LV']
    else:
        dev = periphery.aux_devices[startup_config['periphery_device']]

    # Start up test. Measure input voltage and current after start up.
    V_in_prestart = dev.get_voltage()
    I_in_prestart = periphery.module_devices[module_name]['LV'].get_current()

    with QC_Test() as QC:
        paramerters = startup_config['QC_criteria']
        paramerters['design_value'] = paramerters.get('design_voltage', 1.493) + I_in_prestart * paramerters.get('voltage_drop', 0.045)
        pass_startup = QC.QC_test('Vin Startup', paramerters, V_in_prestart)

    for item in QC_results:
        item.append(pass_startup)
    QC_tests.append('Power Up')

    # Test Efuses
    start_time = time.time()
    test_name = 'Efuse'
    with EFuseScan(bench_config=testbench) as scan:
        scan.start()
        # QC analysis
        for chip in scan.chips.values():
            if hasattr(chip.chip, "QC_passed"):
                qc_pass = chip.chip.QC_passed
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + pass_or_fail_dict[qc_pass][0] + test_name + pass_or_fail_dict[qc_pass][1])
            else:
                qc_pass = -1
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + Fore.YELLOW + test_name + "NOT FOUND" + Style.RESET_ALL)

            chips[int(chip.name[-1]) - 1] = chips[int(chip.name[-1]) - 1] + chip.chip.chip_sn
            QC_results[int(chip.name[-1]) - 1].append(chip.chip.QC_passed)
        QC_tests.append(test_name)
        for chip in QC_results:
            if len(chip) < max(len(QC_results[0]), len(QC_results[1]), len(QC_results[2]), len(QC_results[3])):
                chip.append(-1)
    # Scan timing
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    # Calibrate ADC
    start_time = time.time()
    test_name = 'ADC cal'
    with DACScan(scan_config=QC_configuration['scan_configs']['DACScan'], bench_config=testbench) as scan:
        adc_cal = scan.start()
        # QC analysis
        for chip in scan.chips.values():
            if hasattr(chip.chip, "QC_passed"):
                qc_pass = chip.chip.QC_passed
                log.info(
                    'Chip %s: ' % chip.chip_settings['chip_sn'].upper() + pass_or_fail_dict[qc_pass][0] + test_name +
                    pass_or_fail_dict[qc_pass][1])
            else:
                qc_pass = -1
                log.info('Chip %s: ' % chip.chip_settings[
                    'chip_sn'].upper() + Fore.YELLOW + test_name + "NOT FOUND" + Style.RESET_ALL)

            QC_results[int(chip.name[-1]) - 1].append(chip.chip.QC_passed)
        QC_tests.append(test_name)
        for chip in QC_results:
            if len(chip) < max(len(QC_results[0]), len(QC_results[1]), len(QC_results[2]), len(QC_results[3])):
                chip.append(-1)
    # Scan timing
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    # Update .json files with new config values from ADC calibration
    chip_idx = 0  # chip count
    for k, v in testbench['modules'][module_name].items():
        if isinstance(v, collections.abc.Mapping) and 'chip_sn' in v:
            adc_slope, adc_offset = adc_cal[chip_idx][0], adc_cal[chip_idx][1]
            adc_dict = {"RD53B": {"Parameter": {"ADCcalPar": [adc_offset, adc_slope, 10000.0]}}}  # FIXME: what is this 10000
            chip_sn = v['chip_sn']
            # Update actual test type config
            config_path = os.path.join(CONFIG_PATH, module_id, test_type, chip_sn.lower() + '_' + test_type)
            _update_json_config(filename=config_path + '.json', config_dict=adc_dict)
            # Update LP mode config
            config_path = os.path.join(CONFIG_PATH, module_id, test_type[:2] + '_LP', chip_sn.lower() + '_' + test_type[:2] + '_LP')
            _update_json_config(filename=config_path + '.json', config_dict=adc_dict)
            chip_idx += 1

    # Reset chip config file after first scan to None such that always the latest config is used.
    for k, v in testbench['modules'][module_name].items():
        if isinstance(v, collections.abc.Mapping) and 'chip_sn' in v:
            v['chip_config_file'] = ''
            log.info(f'Resetting CONFIG FILE for chip {k}')

    # SLDO trimming
    start_time = time.time()
    test_name = 'SLDO trimming'
    with TrimScan(scan_config=QC_configuration['scan_configs']['TrimScan'], bench_config=testbench) as scan:
        trim_ret = scan.start()

    QC_tests.append(test_name)
    for chip in QC_results:
        chip.append(True)
    # Scan timing
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    # Update .json files with new config values from VDD trim
    chip_idx = 0  # chip count
    for k, v in testbench['modules'][module_name].items():
        if isinstance(v, collections.abc.Mapping) and 'chip_sn' in v:
            vddd_trim, vdda_trim = trim_ret[chip_idx][0], trim_ret[chip_idx][1]
            trim_dict = {"RD53B": {"GlobalConfig": {"SldoTrimA": int(vdda_trim), "SldoTrimD": int(vddd_trim)}}}
            chip_sn = v['chip_sn']
            # Update actual test type config
            config_path = os.path.join(CONFIG_PATH, module_id, test_type, chip_sn.lower() + '_' + test_type)
            _update_json_config(filename=config_path + '.json', config_dict=trim_dict)
            # Update LP mode config
            config_path = os.path.join(CONFIG_PATH, module_id, test_type[:2] + '_LP', chip_sn.lower() + '_' + test_type[:2] + '_LP')
            _update_json_config(filename=config_path + '.json', config_dict=trim_dict)
            chip_idx += 1

    # Analog readback
    start_time = time.time()
    test_name = 'AnalogRead'
    with AnalogReadbackScan(scan_config=QC_configuration['scan_configs']['AnalogReadbackScan'], bench_config=testbench) as scan:
        scan.start()
        # QC analysis
        for chip in scan.chips.values():
            if hasattr(chip.chip, "QC_passed"):
                qc_pass = chip.chip.QC_passed
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + pass_or_fail_dict[qc_pass][0] + test_name + pass_or_fail_dict[qc_pass][1])
            else:
                qc_pass = -1
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + Fore.YELLOW + test_name + "NOT FOUND" + Style.RESET_ALL)

            QC_results[int(chip.name[-1]) - 1].append(chip.chip.QC_passed)
        QC_tests.append(test_name)
        for chip in QC_results:
            if len(chip) < max(len(QC_results[0]), len(QC_results[1]), len(QC_results[2]), len(QC_results[3])):
                chip.append(-1)
    # Scan timing
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    test_name = "AnalogReadNTC"
    start_time = time.time()
    with NTCReadout(scan_config=QC_configuration['scan_configs']['NTCReadout'], bench_config=testbench) as scan:
        scan.start()
        # QC analysis
        for chip in scan.chips.values():
            if hasattr(chip.chip, "QC_passed"):
                qc_pass = chip.chip.QC_passed
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + pass_or_fail_dict[qc_pass][0] + test_name + pass_or_fail_dict[qc_pass][1])
            else:
                qc_pass = -1
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + Fore.YELLOW + test_name + "NOT FOUND" + Style.RESET_ALL)

            QC_results[int(chip.name[-1]) - 1].append(chip.chip.QC_passed)
        QC_tests.append(test_name)
        for chip in QC_results:
            if len(chip) < max(len(QC_results[0]), len(QC_results[1]), len(QC_results[2]), len(QC_results[3])):
                chip.append(-1)
    # Scan timing
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    # SLDO Scan
    start_time = time.time()
    test_name = 'SLDO scan'
    with SLDO_IV_Scan(scan_config=QC_configuration['scan_configs']['SLDO_IV'], bench_config=testbench) as scan:
        scan.start()
        # QC analysis
        for chip in scan.chips.values():
            if hasattr(chip.chip, "QC_passed"):
                qc_pass = chip.chip.QC_passed
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + pass_or_fail_dict[qc_pass][0] + test_name + pass_or_fail_dict[qc_pass][1])
            else:
                qc_pass = -1
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + Fore.YELLOW + test_name + "NOT FOUND" + Style.RESET_ALL)

            QC_results[int(chip.name[-1]) - 1].append(chip.chip.QC_passed)
        QC_tests.append(test_name)
        for chip in QC_results:
            if len(chip) < max(len(QC_results[0]), len(QC_results[1]), len(QC_results[2]), len(QC_results[3])):
                chip.append(-1)
    # Scan timing
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    # InjCap measurement
    start_time = time.time()
    test_name = 'C_inj'
    with Inj_Cap_Meas(scan_config=QC_configuration['scan_configs']['Inj_Cap'], bench_config=testbench) as scan:
        c_inj = scan.start()
        # QC analysis
        for chip in scan.chips.values():
            if hasattr(chip.chip, "QC_passed"):
                qc_pass = chip.chip.QC_passed
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + pass_or_fail_dict[qc_pass][0] + test_name + pass_or_fail_dict[qc_pass][1])
            else:
                qc_pass = -1
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + Fore.YELLOW + test_name + "NOT FOUND" + Style.RESET_ALL)

            QC_results[int(chip.name[-1]) - 1].append(chip.chip.QC_passed)
        QC_tests.append(test_name)
        for chip in QC_results:
            if len(chip) < max(len(QC_results[0]), len(QC_results[1]), len(QC_results[2]), len(QC_results[3])):
                chip.append(-1)
    # Scan timing
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    # Update .json files with new config values from InjCap measurement
    chip_idx = 0  # chip count
    for k, v in testbench['modules'][module_name].items():
        if isinstance(v, collections.abc.Mapping) and 'chip_sn' in v:
            c_inj_dict = {"RD53B": {"Parameter": {"InjCap": c_inj[chip_idx] * 1e15}}}
            chip_sn = v['chip_sn']
            # Update actual test type config
            config_path = os.path.join(CONFIG_PATH, module_id, test_type, chip_sn.lower() + '_' + test_type)
            _update_json_config(filename=config_path + '.json', config_dict=c_inj_dict)
            # Update LP mode config
            config_path = os.path.join(CONFIG_PATH, module_id, test_type[:2] + '_LP',
                                       chip_sn.lower() + '_' + test_type[:2] + '_LP')
            _update_json_config(filename=config_path + '.json', config_dict=c_inj_dict)
            chip_idx += 1

    # Vcal Calibration
    start_time = time.time()
    test_name = 'VCAL cal'
    with VcalCalibration(scan_config=QC_configuration['scan_configs']['Vcal_Cal'], bench_config=testbench) as scan:
        vcal_par = scan.start()
        # QC analysis
        for chip in scan.chips.values():
            if hasattr(chip.chip, "QC_passed"):
                qc_pass = chip.chip.QC_passed
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + pass_or_fail_dict[qc_pass][0] + test_name + pass_or_fail_dict[qc_pass][1])
            else:
                qc_pass = -1
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + Fore.YELLOW + test_name + "NOT FOUND" + Style.RESET_ALL)

            QC_results[int(chip.name[-1]) - 1].append(chip.chip.QC_passed)
        QC_tests.append(test_name)
        for chip in QC_results:
            if len(chip) < max(len(QC_results[0]), len(QC_results[1]), len(QC_results[2]), len(QC_results[3])):
                chip.append(-1)
    # Scan timing
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    # Update.json files with new config values from Vcal Calibration
    chip_idx = 0  # chip count
    for k, v in testbench['modules'][module_name].items():
        if isinstance(v, collections.abc.Mapping) and 'chip_sn' in v:
            vcal_par_dict = {"RD53B": {"Parameter": {"VcalPar": [vcal_par[chip_idx][0], vcal_par[chip_idx][1]]}}}
            chip_sn = v['chip_sn']
            # Update actual test type config
            config_path = os.path.join(CONFIG_PATH, module_id, test_type, chip_sn.lower() + '_' + test_type)
            _update_json_config(filename=config_path + '.json', config_dict=vcal_par_dict)
            # Update LP mode config
            config_path = os.path.join(CONFIG_PATH, module_id, test_type[:2] + '_LP',
                                       chip_sn.lower() + '_' + test_type[:2] + '_LP')
            _update_json_config(filename=config_path + '.json', config_dict=vcal_par_dict)
            chip_idx += 1

    # Convert LP config to bdaq format and set it as chip config
    for k, v in testbench['modules'][module_name].items():
        if isinstance(v, collections.abc.Mapping) and 'chip_sn' in v:
            chip_sn = v['chip_sn']
            config_path = os.path.join(CONFIG_PATH, module_id, test_type[:2] + '_LP', chip_sn.lower() + '_' + test_type[:2] + '_LP')
            if os.system(f'bdaq_config convert -i {config_path}.json --use_trimbit_registers --use_afe_registers --use_parameters') == 0:
                v['chip_config_file'] = config_path + '.cfg.yaml'
                log.info(f'Setting CONFIG FILE for chip {k} to {v["chip_config_file"]}')
    # Set chip config to LP mode config and change folder for LP test, do not want to use LP config later on anymore
    testbench['general']['output_directory'] = testbench['general']['output_directory'] + '/LP_mode'
    testbench['hardware']['LP_enable'] = True  # enable LP mode

    # Low power mode test
    start_time = time.time()
    test_name = 'LP mode test'
    with LOW_POWER_Scan(scan_config=QC_configuration['scan_configs']['LOW_POWER'], bench_config=testbench) as scan:
        scan.start()
        # QC analysis
        for chip in scan.chips.values():
            if hasattr(chip.chip, "QC_passed"):
                qc_pass = chip.chip.QC_passed
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + pass_or_fail_dict[qc_pass][0] + test_name + pass_or_fail_dict[qc_pass][1])
            else:
                qc_pass = -1
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + Fore.YELLOW + test_name + "NOT FOUND" + Style.RESET_ALL)

            QC_results[int(chip.name[-1]) - 1].append(chip.chip.QC_passed)
        QC_tests.append(test_name)
        for chip in QC_results:
            if len(chip) < max(len(QC_results[0]), len(QC_results[1]), len(QC_results[2]), len(QC_results[3])):
                chip.append(-1)
    # Scan timing
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    # Low power mode digital scan
    start_time = time.time()
    test_name = 'LP digital scan'
    # Set LP input current: FIXME: we need to do this layer dependent
    testbench['modules'][module_name]['powersupply']['lv_current_limit'] = LP_MODE_INPUT_CURRENT

    with LpDigitalScan(scan_config=QC_configuration['scan_configs']['DIGITAL_LOW_POWER'], bench_config=testbench) as scan_digit:
        scan_digit.start()
    # Scan timing
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    # Reset current to nominal one
    testbench['modules'][module_name]['powersupply']['lv_current_limit'] = I_in_prestart  # FIXME: use set value, not measured
    testbench['general']['output_directory'] = os.path.split(testbench['general']['output_directory'])[0]

    # Use regular chip config
    for k, v in testbench['modules'][module_name].items():
        if isinstance(v, collections.abc.Mapping) and 'chip_sn' in v:
            v['chip_config_file'] = ''

    # Over voltage pprotection test
    start_time = time.time()
    test_name = 'OVP test'
    with OVP_Scan(scan_config=QC_configuration['scan_configs']['OVERVOLTAGE_PROTECTION'], bench_config=testbench) as scan:
        scan.start()
        # QC analysis
        for chip in scan.chips.values():
            if hasattr(chip.chip, "QC_passed"):
                qc_pass = chip.chip.QC_passed
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + pass_or_fail_dict[qc_pass][0] + test_name + pass_or_fail_dict[qc_pass][1])
            else:
                qc_pass = -1
                log.info('Chip %s: ' % chip.chip_settings['chip_sn'].upper() + Fore.YELLOW + test_name + "NOT FOUND" + Style.RESET_ALL)

            QC_results[int(chip.name[-1]) - 1].append(chip.chip.QC_passed)
        QC_tests.append(test_name)
        for chip in QC_results:
            if len(chip) < max(len(QC_results[0]), len(QC_results[1]), len(QC_results[2]), len(QC_results[3])):
                chip.append(-1)
    # Scan timing
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    # Disable LP mode
    testbench['hardware']['LP_enable'] = False

    # why dips in module NTC?
    # Minimum healt test
    # FIXME: put this in separate script
    # TODO: do we need to update json after tuning etc?
    start_time = time.time()
    test_name = 'Minimum Health: Digital'
    with DigitalScan(scan_config=QC_configuration['scan_configs']['MHT_digital_scan'], bench_config=testbench) as scan_digital:
        scan_digital.start()
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    start_time = time.time()
    test_name = 'Minimum Health: Analog'
    with AnalogScan(scan_config=QC_configuration['scan_configs']['MHT_analog_scan'], bench_config=testbench) as scan_analog:
        scan_analog.start()
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    start_time = time.time()
    test_name = 'Minimum Health: Threshold'
    # Use non blocking analysis
    testbench['analysis']['blocking'] = False
    with ThresholdScan(scan_config=QC_configuration['scan_configs']['MHT_thresholdscan_hr'], bench_config=testbench, suffix='_hr') as scan_threshold:
        scan_threshold.scan_id = 'threshold_scan_hr'  # FIXME: change plotting!!
        scan_threshold.start()
    testbench['analysis']['blocking'] = True
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Minimum Health: Analog tot'
    # TODO: calculate DVCAL from e-conversion, per chip!! (should be possible already)
    with AnalogScan(scan_config=QC_configuration['scan_configs']['MHT_tot_scan'], bench_config=testbench, suffix='_tot') as scan_analog:
        scan_analog.scan_id = 'analog_scan_tot'
        scan_analog.start()

    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Tuning: global 2000e'

    # Tuning performance test
    # TODO: how to differentiate if scan belongs to MHT, TUN or whatever? -> upload data to DB after every scan with specific tag should solve that issue
    # TODO: calculate DVCAL from e-conversion, per chip!! (should be possible already)
    with GDACTuning(scan_config=QC_configuration['scan_configs']['TUN_global_threshold_2000'], bench_config=testbench) as tune_global:
        tune_global.start()

    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Tuning: Threshold hr'

    # Use non blocking analysis
    testbench['analysis']['blocking'] = False
    with ThresholdScan(scan_config=QC_configuration['scan_configs']['TUN_thresholdscan_hr'], bench_config=testbench, suffix='_hr') as scan_threshold:
        scan_threshold.scan_id = 'threshold_scan_hr'
        scan_threshold.start()
    testbench['analysis']['blocking'] = True

    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Tuning: Analog tot'
    # TODO: calculate DVCAL from e-conversion, per chip!! (should be possible already)
    with AnalogScan(scan_config=QC_configuration['scan_configs']['TUN_tot_scan'], bench_config=testbench, suffix='_tot') as scan_analog:
        scan_analog.scan_id = 'analog_scan_tot'
        scan_analog.start()

    # TODO: add TOT tuning
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Tuning: global 2000e'
    # TODO: calculate DVCAL from e-conversion, per chip!! (should be possible already)
    with GDACTuning(scan_config=QC_configuration['scan_configs']['TUN_global_threshold_2000'], bench_config=testbench) as tune_global:
        tune_global.start()
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Tuning: local 2000e'
    # TODO: calculate DVCAL from e-conversion, per chip!! (should be possible already)
    with TDACTuning(scan_config=QC_configuration['scan_configs']['TUN_local_threshold_2000'], bench_config=testbench) as tune_local:
        tune_local.start()
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Tuning: global 1500e'
    # TODO: calculate DVCAL from e-conversion, per chip!! (should be possible already)
    with GDACTuning(scan_config=QC_configuration['scan_configs']['TUN_global_threshold_1000'], bench_config=testbench) as tune_global:
        tune_global.start()
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Tuning: local 1500e'
    # TODO: calculate DVCAL from e-conversion, per chip!! (should be possible already)
    with TDACTuning(scan_config=QC_configuration['scan_configs']['TUN_local_threshold_1000'], bench_config=testbench) as tune_local:
        tune_local.start()

    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Threshold: hd'
    # Use non blocking analysis
    testbench['analysis']['blocking'] = False
    with ThresholdScan(scan_config=QC_configuration['scan_configs']['TUN_thresholdscan_hd'], bench_config=testbench, suffix='_hd') as scan_threshold:
        scan_threshold.scan_id = 'threshold_scan_hd'
        scan_threshold.start()
    testbench['analysis']['blocking'] = True

    # TODO: add in-time threshold scan

    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Pixel Analysis: Analog tot'
    # TODO: calculate DVCAL from e-conversion, per chip!! (should be possible already)
    with AnalogScan(scan_config=QC_configuration['scan_configs']['TUN_tot_scan'], bench_config=testbench, suffix='_tot') as scan_analog:
        scan_analog.scan_id = 'analog_scan_tot'
        scan_analog.start()

    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Pixel Analysis: Digital'
    # Pixel failure analysis test
    # TODO: how to differentiate if scan belongs to MHT, TUN or whatever? -> upload data to DB after every scan with specific tag should solve that issue
    # FIXME: check what clear mask means (L582 in QC doc))
    with DigitalScan(scan_config=QC_configuration['scan_configs']['PFA_digital_scan'], bench_config=testbench) as scan_digital:
        scan_digital.start()

    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Pixel Analysis: Analog'
    with AnalogScan(scan_config=QC_configuration['scan_configs']['PFA_analog_scan'], bench_config=testbench) as scan_analog:
        scan_analog.start()

    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Pixel Analysis: Threshold'
    # Use non blocking analysis
    testbench['analysis']['blocking'] = False
    with ThresholdScan(scan_config=QC_configuration['scan_configs']['PFA_thresholdscan_hd'], bench_config=testbench, suffix='_hd') as scan_threshold:
        scan_threshold.scan_id = 'threshold_scan_hd'
        scan_threshold.start()
    testbench['analysis']['blocking'] = True

    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)
    start_time = time.time()

    test_name = 'Pixel Analysis: Noise'
    with NoiseOccScan(scan_config=QC_configuration['scan_configs']['PFA_noise_scan'], bench_config=testbench) as scan:
        scan.start()
    scan_time, scan_time_name = _add_scan_time(scan_time, scan_time_name, start_time, test_name)

    # TODO: add Tot memory scan

    print(QC_results)
    print(chips)
    print(QC_tests)

    plot_QC_results(QC_results, Chips=chips, Tests=QC_tests, ModuleName=module_name, chip_type=chip_type,
                    test_type=test_type, output_path=testbench['general']['output_directory'])

    # Scan time overview
    for i in range(len(scan_time)):
        log.info("%s: %d min %d s" % (scan_time_name[i], scan_time[i] // 60, scan_time[i] % 60))
    log.info("Total Scan Time: %d min %d s" % (scan_time[-1] - scan_time_begin // 60, scan_time[-1] - scan_time_begin % 60))
    periphery.close()


if __name__ == "__main__":
    qc_config_file = '../module_QC/QC_config_files/module_QC_config.yaml'
    testbench_file = '../testbench_preprod_4.yaml'
    with open(qc_config_file, 'r') as QC_config_file:
        QC_configuration = yaml.safe_load(QC_config_file)
    with open(testbench_file, 'r') as testbench_f:
        testbench = yaml.safe_load(testbench_f)

    for module in testbench['modules'].keys():
        QC_routine(QC_configuration=QC_configuration, testbench=testbench, module_name=module)
