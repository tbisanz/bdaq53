import seaborn as sns
from matplotlib import colors
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

import os

DEFAULT_OUTPUT_PATH = './output/'


def plot_QC_results(Results, Chips=['Chip', 'Chip GA2', 'Chip GA3', 'Chip GA4'],
                    Tests=['Power Up', 'Efuse', 'Analog Readback', 'SLDO IV'],
                    ModuleName='Module', chip_type='ITkPixV1', test_type='L2', output_path=DEFAULT_OUTPUT_PATH):
    cmap = colors.ListedColormap(['grey', 'red', 'green'])
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)
    ax = sns.heatmap(Results, vmin=-1, vmax=1, cmap=cmap, linewidth=0.25, cbar_kws={'orientation': 'horizontal', 'ticks': [-0.66, 0, 0.66], 'fraction': 0.1, 'shrink': 0.4, 'anchor': (1, 0.5)}, ax=ax)

    ax.tick_params(axis='x', length=0, labelrotation=45, labelbottom=True)
    ax.set_ylabel(ModuleName)
    ax.set_yticklabels(Chips)
    ax.set_xticklabels(Tests)

    ax.collections[0].colorbar.set_ticklabels(['Unknown', 'Failed', 'Passed'])

    ax.set_title('QC Results')
    fig.tight_layout()
    _add_text(fig, ModuleName, chip_type=chip_type, test_type=test_type)
    _save_plots(fig, ModuleName, output_path=output_path)


def _add_text(fig, module_name, chip_type='ITkPixV1', test_type='L2'):
    fig.subplots_adjust(top=0.85)
    y_coord = 0.92
    fig.text(0.1, y_coord, '{0} {1}'.format(chip_type, test_type), fontsize=12, transform=fig.transFigure)

    module_text = 'Module: {0}'.format(module_name)
    fig.text(0.7, y_coord, module_text, fontsize=12, transform=fig.transFigure)


def _save_plots(fig, module_name, output_path, suffix=None, tight=False, save_png=False, save_pdf=True):
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    filename = 'QC_results_' + module_name

    if suffix is not None:
        filename = filename + str(suffix)
    suffix = 0

    file = os.path.join(output_path, filename)
    file_c = file
    while os.path.isfile(file_c + '.pdf') or os.path.isfile(file_c + '.png'):
        file_c = file + '_' + str(suffix)
        suffix += 1

    file = file_c
    bbox_inches = 'tight' if tight else ''

    if save_png:
        fig.savefig(file + '.png', bbox_inches=bbox_inches)
    if save_pdf:
        fig.savefig(file + '.pdf', bbox_inches=bbox_inches)
