import os
import json

import tables as tb
import numpy as np

from bdaq53.manage_configuration import convert_config_bdaq_to_yarr
from bdaq53.system.scan_base import fill_dict_from_conf_table
from bdaq53.analysis import analysis_utils as au

# FIXME: Find a better solution for import of internal modules so this script can be called via CLI from both outside and within the package
try:
    from bdaq53.system import logger
except ImportError:
    import logger

log = logger.setup_derived_logger('File Converter')

n_cols = 400
n_rows = 384

# Specify what is required for each testType
results_from_scans = {
    "std_digitalscan": ["EnMask", "OccupancyMap", "MeanTotMap",
                        "MeanTotDist", "MeanTagMap", "TagDist",
                        "SigmaTotMap", "SigmaTotDist", "Config_before", "Config_after"],
    "std_analogscan": ["EnMask", "OccupancyMap", "MeanTotMap",
                       "MeanTotDist", "MeanTagMap", "TagDist",
                       "SigmaTotMap", "SigmaTotDist", "Config_before", "Config_after"],
    "std_thresholdscan_hr": ["Chi2Map", "Chi2Dist", "InjVcalDiff", "InjVcalDiff_Map",
                             "ThresholdMap", "ThresholdDist", "NoiseMap", "NoiseDist",
                             "Config_before", "Config_after"],  # FIXME: add StatusDist, StatusMap, TimePerFitDist
    "std_thresholdscan_hd": ["Chi2Map", "ThresholdMap", "NoiseMap", "Config_before", "Config_after"],
    "std_noisescan": ["NoiseOccupancy", "Config_before", "Config_after"],
    "std_totscan": ["EnMask", "OccupancyMap", "MeanTotMap",
                    "MeanTotDist", "MeanTagMap", "TagDist",
                    "SigmaTotMap", "SigmaTotDist", "Config_before", "Config_after"],
    "std_discbumpscan": ["OccupancyMap"],
}


def _calculate_bins(data):
    diff = np.amax(data) - np.amin(data)
    median = np.ma.median(data)
    if (np.amax(data)) > median * 5:
        plot_range = np.arange(np.amin(data), median * 2, median / 100.)
    else:
        plot_range = np.arange(np.amin(data), np.amax(data) + diff / 100., diff / 100.)

    return plot_range


def _convert_chip_sn(chip_sn):
    ''' Converts chip S/N (0x....) to ATLAS S/N (20PGFC).
    '''
    return '20UPGFC{0:07d}'.format(int(chip_sn, 16))


def _convert_to_e(dac, chip_config, use_offset=True):
    if use_offset:
        e = dac * chip_config["calibration"]["e_conversion_slope"] + chip_config["calibration"]["e_conversion_offset"]
        de = np.sqrt((dac * chip_config["calibration"]["e_conversion_slope_error"]) ** 2 + chip_config["calibration"]["e_conversion_offset_error"] ** 2)
    else:
        e = dac * chip_config["calibration"]["e_conversion_slope"]
        de = dac * chip_config["calibration"]["e_conversion_slope_error"]
    return e, de


def _load_chip_config(node):
    chip_config = au.ConfigDict()
    chip_config["settings"] = fill_dict_from_conf_table(node.settings)
    chip_config["calibration"] = fill_dict_from_conf_table(node.calibration)
    chip_config["trim"] = fill_dict_from_conf_table(node.trim)
    chip_config["registers"] = fill_dict_from_conf_table(node.registers)
    chip_config["module"] = fill_dict_from_conf_table(node.module)
    chip_config["use_pixel"] = node.use_pixel[:]
    chip_config["masks"] = {}
    for n in node.masks:
        chip_config["masks"][n.name] = n[:]
    return chip_config


def _convert_to_yarr_test_type(scan_id):
    # Convert test type to yarr test type naming according to 'https://gitlab.cern.ch/YARR/localdb-tools/-/blob/v2.0.3/viewer/json-lists/scan_datafile_list.json'
    bdaq_yarr_map = {
        "analog_scan"            : "std_analogscan",
        "digital_scan"           : "std_digitalscan",
        "threshold_scan_hr"         : "std_thresholdscan_hr",
        "threshold_scan_hd"         : "std_thresholdscan_hd",
        # "fast_threshold_scan"    : "std_thresholdscan_hr",
        "analog_scan_tot"    : "std_totscan",
        "global_threshold_tuning": "std_tune_globalthreshold",
        "local_threshold_tuning" : "std_tune_pixelthreshold",
        "noise_occupancy_scan"    : "std_noisescan",
    }

    if scan_id in bdaq_yarr_map.keys():
        return bdaq_yarr_map[scan_id]
    else:
        raise RuntimeError(f"testType not supported: {scan_id}")


def convert_h5_to_json(input_folder, test_type):
    chip_dirs = []
    for root, dirs, files in os.walk(input_folder):
        for name in dirs:
            if name.startswith("chip_"):
                chip_dirs.append(input_folder + '/' + name)

    filename = None
    for chip_dir in chip_dirs:
        # get scan
        filenames = []
        for root, dirs, files in os.walk(chip_dir):
            for name in files:
                if test_type is not None:
                    if test_type + '_interpreted.h5' in name:
                        filenames.append(os.path.join(chip_dir, name))
        for filename in filenames:
            if filename is None:
                raise RuntimeError("Could not find data for testType ({0}) in: {1}".format(test_type, chip_dir))

            bench_config, scan_config = au.ConfigDict(), au.ConfigDict()
            with tb.open_file(filename, mode='r') as h5_file:
                log.info('Converting: {0}'.format(filename))
                configuration_out = h5_file.root.configuration_out
                for node in configuration_out.bench._f_list_nodes():
                    bench_config[node.name] = fill_dict_from_conf_table(node)
                for node in configuration_out.scan._f_list_nodes():
                    if node._v_name in ['enable', 'chips']:  # skip chips and enable node, not needed here
                        continue
                    scan_config[node.name] = fill_dict_from_conf_table(node)
                chip_config = _load_chip_config(node=h5_file.root.configuration_out.chip)

                # FIXE should read scan:id form config file with can_config["run_config"]["scan_id"], does not yet work with _hr/_hd threshold scan
                scan_id_yarr = _convert_to_yarr_test_type(scan_id=test_type)
                # folder structure
                run_number = scan_config["run_config"]["run_name"].split("_")[1]
                output_folder = os.path.join(input_folder, '{0}_{1}'.format(run_number, scan_id_yarr))

                try:
                    os.mkdir(output_folder)
                except FileExistsError:
                    pass

                scan_results = results_from_scans[scan_id_yarr]
                chip_sn = chip_config["settings"]["chip_sn"]
                try:
                    layer_setting = bench_config['QC']['layer_setting']
                except KeyError:
                    layer_setting = 'L2_warm'
                try:
                    config_path = bench_config['QC']['config_path']
                except KeyError:
                    config_path = 'config'
                for scan_result in scan_results:
                    # Enable Mask
                    if scan_result == 'EnMask':
                        log.info('Adding {0}...'.format(scan_result))
                        # create .json to write data
                        json_file = os.path.join(output_folder, '{0}_{1}.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            enable_mask = chip_config['masks']['enable']
                            enable_data = enable_mask.astype(float).tolist()
                            n_entries = 0.0
                            occupancy_data = {"Data": enable_data,
                                              "Entries": n_entries, "Name": "EnMask", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Column", "Bins": n_cols, "High": n_cols + 0.5, "Low": 0.5},
                                              "y": {"AxisTitle": "Row", "Bins": n_rows, "High": n_rows + 0.5, "Low": 0.5},
                                              "z": {"AxisTitle": "Enable"}}

                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'OccupancyMap':
                        log.info('Adding {0}...'.format(scan_result))
                        # create .json to write data
                        json_file = os.path.join(output_folder, '{0}_{1}.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            occ_data = h5_file.root.HistOcc[:].sum(axis=2).astype(float).tolist()
                            n_entries = int(h5_file.root.HistOcc[:].sum())
                            occupancy_data = {"Data": occ_data,
                                              "Entries": n_entries, "Name": "OccupancyMap", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Column", "Bins": n_cols, "High": n_cols + 0.5, "Low": 0.5},
                                              "y": {"AxisTitle": "Row", "Bins": n_rows, "High": n_rows + 0.5, "Low": 0.5},
                                              "z": {"AxisTitle": "Hits"}}

                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'MeanTagMap':
                        log.info('Adding {0}...'.format(scan_result))
                        # create .json to write data
                        json_file = os.path.join(output_folder, '{0}_{1}-0.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            hist_rel_bcid = h5_file.root.HistRelBCID[:]
                            rel_bcid_data = au.get_mean_from_histogram(hist_rel_bcid.sum(axis=(2)), np.arange(hist_rel_bcid.shape[3]), axis=2).astype(float).tolist()
                            n_entries = int(hist_rel_bcid.sum())
                            occupancy_data = {"Data": rel_bcid_data,
                                              "Entries": n_entries, "Name": "MeanTagMap-0", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Column", "Bins": n_cols, "High": n_cols + 0.5, "Low": 0.5},
                                              "y": {"AxisTitle": "Row", "Bins": n_rows, "High": n_rows + 0.5, "Low": 0.5},
                                              "z": {"AxisTitle": "Mean Tag"}}

                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'MeanTotDist':
                        log.info('Adding {0}...'.format(scan_result))
                        n_tot = 16
                        # create .json to write data
                        json_file = os.path.join(output_folder, '{0}_{1}-0.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            hist_tot = h5_file.root.HistTot[:]
                            tot_data = hist_tot.sum(axis=(0, 1, 2)).T.astype(float).tolist()
                            n_entries = int(hist_tot.sum())
                            occupancy_data = {"Data": tot_data,
                                              "Entries": n_entries, "Name": "MeanTotDist-0", "Overflow": 0.0, "Type": "Histo1d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Mean ToT [BC]", "Bins": n_tot, "High": n_tot + 0.5, "Low": 0.5},
                                              "y": {"AxisTitle": "Number of Pixels"},
                                              "z": {"AxisTitle": "z"}}

                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'SigmaTotDist':
                        log.info('Adding {0}...'.format(scan_result))
                        n_bins = 101
                        # create .json to write data
                        json_file = os.path.join(output_folder, '{0}_{1}-0.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            tot_data = np.nan_to_num(h5_file.root.HistTot[:])
                            bin_positions = np.tile(np.arange(tot_data.shape[3]), (tot_data.shape[0], tot_data.shape[1], 1))  # make bin positions the same size as counts
                            tot_rms = au.get_std_from_histogram(tot_data.sum(axis=(2)), bin_positions, axis=2)
                            tot_rms_hist, tot_rms_edge = np.histogram(tot_rms, bins=np.linspace(start=-0.5, stop=1.05, num=n_bins))  # TODO: automatic bin calculation
                            n_entries = int(tot_rms_hist.sum())
                            occupancy_data = {"Data": tot_rms_hist.astype(float).tolist(),
                                              "Entries": n_entries, "Name": "SigmaTotDist-0", "Overflow": 0.0, "Type": "Histo1d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Sigma ToT [BC]", "Bins": len(tot_rms_edge) - 1, "High": tot_rms_edge.max(), "Low": tot_rms_edge.min()},
                                              "y": {"AxisTitle": "Number of Pixels"},
                                              "z": {"AxisTitle": "z"}}

                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'SigmaTotMap':
                        log.info('Adding {0}...'.format(scan_result))
                        # create .json to write data
                        json_file = os.path.join(output_folder, '{0}_{1}-0.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            tot_data = h5_file.root.HistTot[:]
                            bin_positions = np.tile(np.arange(tot_data.shape[3]), (tot_data.shape[0], tot_data.shape[1], 1))  # make bin positions the same size as counts
                            tot_std_hist = au.get_std_from_histogram(tot_data.sum(axis=(2)), bin_positions, axis=2)
                            tot_std = tot_std_hist.astype(float).tolist()
                            n_entries = np.ma.sum(tot_std_hist)
                            occupancy_data = {"Data": tot_std,
                                              "Entries": n_entries, "Name": "SigmaTotMap-0", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Column", "Bins": n_cols, "High": n_cols + 0.5, "Low": 0.5},
                                              "y": {"AxisTitle": "Row", "Bins": n_rows, "High": n_rows + 0.5, "Low": 0.5},
                                              "z": {"AxisTitle": "Sigma ToT [BC]"}}

                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'TagDist':
                        log.info('Adding {0}...'.format(scan_result))
                        n_tag = 31
                        # create .json to write data
                        json_file = os.path.join(output_folder, '{0}_{1}.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            hist_rel_bcid = h5_file.root.HistRelBCID[:]
                            rel_bcid_data = hist_rel_bcid.sum(axis=(0, 1, 2)).T.astype(float).tolist()
                            n_entries = int(hist_rel_bcid.sum())
                            occupancy_data = {"Data": rel_bcid_data,
                                              "Entries": n_entries, "Name": "TagDist", "Overflow": 0.0, "Type": "Histo1d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Tag", "Bins": hist_rel_bcid.shape[-1], "High": n_tag + 0.5, "Low": -0.5},
                                              "y": {"AxisTitle": "Hits"},
                                              "z": {"AxisTitle": "z"}}

                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'MeanTotMap':
                        log.info('Adding {0}...'.format(scan_result))
                        # create .json to write data
                        json_file = os.path.join(output_folder, '{0}_{1}.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            tot_data = h5_file.root.HistTot[:]
                            tot_mean_hist = au.get_mean_from_histogram(tot_data.sum(axis=(2)), np.arange(tot_data.shape[3]), axis=2)
                            tot_mean = tot_mean_hist.astype(float).tolist()
                            n_entries = np.ma.sum(tot_mean_hist)
                            occupancy_data = {"Data": tot_mean,
                                              "Entries": n_entries, "Name": "MeanTotMap-0", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Column", "Bins": n_cols, "High": n_cols + 0.5, "Low": 0.5},
                                              "y": {"AxisTitle": "Row", "Bins": n_rows, "High": n_rows + 0.5, "Low": 0.5},
                                              "z": {"AxisTitle": "Mean ToT [BC]"}}

                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'Chi2Map':
                        log.info('Adding {0}...'.format(scan_result))
                        # Chi2 distribution
                        json_file = os.path.join(output_folder, '{0}_{1}-0.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            data = h5_file.root.Chi2Map[:].astype(float).tolist()
                            occupancy_data = {"Data": data,
                                              "Entries": 0, "Name": "Chi2Map-0", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Column", "Bins": n_cols, "High": n_cols + 0.5, "Low": 0.5},
                                              "y": {"AxisTitle": "Row", "Bins": n_rows, "High": n_rows + 0.5, "Low": 0.5},
                                              "z": {"AxisTitle": "Chi2"}}

                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'Chi2Dist':
                        log.info('Adding {0}...'.format(scan_result))
                        # Chi2 distribution
                        json_file = os.path.join(output_folder, '{0}_{1}-0.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            chi2_map = h5_file.root.Chi2Map[:]
                            chi2_dist, chi2_dist_edges = np.histogram(chi2_map.flatten(), bins=np.linspace(start=-0.025, stop=2.525, num=51))
                            occupancy_data = {"Data": chi2_dist.astype(float).tolist(),
                                              "Entries": 0, "Name": "Chi2Dist-0", "Overflow": 0.0, "Type": "Histo1d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Fit Chi/ndf", "Bins": len(chi2_dist_edges) - 1, "High": chi2_dist_edges.max(), "Low": chi2_dist_edges.min()},
                                              "y": {"AxisTitle": "Number of Pixels"},
                                              "z": {"AxisTitle": "z"}}

                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'InjVcalDiff':
                        log.info('Adding {0}...'.format(scan_result))
                        # Threshold map
                        step_size = scan_config["scan_config"]['VCAL_HIGH_step']
                        start = scan_config["scan_config"]['VCAL_HIGH_start'] - scan_config["scan_config"]['VCAL_MED']
                        stop = scan_config["scan_config"]['VCAL_HIGH_stop'] - scan_config["scan_config"]['VCAL_MED']
                        json_file = os.path.join(output_folder, '{0}_{1}.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            occ_map = h5_file.root.HistOcc[:]
                            scurves = occ_map.reshape((n_rows * n_cols, -1)).T
                            param_count = scurves.shape[0]
                            max_occ = 150  # hard-coded since needs to be the same for all chips

                            hist = np.empty([param_count, max_occ], dtype=int)
                            for param in range(param_count):
                                hist[param] = np.bincount(scurves[param, :], minlength=max_occ)

                            data = hist.astype(float).tolist()
                            occupancy_data = {"Data": data,
                                              "Entries": 0, "Name": "InjVcalDiff", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "InjVcalDiff", "Bins": param_count, "High": stop + step_size, "Low": start - step_size},
                                              "y": {"AxisTitle": "Occupancy", "Bins": max_occ, "High": max_occ - 0.5, "Low": 0.5},
                                              "z": {"AxisTitle": "Number of pixels"}}
                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'InjVcalDiff_Map':
                        log.info('Adding {0}...'.format(scan_result))
                        # Threshold map
                        step_size = scan_config["scan_config"]['VCAL_HIGH_step']
                        start = scan_config["scan_config"]['VCAL_HIGH_start'] - scan_config["scan_config"]['VCAL_MED']
                        stop = scan_config["scan_config"]['VCAL_HIGH_stop'] - scan_config["scan_config"]['VCAL_MED']
                        json_file = os.path.join(output_folder, '{0}_{1}.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            occ_map = h5_file.root.HistOcc[:]
                            scurves = occ_map.reshape((n_rows * n_cols, -1))
                            data = scurves.astype(float).tolist()
                            occupancy_data = {"Data": data,
                                              "Entries": 0, "Name": "InjVcalDiff_Map", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Channel Number", "Bins": n_rows * n_cols, "High": n_rows * n_cols - 0.5, "Low": -0.5},
                                              "y": {"AxisTitle": "InjVcalDiff", "Bins": param_count, "High": stop + step_size, "Low": start - step_size},
                                              "z": {"AxisTitle": "Number of Hits"}}
                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'ThresholdMap':
                        log.info('Adding {0}...'.format(scan_result))
                        # Threshold map
                        json_file = os.path.join(output_folder, '{0}_{1}-0_0_500_0_0.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            threshold_map = h5_file.root.ThresholdMap[:]
                            threshold_map, _ = _convert_to_e(threshold_map, chip_config=chip_config, use_offset=True)
                            data = threshold_map.astype(float).tolist()
                            occupancy_data = {"Data": data,
                                              "Entries": 0, "Name": "ThresholdMap-0", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0,
                                              'loopStatus': [0, 500, 0, 0, 0],  # FIXME: do not hard code that
                                              "x": {"AxisTitle": "Column", "Bins": n_cols, "High": n_cols + 0.5, "Low": 0.5},
                                              "y": {"AxisTitle": "Row", "Bins": n_rows, "High": n_rows + 0.5, "Low": 0.5},
                                              "z": {"AxisTitle": "Threshold [e]"}}
                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'ThresholdDist':
                        # FIXME: why so creazy mean?
                        log.info('Adding {0}...'.format(scan_result))
                        # Threshold dist
                        json_file = os.path.join(output_folder, '{0}_{1}-0.json'.format(chip_sn, scan_result))
                        step_size = int(_convert_to_e(scan_config["scan_config"]['VCAL_HIGH_step'], chip_config=chip_config, use_offset=False)[0])
                        start = int(_convert_to_e(scan_config["scan_config"]['VCAL_HIGH_start'] - scan_config["scan_config"]['VCAL_MED'], chip_config=chip_config, use_offset=True)[0])
                        stop = int(_convert_to_e(scan_config["scan_config"]['VCAL_HIGH_stop'] - scan_config["scan_config"]['VCAL_MED'], chip_config=chip_config, use_offset=True)[0])
                        with open(json_file, "w") as f:
                            threshold_map = h5_file.root.ThresholdMap[:]
                            threshold_map, _ = _convert_to_e(threshold_map, chip_config=chip_config, use_offset=True)
                            threshold_dist, threshold_dist_edges = np.histogram(threshold_map.flatten(), bins=np.arange(start=start, stop=stop, step=step_size))
                            data = threshold_dist.astype(float).tolist()
                            occupancy_data = {"Data": data,
                                              "Entries": 0, "Name": "ThresholdDist-0", "Overflow": 0.0, "Type": "Histo1d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Threshold [e]", "Bins": len(threshold_dist_edges) - 1, "High": int(threshold_dist_edges.max()), "Low": int(threshold_dist_edges.min())},
                                              "y": {"AxisTitle": "Number of Pixels"},
                                              "z": {"AxisTitle": "z"}}
                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'NoiseMap':
                        log.info('Adding {0}...'.format(scan_result))
                        # Noise map
                        json_file = os.path.join(output_folder, '{0}_{1}-0.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            threshold_map = h5_file.root.NoiseMap[:]
                            threshold_map, _ = _convert_to_e(threshold_map, chip_config=chip_config, use_offset=False)
                            data = threshold_map.astype(float).tolist()
                            occupancy_data = {"Data": data,
                                              "Entries": 0, "Name": "NoiseMap-0", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Column", "Bins": n_cols, "High": n_cols + 0.5, "Low": 0.5},
                                              "y": {"AxisTitle": "Row", "Bins": n_rows, "High": n_rows + 0.5, "Low": 0.5},
                                              "z": {"AxisTitle": "Noise [e]"}}
                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'NoiseDist':
                        log.info('Adding {0}...'.format(scan_result))
                        # Noise dist
                        json_file = os.path.join(output_folder, '{0}_{1}-0.json'.format(chip_sn, scan_result))
                        step_size = int(_convert_to_e(scan_config["scan_config"]['VCAL_HIGH_step'], chip_config=chip_config, use_offset=False)[0])
                        with open(json_file, "w") as f:
                            threshold_map = h5_file.root.NoiseMap[:]
                            threshold_map, _ = _convert_to_e(threshold_map, chip_config=chip_config, use_offset=False)
                            threshold_dist, threshold_dist_edges = np.histogram(threshold_map.flatten(), bins=_calculate_bins(threshold_map))
                            data = threshold_dist.astype(float).tolist()
                            occupancy_data = {"Data": data,
                                              "Entries": 0, "Name": "ThresholdDist-0", "Overflow": 0.0, "Type": "Histo1d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Noise [e]", "Bins": len(threshold_dist_edges) - 1, "High": int(threshold_dist_edges.max()), "Low": int(threshold_dist_edges.min())},
                                              "y": {"AxisTitle": "Number of Pixels"},
                                              "z": {"AxisTitle": "z"}}
                            json.dump(occupancy_data, f, indent=4)

                    elif scan_result == 'Config_before':
                        log.info('Adding {0}...'.format(scan_result))
                        json_file = os.path.join(output_folder, '{0}_{1}.json.before'.format(chip_sn, layer_setting))
                        with open(json_file, "w") as f:
                            # Convert BDAQ chip config to YARR
                            bdaq_conf_in = _load_chip_config(h5_file.root.configuration_in.chip)
                            config_data = convert_config_bdaq_to_yarr(bdaq_config=bdaq_conf_in, chip_type=chip_config["settings"]['chip_type'])

                            pixel_conf = []
                            # Write masks
                            enable_mask = h5_file.root.configuration_in.chip.masks.enable[:]
                            hitbus_mask = h5_file.root.configuration_in.chip.masks.hitbus[:]
                            injection_mask = h5_file.root.configuration_in.chip.masks.injection[:]
                            tdac_mask = h5_file.root.configuration_in.chip.masks.tdac[:]
                            for col in range(n_cols):
                                pixel_conf.append({"Col": col,
                                                   "Enable": enable_mask[col, :].astype(int).tolist(),
                                                   "Hitbus": hitbus_mask[col, :].astype(int).tolist(),
                                                   "InjEn": injection_mask[col, :].astype(int).tolist(),
                                                   "TDAC": tdac_mask[col, :].astype(int).tolist()
                                                   })
                            # Add masks to YARR config
                            config_data['RD53B']['PixelConfig'] = pixel_conf

                            json.dump(config_data, f, indent=4)

                    elif scan_result == 'Config_after':
                        log.info('Adding {0}...'.format(scan_result))
                        json_file = os.path.join(output_folder, '{0}_{1}.json.after'.format(chip_sn, layer_setting))
                        with open(json_file, "w") as f:
                            # Convert BDAQ chip config to YARR
                            bdaq_conf_in = _load_chip_config(h5_file.root.configuration_out.chip)
                            config_data = convert_config_bdaq_to_yarr(bdaq_config=bdaq_conf_in, chip_type=chip_config["settings"]['chip_type'])

                            pixel_conf = []
                            # Write masks
                            enable_mask = h5_file.root.configuration_out.chip.masks.enable[:]
                            hitbus_mask = h5_file.root.configuration_out.chip.masks.hitbus[:]
                            injection_mask = h5_file.root.configuration_out.chip.masks.injection[:]
                            tdac_mask = h5_file.root.configuration_out.chip.masks.tdac[:]
                            for col in range(n_cols):
                                pixel_conf.append({"Col": col,
                                                   "Enable": enable_mask[col, :].astype(int).tolist(),
                                                   "Hitbus": hitbus_mask[col, :].astype(int).tolist(),
                                                   "InjEn": injection_mask[col, :].astype(int).tolist(),
                                                   "TDAC": tdac_mask[col, :].astype(int).tolist()
                                                   })
                            # Add masks to YARR config
                            config_data['RD53B']['PixelConfig'] = pixel_conf

                            json.dump(config_data, f, indent=4)

                    elif scan_result == 'NoiseOccupancy':
                        log.info('Adding {0}...'.format(scan_result))
                        try:
                            n_triggers = scan_config["scan_config"]["n_triggers"]
                        except KeyError:
                            n_triggers = 1e7
                            print("No n_triggers found, assume %f" % n_triggers)
                        json_file = os.path.join(output_folder, '{0}_{1}.json'.format(chip_sn, scan_result))
                        with open(json_file, "w") as f:
                            occ_data = (h5_file.root.HistOcc[:] / n_triggers).sum(axis=2).astype(float).tolist()
                            n_entries = 0
                            occupancy_data = {"Data": occ_data,
                                              "Entries": n_entries, "Name": "NoiseOccupancy", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0,
                                              "x": {"AxisTitle": "Column", "Bins": n_cols, "High": n_cols + 0.5, "Low": 0.5},
                                              "y": {"AxisTitle": "Row", "Bins": n_rows, "High": n_rows + 0.5, "Low": 0.5},
                                              "z": {"AxisTitle": "Noise Occupancy hits/bc"}}

                            json.dump(occupancy_data, f, indent=4)

                    else:
                        raise RuntimeError("Scan result not implemented ({0})".format(scan_result))

                    # always write scanLog.json
                    scan_log_json = output_folder + '/' + 'scanLog' + '.json'
                    log.info('Adding scanLog ...')
                    with open(scan_log_json, "w") as f:
                        scan_log_data = {}

                        # get chips
                        chips = []
                        # first chip
                        chips.append({
                            "__config_path__": config_path + "/" + layer_setting + "/" + chip_sn + '_' + layer_setting + ".json",
                            "config": layer_setting + "/" + chip_sn + '_' + layer_setting + ".json",
                            "enable": 1,
                            "locked": 0,
                            "path": "relToCon",
                            "rx": chip_config["settings"]["receiver"],
                            "serialNumber": _convert_chip_sn(chip_sn=chip_config["settings"]["chip_sn"])
                        })
                        # loop over other chips
                        for v in chip_config["module"]:
                            if "chip_" in v:
                                ch = chip_config["module"][v]  # eval(chip_config["module"][v])  # dangerous, but want to convert strin to object...
                                chips.append({
                                    "__config_path__": config_path + "/" + layer_setting + "/" + ch["chip_sn"] + '_' + layer_setting + ".json",
                                    "config": layer_setting + "/" + ch["chip_sn"] + '_' + layer_setting + ".json",
                                    "enable": 1,
                                    "locked": 0,
                                    "path": "relToCon",
                                    "rx": ch["receiver"],
                                    "serialNumber": _convert_chip_sn(chip_sn=ch["chip_sn"])
                                })

                        connectivity = [{"chipType": 'RD53B' if 'itkpix' in chip_config["settings"]['chip_type'] else "RD53A", "chips": chips}]

                        # ctrlCfg
                        ctrlCfg = {"ctrlCfg":
                                   {"cfg": {"cmdPeriod": 6.250000073038109e-9, "idle": {"word": 2863311530}, "pulse": {"interval": 500, "word": 0,
                                                                                                                       "rxActiveLanes": 1,
                                                                                                                       "rxPolarity": 65535,
                                                                                                                       "specNum": 0,
                                                                                                                       "spiConfig": 541200,
                                                                                                                       "sync": {"interval": 16, "word": 2172551550},
                                                                                                                       "txPolarity": 0
                                                                                                                       },
                                            "type": "spec"}
                                    }}

                        # ctrlStatus
                        # TODO: add more info about hardware to bench config
                        ctrlStatus = {"channel_configuration": "12x1 TLU",
                                      "fe_chip_type": "RD53A/B",
                                      "firmware_hash": "",
                                      "firmware_identifier": "",
                                      "firmware_vers": "",
                                      "fmc_card_type": None,
                                      "fpga_card": "",
                                      "lpm_status": bench_config.get('LP_enable', False),
                                      "rx_speed": ""
                                      }

                        # dbCfg
                        dbCfg = {
                            "KeyFile": "null",
                            "auth": "default",
                            "component": ["Front-end Chip", "Front-end Chips Wafer", "Hybrid", "Module", "Sensor Tile", "Sensor Wafer"],
                            "dbName": "localdb",
                            "environment": ["lv_voltage", "lv_current", "hv_voltage", "hv_current", "temp_module"],  # TODO: adjust this
                            "hostIp": "itkpix-localdb.physik.uni-bonn.de",
                            "hostPort": "27017",
                            "ssl": {
                                "CAFile": "null",
                                "PEMKeyFile": "null",
                                "enabled": False
                            },
                            "stage": ["Bare Module", "Wire Bonded", "Potted", "Final Electrical", "Complete", "Loaded", "Parylene", "Initial Electrical", "Thermal Cycling", "Flex + Bare Module Attachment", "Testing"],
                            "tls": {
                                "CAFile": "null",
                                "CertificateKeyFile": "null",
                                "enabled": False
                            },
                            "verify": False
                        }

                        finish_time = h5_file.root.environmental_data[:]['timestamp'][-1]
                        start_time = h5_file.root.environmental_data[:]['timestamp'][0]
                        # Update dict and write to file
                        scan_log_data.update({
                            "connectivity": connectivity,
                            "ctrlCfg": ctrlCfg,
                            "ctrlStatus": ctrlStatus,
                            "dbCfg": dbCfg,
                            "exec": "",
                            "finishTime": int(finish_time),
                            "startTime": int(start_time),
                            "stopwatch": {"analysis": 0, "config": 0, "processing": 0, "scan": int(finish_time - start_time)},
                            "runNumber": run_number,
                            "siteCfg": {"institution": "University of Bonn"},
                            "targetCharge": -1,
                            "targetTot": -1,
                            "testType": scan_id_yarr,
                            "timestamp": scan_config["run_config"]["run_name"].split("_")[0],
                            "userCfg": {"description": "default", "institution": "University of Bonn", "userName": "admin"},
                            "yarr_version": {
                                "git_branch": str(scan_config["run_config"]["software_version"].split("@")[0]),
                                "git_date": "",
                                "git_hash": str(scan_config["run_config"]["software_version"].split("@")[-1].split("Changed")[0])[:-1],
                                "git_subject": "",
                                "git_tag": ""
                            }
                        })
                        json.dump(scan_log_data, f, indent=4)

                    # always write testType.json
                    # FIXME: too Yarr specific. Do not fill anything and hope for the best
                    log.info('Adding {0} ...'.format(scan_id_yarr))
                    test_type_json = output_folder + '/' + scan_id_yarr + '.json'
                    with open(test_type_json, "w") as f:
                        test_type_data = {
                            "scan": {
                                "analysis": {},
                                "histogrammer": {},
                                "loops": {},
                                "name": {},
                                "prescan": {}
                            }
                        }
                        json.dump(test_type_data, f, indent=4)


if __name__ == '__main__':
    input_folder = '/media/yannick/sirrush/silab/Yannick/Module_QC/Q2/Q2_QC/Quad_2/'
    test_types = ['noise_occupancy_scan']
    for test_type in test_types:
        convert_h5_to_json(input_folder=input_folder, test_type=test_type)
