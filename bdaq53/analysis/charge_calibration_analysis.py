'''
This analysis script is used for the calibration of the charge injection circuitry.
For this, HitOR calibration for the desired FE should be performed first and the interpreted file
should be used in the scan_source.py in order to convert TDC to DVCAL during the analysis.
This is something that has to be done "on the fly"
otherwise analysis for cluster size > 1 events is not correct.
Interpreted files from the scan_source.py are given as configuration to this analysis script,
which uses new plotting functions from plotting.py to plot and fit the source spectra
as well as to perform the final charge calibration. The fits are performed using iminuit module.
'''

import time
import os
import numpy as np
import uncertainties as u
from uncertainties import unumpy

from bdaq53.analysis import plotting

configuration = {

    # Put your working directory here, otherwise your output_data directory will be used by default
    'working_dir': None,

    # Cluster size events that will be considered for the analysis
    'cluster_size': 1,

    # Column and row selection
    'start_column': 264,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,
    # This is the bin size used for plotting the spectra.
    # From a rough estimation using HitOR calibration -> bin_size=5 for LIN & bin_size=10 for DIFF
    'bin_size': 5,

    # If set to False ax.plot is used to plot the spectra
    # (no bins, just a line going through the middle of every bin). This plotting way is faster.
    # If set to True, ax.bar is used to plot the spectra,
    # which is significantly slower comparing to the ax.plot
    'binned_spectrum': False,

    'hit_file_select': {
        # Interpreted files after source scan used for the charge calibration
        # Put the source scan interpreted file of each source in between the apostrophes
        # For the ones that you don't have any source scans leave them as they are
        'Fe': '',
        'Cu': '',
        'Rb': '',
        'Mo': '',
        'Ag': '',
        'Cd': '',
        'Ba': '',
        'Tb': '',
        'Am': ''

    },

    'spectrum_keys': {
        # Sources used for the charge calibration. Set to True the ones you use!!
        'Fe': False,
        'Cu': False,
        'Rb': False,
        'Mo': False,
        'Ag': False,
        'Cd': False,
        'Ba': False,
        'Tb': False,
        'Am': False
    },

    'fit_range': {
        # Ranges to fit gaussian(s)
        # Note that the left part of the spectrum is affected by charge sharing
        # and thus it's a convolution of two functions
        # Recommendation: Try to fit the spectra starting close to the Ka peak from the left side
        # until the end of Kb
        'Fe': (120, 180),
        'Cu': (185, 255),
        'Rb': (330, 460),
        'Mo': (425, 590),
        'Cd': (530, 710),
        'Ag': (550, 725),
        'Ba': (810, 1040),
        'Tb': (1110, 1440),
        'Am': ()
    },

    'Energies_in_keV': {
        # Energies of the radioactive sources from literature that will be fitted.
        # Keep only the peaks that you want to fit
        # Note that if two energies are assigned to a source, double gaussian fit will be performed
        # If you want to fit only Ka, please assign only one energy per source
        'Fe': (5.905, 6.490),
        'Cu': (8.050, 8.910),
        'Rb': (13.370, 15.078),
        'Mo': (17.425, 19.793),
        'Ag': (22.075, 25.205),
        'Cd': (22.075, 25.205),
        'Ba': (32.005, 36.813),
        'Tb': (44.110, 51.042),
        'Am': (59.54)
    }

}


def analyze():

    # Create timestamp to add in the filename
    timestamp = time.strftime("%Y%m%d_%H%M%S")
    run_name = timestamp + '_' + 'charge_calibration_analysis.pdf'

    # Check if user provided a working directory
    if configuration['working_dir'] is None:
        working_dir = os.path.join(os.getcwd(), "output_data")
    else:
        working_dir = configuration['working_dir']

    # Create target Directory if doesn't exist
    if not os.path.exists(working_dir):
        os.mkdir(working_dir)

    output_filename = os.path.join(working_dir, run_name)
    spectrum_keys = configuration['spectrum_keys']
    bin_size = configuration['bin_size']
    cluster_size_selected = configuration['cluster_size']
    binned_spectrum = configuration['binned_spectrum']
    peaks = np.array([])
    sigmas = np.array([])
    peaks_stat_uncertainties = np.array([])
    sigmas_stat_uncertainties = np.array([])
    Energies_in_keV = np.array([])
    energies_from_literature_labels = []
    point_label = []
    w = u.ufloat(3.65, 0.03)  # w with its experimental uncertainty
    start_column = configuration['start_column']
    stop_column = configuration['stop_column']
    start_row = configuration['start_row']
    stop_row = configuration['stop_row']

    with plotting.Plotting(analyzed_data_file='', pdf_file=output_filename) as p:
        for key, spectrum_value in spectrum_keys.items():
            if spectrum_value:
                spectrum_key = key
                energies_from_literature_labels.append(spectrum_key)
                energies = np.array(configuration['Energies_in_keV'][spectrum_key])
                hit_file = configuration['hit_file_select'][spectrum_key]
                fit_range = configuration['fit_range'][spectrum_key]
        # Step 1: Plot energy spectrum in Delta_VCAL and fit wit gaussian(s)
                peak, peak_stat_uncertainty, sigma, sigma_stat_uncertainty, fig = p._plot_and_fit_spectrum_in_dvcal(energies=energies,
                                                                                                                    hit_file=hit_file, fit_range=fit_range,
                                                                                                                    cluster_size_selected=cluster_size_selected,
                                                                                                                    spectrum_key=spectrum_key, bin_size=bin_size,
                                                                                                                    start_column=start_column, stop_column=stop_column,
                                                                                                                    start_row=start_row, stop_row=stop_row,
                                                                                                                    binned_spectrum=binned_spectrum)
                p._save_plots(fig)
                # Statistical uncertainties
                peaks = np.append(peaks, peak)
                sigmas = np.append(sigmas, sigma)
                peaks_stat_uncertainties = np.append(peaks_stat_uncertainties,
                                                     peak_stat_uncertainty)
                sigmas_stat_uncertainties = np.append(sigmas_stat_uncertainties,
                                                      sigma_stat_uncertainty)
                Energies_in_keV = np.append(Energies_in_keV, energies)
                # Extend the point labels for the charge and energy calibration plots
                # based on the sources used and peaks fitted
                try:
                    if np.size(energies) == 1:
                        point_label.extend([r'${K}_{\alpha}^{' + spectrum_key + '}$'])
                    else:
                        point_label.extend([r'${K}_{\alpha}^{' + spectrum_key + '}$',
                                            r'${K}_{\beta}^{' + spectrum_key + '}$'])
                except Exception as e:
                    print(e)

        n_electrons = Energies_in_keV * 1000 / w
        # Step 2: Energy calibration
        fig = p._plot_calibration_line(x=Energies_in_keV, y=peaks, y_err=peaks_stat_uncertainties,
                                       point_label=point_label, suffix='Energy Calibration',
                                       xlabel='Photon Energy [keV]', ylabel='$\Delta$VCAL [DAC]',
                                       plot_offset_from_fit=True)
        p._save_plots(fig)
        # Step 3: Charge calibration
        fig = p._plot_calibration_line(x=peaks, y=unumpy.nominal_values(n_electrons),
                                       y_err=unumpy.std_devs(n_electrons), point_label=point_label,
                                       suffix='Charge Calibration', xlabel='$\Delta$VCAL [DAC]',
                                       ylabel='Charge [$e^-$]', plot_offset_from_fit=True)
        p._save_plots(fig)


if __name__ == "__main__":
    analyze()
