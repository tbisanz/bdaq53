import json
import os
import logging
import tables as tb
import bdaq53.analysis.analysis_utils as au
from argparse import ArgumentParser


class JSONConverter:
    dir_name_dict = {
        'adc_calibration': 'ADC_CALIBRATION',
        'dac_linearity_scan': 'ADC_CALIBRATION',
        'sldo_iv_scan': 'SLDO',
        'LOW_POWER_Scan': 'LP_MODE',
        'analog_readback': 'ANALOG_READBACK',
        'NTC_readout': 'ANALOG_READBACK',
        'trim_scan': 'ANALOG_READBACK',
        'meas_inj_cap': 'INJECTION_CAPACITANCE',
        'vcal_calibration': 'VCAL_CALIBRATION',
        'OVP_Scan': 'OVERVOLTAGE_PROTECTION'
    }

    test_types_dict = {
        'adc_calibration': ['ADC_CALIBRATION', ''],
        'dac_linearity_scan': ['ADC_CALIBRATION', ''],
        'sldo_iv_scan': ['SLDO', ''],
        'LOW_POWER_Scan': ['LP_MODE', ''],
        'analog_readback': ['ANALOG_READBACK', 'AR_VMEAS'],
        'NTC_readout': ['ANALOG_READBACK', 'AR_TEMP'],
        'trim_scan': ['ANALOG_READBACK', 'AR_VDD'],
        'meas_inj_cap': ['INJECTION_CAPACITANCE', ''],
        'vcal_calibration': ['VCAL_CALIBRATION', ''],
        'OVP_Scan': ['OVERVOLTAGE_PROTECTION', '']
    }

    vcal_data_names = {
        'VCAL_MED': 'VCAL_MED_largeRange_data',
        'VCAL_MED_SMALL_RANGE': 'VCAL_MED_smallRange_data',
        'VCAL_HIGH': 'VCAL_HIGH_largeRange_data',
        'VCAL_HIGH_SMALL_RANGE': 'VCAL_HIGH_smallRange_data'
    }

    voltage_mux = {'VREF_ADC': 'Vmux0',
                   'I_MUX': 'Imux63',
                   'NTC_PAD': 'Vmux2',
                   'VREF_VDAC': 'Vmux3',
                   'VDDA_HALF_CAP': 'Vmux4',
                   'TEMPSENS_T': 'Vmux5',
                   'TEMPSENS_B': 'Vmux6',
                   'VCAL_HI': 'Vmux7',
                   'VCAL_HIGH': 'Vmux7',
                   'VCAL_MED': 'Vmux8',
                   'DAC_TH2': 'Vmux9',
                   'DAC_TH1_M': 'Vmux10',
                   'DAC_TH1_L': 'Vmux11',
                   'DAC_TH1_R': 'Vmux12',
                   'RADSENS_A': 'Vmux13',
                   'TEMPSENS_A': 'Vmux14',
                   'RADSENS_D': 'Vmux15',
                   'TEMPSENS_D': 'Vmux16',
                   'RADSENS_C': 'Vmux17',
                   'TEMPSENS_C': 'Vmux18',
                   'GNDA19': 'Vmux19',
                   'GNDA20': 'Vmux20',
                   'GNDA21': 'Vmux21',
                   'GNDA22': 'Vmux22',
                   'GNDA23': 'Vmux23',
                   'GNDA24': 'Vmux24',
                   'GNDA25': 'Vmux25',
                   'GNDA26': 'Vmux26',
                   'GNDA27': 'Vmux27',
                   'GNDA28': 'Vmux28',
                   'GNDA29': 'Vmux29',
                   'GNDA30': 'Vmux30',
                   'VREF_CORE': 'Vmux31',
                   'VREF_PRE': 'Vmux32',
                   'VINA_HALF': 'Vmux33',
                   'VDDA_HALF': 'Vmux34',
                   'VREFA': 'Vmux35',
                   'VOFS_HALF': 'Vmux36',
                   'VIND_HALF': 'Vmux37',
                   'VDDD_HALF': 'Vmux38',
                   'VREFD': 'Vmux39'}

    current_mux = {'IREF': 'Imux0',
                   'CDR_VCO_MAIN': 'Imux1',
                   'CDR_VCO_BUF': 'Imux2',
                   'CDR_CP': 'Imux3',
                   'CDR_FD': 'Imux4',
                   'CDR_BUF': 'Imux5',
                   'CML_TAP2': 'Imux6',
                   'CML_TAP1': 'Imux7',
                   'CML_MAIN': 'Imux8',
                   'NTC_PAD': 'Imux9',
                   'CAP': 'Imux10',
                   'CAP_PARASIT': 'Imux11',
                   'PREAMP_MATRIX_MAIN': 'Imux12',
                   'DAC_PRECOMP': 'Imux13',
                   'DAC_COMP': 'Imux14',
                   'DAC_TH2': 'Imux15',
                   'DAC_TH1_M': 'Imux16',
                   'DAC_LCC': 'Imux17',
                   'DAC_FB': 'Imux18',
                   'DAC_PREAMP_L': 'Imux19',
                   'DAC_TH1_L': 'Imux20',
                   'DAC_PREAMP_R': 'Imux21',
                   'DAC_PREAMP_TL': 'Imux22',
                   'DAC_TH1_R': 'Imux23',
                   'DAC_PREAMP_T': 'Imux24',
                   'DAC_PREAMP_TR': 'Imux25',
                   'IINA': 'Imux28',
                   'I_SHUNT_A': 'Imux29',
                   'IIND': 'Imux30',
                   'I_SHUNT_D': 'Imux31'}

    def __init__(self, data_file, institution='University of Bonn', outpath=None):
        self.data_file = data_file
        self.institution = institution
        if outpath is not None:
            self.output_path = outpath
        else:
            current_path = os.getcwd()
            self.output_path = current_path + '/output'

    def convert_h5_to_json(self):
        """
            Takes the BDAQ53 h5 file output and converts it to the json format used by ATLAS QC

            Parameters
            ----------
            data_file : string
                A string raw data file name. File ending (.h5).
            institution: string
                specifies where the measurement was taken.
            outpath: basestring
                optional: specifies where to save the created json file
        """

        try:
            if isinstance(self.data_file, str):
                in_file = tb.open_file(self.data_file, 'r')
                self.root = in_file.root
            else:
                self.root = self.data_file
        except IOError:
            logging.error("Input file %s does not exist" % self.data_file)
            return

        self.run_config = au.ConfigDict(self.root.configuration_in.scan.run_config[:])
        self.scan_config = au.ConfigDict(self.root.configuration_in.scan.scan_config[:])
        self.registers = au.ConfigDict(self.root.configuration_in.chip.registers[:])
        self.chip_cal = au.ConfigDict(self.root.configuration_in.chip.calibration[:])
        self.chip_trim = au.ConfigDict(self.root.configuration_in.chip.trim[:])
        self.module_sn = au.ConfigDict(self.root.configuration_in.chip.module[:])['identifier']

        self.scan_id = self.run_config['scan_id']
        self.chip_sn = self.run_config['chip_sn']

        if self.scan_id != 'vcal_calibration':
            self.output = {'serialNumber': self.atlas_sn, 'results': {"property": {"unknown": 'version'}, "comment": ""}}
            self._get_test_type()
            self._get_measurements()
            self._get_metadata()

            output = [self.output]
        else:
            output = []
            for subtest in ['VCAL_HIGH', 'VCAL_MED', 'VCAL_HIGH_SMALL_RANGE', 'VCAL_MED_SMALL_RANGE']:
                self.output = {'serialNumber': self.atlas_sn,
                               'testType': 'VCAL_CALIBRATION',
                               'subtestType': subtest,
                               'results': {"property": {"unknown": 'version'}, "comment": ""}}
                self._get_measurements()
                self._get_metadata()
                self.output['results']['Metadata']['ChipConfigs']["RD53B"]["GlobalConfig"]["InjVcalRange"] = 0 if 'SMALL' in subtest else 1
                self.output['results']['Metadata']['ChipConfigs']["RD53B"]["GlobalConfig"][
                    "MonitorV"] = 7 if 'HIGH' in subtest else 8
                output.append(self.output.copy())

        filename = self.create_filepath()

        logging.info("Saving JSON file: %s" % filename)
        try:
            with open(filename, "w") as outfile:
                json.dump([output], outfile, indent=4,)
        except Exception:
            logging.error("JSON file %s could not be saved" % filename)
            raise ValueError
        logging.info("Finished JSON Dump")

        try:
            in_file.close()
        except Exception:
            pass

    @property
    def atlas_sn(self):
        '''
        Converts chip S/N (0x....) to ATLAS S/N (20PGFC).
        '''
        return '20UPGFC{0:07d}'.format(int(self.chip_sn, 16))

    def _get_test_type(self):
        test_type = self.test_types_dict.get(self.scan_id, [self.scan_id, ''])

        self.output['testType'] = test_type[0]
        self.output['subtestType'] = test_type[1]
        # Work in process!

    def _get_measurements(self):
        if self.scan_id in ['dac_linearity_scan', 'adc_tuning']:
            self._get_measurements_dac()
        elif self.scan_id == 'sldo_iv_scan':
            self._get_measurements_sldo()
        elif self.scan_id == 'LOW_POWER_Scan':
            self._get_measurements_low_power()
        elif self.scan_id == 'trim_scan':
            self._get_measurements_trim()
        elif self.scan_id == 'analog_readback':
            self._get_measurements_analog_read()
        elif self.scan_id == 'NTC_readout':
            self._get_measurements_NTC_read()
        elif self.scan_id == 'meas_inj_cap':
            self._get_measurements_inj_cap()
        elif self.scan_id == 'vcal_calibration':
            self._get_measurements_vcal()
        elif self.scan_id == 'OVP_Scan':
            self._get_measurements_OVP()
        else:
            self.output['results']['Measurements'] = {'Data': 'not Found'}

    def create_filepath(self):
        _filename = self.chip_sn
        _filepath = os.path.join(self.output_path, self.dir_name_dict[self.scan_id], self.module_sn)

        if not os.path.exists(os.path.normpath(_filepath)):
            os.makedirs(os.path.normpath(_filepath))
            logging.info("Creating output directory: %s" % _filepath)

        filename = ''

        count = 0
        while os.path.exists(filename) or count == 0:
            filename = os.path.join(_filepath, _filename)
            if self.output['testType'] == 'ANALOG_READBACK':
                filename += '_' + self.output['subtestType']
            if count != 0:
                filename += '_' + str(count)
            filename += '.json'
            count += 1

        return filename

    def _get_measurements_dac(self):
        measurement_out = {'DACs_input': {'X': True, 'Unit': 'Count', 'Values': []}}

        mux_name = self.voltage_mux[self.scan_config['DAC']]
        measurement_out[mux_name] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out['Vmux30'] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out['ADC_' + mux_name] = {'X': False, 'Unit': 'Count', 'Values': []}
        measurement_out['ADC_Vmux30'] = {'X': False, 'Unit': 'Count', 'Values': []}

        DAC_data = self.root.dac_data[:]
        for dac_row in DAC_data:
            measurement_out['DACs_input']['Values'].append(float(dac_row[1]))
            measurement_out['ADC_' + mux_name]['Values'].append(float(dac_row[2]))
            measurement_out['ADC_Vmux30']['Values'].append(float(0))
            measurement_out[mux_name]['Values'].append(float(dac_row[3]))
            measurement_out['Vmux30']['Values'].append(float(dac_row[4]))

        self.output['results']['Measurements'] = measurement_out

    def _get_measurements_vcal(self):
        measurement_out = {'DACs_input': {'X': True, 'Unit': 'Count', 'Values': []}}

        mux_name = 'Vmux8' if 'VCAL_MED' in self.output['subtestType'] else 'Vmux7'
        measurement_out[mux_name] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out['Vmux30'] = {'X': False, 'Unit': 'V', 'Values': []}

        DAC_data = eval('self.root.' + self.vcal_data_names[self.output['subtestType']] + '[:]')

        for dac_row in DAC_data:
            measurement_out['DACs_input']['Values'].append(float(dac_row[1]))
            measurement_out[mux_name]['Values'].append(float(dac_row[3]))
            measurement_out['Vmux30']['Values'].append(float(dac_row[4]))

        self.output['results']['Measurements'] = measurement_out

    def _get_measurements_trim(self):
        data_vdda = self.root.trim_vdda[:]
        data_vddd = self.root.trim_vddd[:]

        measurement_out = {}

        measurement_out['Vmux34'] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out['Vmux38'] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out['Vmux30'] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out['SldoTrimA'] = {'X': False, 'Unit': '-', 'Values': []}
        measurement_out['SldoTrimD'] = {'X': False, 'Unit': '-', 'Values': []}

        for row in data_vdda:
            measurement_out['SldoTrimA']['Values'].append(int(row[1]))
            measurement_out['Vmux34']['Values'].append(float(row[2]))
            measurement_out['Vmux30']['Values'].append(float(row[3]))
        for row in data_vddd:
            measurement_out['SldoTrimD']['Values'].append(int(row[1]))
            measurement_out['Vmux38']['Values'].append(float(row[2]))

        self.output['results']['Measurements'] = measurement_out

    def _get_measurements_analog_read(self):
        data = self.root.analog_readback[:]

        self.output['results']['Measurements'] = {}
        for row in data:
            mux_name = row[0].decode()
            measure = mux_name[0]

            if measure == 'V':
                mux = self.voltage_mux.get(mux_name[2:])
                self.output['results']['Measurements']['Vmux'] = {'X': False, 'Unit': 'V', 'Values': [float(row[1])]}
            else:
                mux = self.current_mux.get(mux_name[2:])
            if mux is not None:
                self.output['results']['Measurements'][mux] = {'X': False, 'Unit': 'V', 'Values': [float(row[1])]}
            else:
                self.output['results']['Measurements'][row[0].decode()] = {'X': False, 'Unit': 'V', 'Values': [float(row[1])]}

    def _get_measurements_NTC_read(self):
        sensor_registers = {
            'TEMPSENS_A': 'SldoAna',
            'TEMPSENS_D': 'SldoDig',
            'TEMPSENS_C': 'Acb'
        }
        measurement_out = {}
        for sensor in ['TEMPSENS_A', 'TEMPSENS_C', 'TEMPSENS_D']:
            data = eval('self.root.' + sensor + '[:]')
            mux = self.voltage_mux[sensor]
            measurement_out[mux] = {'X': False, 'Unit': 'V', 'Values': []}
            measurement_out['MonSens' + sensor_registers[sensor] + 'SelBias'] = {'X': False, 'Unit': '-', 'Values': []}
            measurement_out['MonSens' + sensor_registers[sensor] + 'Dem'] = {'X': False, 'Unit': '-', 'Values': []}

            for row in data:
                measurement_out[mux]['Values'].append(float(row[1]))
                measurement_out['MonSens' + sensor_registers[sensor] + 'SelBias']['Values'].append(0)
                measurement_out['MonSens' + sensor_registers[sensor] + 'Dem']['Values'].append(int(row[0]))
            for row in data:
                measurement_out[mux]['Values'].append(float(row[2]))
                measurement_out['MonSens' + sensor_registers[sensor] + 'SelBias']['Values'].append(1)
                measurement_out['MonSens' + sensor_registers[sensor] + 'Dem']['Values'].append(int(row[0]))

        data = self.root.NTC[:]
        measurement_out['TExtExtNTC'] = {'X': False, 'Unit': 'C', 'Values': []}
        measurement_out[self.voltage_mux['NTC_PAD']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out[self.voltage_mux['GNDA30']] = {'X': False, 'Unit': 'V', 'Values': []}

        measurement_out[self.current_mux['NTC_PAD']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out['Imux63'] = {'X': False, 'Unit': 'V', 'Values': []}
        for row in data:
            measurement_out['TExtExtNTC']['Values'].append(float(row[4]))
            measurement_out[self.voltage_mux['NTC_PAD']]['Values'].append(float(row[0]))
            measurement_out[self.voltage_mux['GNDA30']]['Values'].append(float(row[1]))
            measurement_out[self.current_mux['NTC_PAD']]['Values'].append(float(row[2]))
            measurement_out['Imux63']['Values'].append(float(row[3]))

        self.output['results']['Measurements'] = measurement_out

    def _get_measurements_sldo(self):
        measurement_out = {}
        DAC_data = self.root.dac_data[:]

        measurement_out['SetCurrent'] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out['Current'] = {'X': True, 'Unit': 'A', 'Values': []}

        measurement_out[self.voltage_mux['VINA_HALF']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out[self.voltage_mux['VIND_HALF']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out[self.voltage_mux['VDDA_HALF']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out[self.voltage_mux['VDDD_HALF']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out[self.voltage_mux['VOFS_HALF']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out[self.voltage_mux['VREF_PRE']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out['Vmux30'] = {'X': False, 'Unit': 'V', 'Values': []}

        measurement_out[self.current_mux['IREF']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out[self.current_mux['IINA']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out[self.current_mux['I_SHUNT_A']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out[self.current_mux['IIND']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out[self.current_mux['I_SHUNT_D']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out['Imux63'] = {'X': False, 'Unit': 'A', 'Values': []}

        measurement_out['Temperature'] = {'X': False, 'Unit': 'C', 'Values': []}

        for dac_row in DAC_data:
            measurement_out['SetCurrent']['Values'].append(float(dac_row[1]))
            measurement_out['Current']['Values'].append(float(dac_row[15]))

            measurement_out[self.voltage_mux['VINA_HALF']]['Values'].append(float(dac_row[2]))
            measurement_out[self.voltage_mux['VIND_HALF']]['Values'].append(float(dac_row[3]))
            measurement_out[self.voltage_mux['VDDA_HALF']]['Values'].append(float(dac_row[4]))
            measurement_out[self.voltage_mux['VDDD_HALF']]['Values'].append(float(dac_row[5]))
            measurement_out[self.voltage_mux['VOFS_HALF']]['Values'].append(float(dac_row[6]))
            measurement_out[self.voltage_mux['VREF_PRE']]['Values'].append(float(dac_row[7]))
            measurement_out['Vmux30']['Values'].append(float(dac_row[8]))

            measurement_out[self.current_mux['IREF']]['Values'].append(float(dac_row[9]))
            measurement_out[self.current_mux['IINA']]['Values'].append(float(dac_row[10]))
            measurement_out[self.current_mux['I_SHUNT_A']]['Values'].append(float(dac_row[11]))
            measurement_out[self.current_mux['IIND']]['Values'].append(float(dac_row[12]))
            measurement_out[self.current_mux['I_SHUNT_D']]['Values'].append(float(dac_row[13]))
            measurement_out['Imux63']['Values'].append(float(dac_row[16]))

            measurement_out['Temperature']['Values'].append(float(dac_row[14]))

        self.output['results']['Measurements'] = measurement_out

    def _get_measurements_low_power(self):
        measurement_out = {}
        DAC_data = self.root.dac_data[:]

        measurement_out['SetCurrent'] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out['Current'] = {'X': True, 'Unit': 'A', 'Values': []}

        measurement_out[self.voltage_mux['VINA_HALF']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out[self.voltage_mux['VIND_HALF']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out[self.voltage_mux['VOFS_HALF']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out['Vmux30'] = {'X': False, 'Unit': 'V', 'Values': []}

        measurement_out[self.current_mux['IREF']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out[self.current_mux['IINA']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out[self.current_mux['I_SHUNT_A']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out[self.current_mux['IIND']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out[self.current_mux['I_SHUNT_D']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out['Imux63'] = {'X': False, 'Unit': 'A', 'Values': []}

        measurement_out['Temperature'] = {'X': False, 'Unit': 'C', 'Values': []}
        measurement_out['FailingPixels'] = {'X': False, 'Unit': 'Pixels', 'Values': []}

        for dac_row in DAC_data:
            measurement_out['SetCurrent']['Values'].append(float(dac_row[1]))
            measurement_out['Current']['Values'].append(float(dac_row[9]))

            measurement_out[self.voltage_mux['VINA_HALF']]['Values'].append(float(dac_row[2]))
            measurement_out[self.voltage_mux['VIND_HALF']]['Values'].append(float(dac_row[3]))
            measurement_out[self.voltage_mux['VOFS_HALF']]['Values'].append(float(dac_row[4]))
            measurement_out['Vmux30']['Values'].append(float(dac_row[10]))

            measurement_out[self.current_mux['IREF']]['Values'].append(float(dac_row[14]))
            measurement_out[self.current_mux['IINA']]['Values'].append(float(dac_row[5]))
            measurement_out[self.current_mux['I_SHUNT_A']]['Values'].append(float(dac_row[6]))
            measurement_out[self.current_mux['IIND']]['Values'].append(float(dac_row[7]))
            measurement_out[self.current_mux['I_SHUNT_D']]['Values'].append(float(dac_row[8]))
            measurement_out['Imux63']['Values'].append(float(dac_row[11]))

            measurement_out['Temperature']['Values'].append(float(dac_row[12]))
            measurement_out['FailingPixels']['Values'].append(float(dac_row[13]))

        self.output['results']['Measurements'] = measurement_out

    def _get_measurements_inj_cap(self):
        measurement_out = {'Vmux4': {'X': False, 'Unit': 'V', 'Values': []},
                           'Vmux30': {'X': False, 'Unit': 'V', 'Values': []},
                           'Imux10': {'X': False, 'Unit': 'V', 'Values': []},
                           'Imux11': {'X': False, 'Unit': 'V', 'Values': []},
                           'Imux63': {'X': False, 'Unit': 'V', 'Values': []}}

        data = self.root.cap_meas_data[:]
        for row in data:
            measurement_out['Vmux4']['Values'].append(float(row[3]))
            measurement_out['Vmux30']['Values'].append(float(row[4]))
            measurement_out['Imux10']['Values'].append(float(row[1]))
            measurement_out['Imux11']['Values'].append(float(row[5]))
            measurement_out['Imux63']['Values'].append(float(row[2]))

        self.output['results']['Measurements'] = measurement_out

    def _get_measurements_OVP(self):
        measurement_out = {}
        DAC_data = self.root.dac_data[:]

        measurement_out['SetCurrent'] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out['Current'] = {'X': True, 'Unit': 'A', 'Values': []}

        measurement_out[self.voltage_mux['VINA_HALF']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out[self.voltage_mux['VIND_HALF']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out[self.voltage_mux['VREF_PRE']] = {'X': False, 'Unit': 'V', 'Values': []}
        measurement_out['Vmux30'] = {'X': False, 'Unit': 'V', 'Values': []}

        measurement_out[self.current_mux['IREF']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out[self.current_mux['IINA']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out[self.current_mux['IIND']] = {'X': False, 'Unit': 'A', 'Values': []}
        measurement_out['Imux63'] = {'X': False, 'Unit': 'A', 'Values': []}

        measurement_out['Temperature'] = {'X': False, 'Unit': 'C', 'Values': []}

        for dac_row in DAC_data:
            measurement_out['SetCurrent']['Values'].append(float(dac_row[1]))
            measurement_out['Current']['Values'].append(float(dac_row[7]))

            measurement_out[self.voltage_mux['VINA_HALF']]['Values'].append(float(dac_row[2]))
            measurement_out[self.voltage_mux['VIND_HALF']]['Values'].append(float(dac_row[3]))
            measurement_out[self.voltage_mux['VREF_PRE']]['Values'].append(float(dac_row[4]))
            measurement_out['Vmux30']['Values'].append(float(dac_row[10]))

            measurement_out[self.current_mux['IREF']]['Values'].append(float(dac_row[8]))
            measurement_out[self.current_mux['IINA']]['Values'].append(float(dac_row[5]))
            measurement_out[self.current_mux['IIND']]['Values'].append(float(dac_row[6]))
            measurement_out['Imux63']['Values'].append(float(dac_row[11]))

            measurement_out['Temperature']['Values'].append(float(dac_row[9]))

        self.output['results']['Measurements'] = measurement_out

    def _get_metadata(self):
        metadata_out = {
            "ChipID": au.ConfigDict(self.root.configuration_in.chip.settings[:])['chip_id'],
            "Name": self.chip_sn,
            "ModuleSN": self.module_sn,
            "Institution": self.institution,
            "FirmwareVersion": "",
            "FirmwareIdentifier": "",
            "ChannelConfig": "",
            "SoftwareVersion": self.run_config["software_version"],
            "ChipConfigs": {
                "RD53B": {
                    "GlobalConfig": {
                        # "AuroraActiveLanes": 1,
                        "CmlBias0": self.registers['DAC_CML_BIAS_0'],
                        "CmlBias1": self.registers['DAC_CML_BIAS_1'],
                        "CmlBias2": self.registers['DAC_CML_BIAS_2'],
                        "ServiceBlockEn": self.registers['ServiceDataConf'],
                        "DiffPreComp": self.registers['DAC_PRECOMP_DIFF'],
                        "DiffPreampL": self.registers['DAC_PREAMP_L_DIFF'],
                        "DiffPreampM": self.registers['DAC_PREAMP_M_DIFF'],
                        "DiffPreampR": self.registers['DAC_PREAMP_R_DIFF'],
                        "DiffPreampT": self.registers['DAC_PREAMP_T_DIFF'],
                        "DiffPreampTL": self.registers['DAC_PREAMP_TL_DIFF'],
                        "DiffPreampTR": self.registers['DAC_PREAMP_TR_DIFF'],
                        "EnCoreCol0": self.registers["EnCoreCol_0"],
                        "EnCoreCol1": self.registers["EnCoreCol_1"],
                        "EnCoreCol2": self.registers["EnCoreCol_2"],
                        "EnCoreCol3": self.registers["EnCoreCol_3"],
                        # "DataMergeOutMux0": 2,
                        # "DataMergeOutMux1": 3,
                        # "DataMergeOutMux2": 0,
                        # "DataMergeOutMux3": 1,
                        # "MonitorI": 63,
                        # "MonitorV": 7,
                        # "SerEnLane": 4,
                        "SldoTrimA": self.chip_trim['VREF_A_TRIM'],
                        "SldoTrimD": self.chip_trim['VREF_D_TRIM']
                    },
                    "Parameter": {
                        "ADCcalPar": [
                            self.chip_cal['ADC_b'],
                            self.chip_cal['ADC_a'],
                            4990.0  # FIX ME: WHAT IS THIS?
                        ],
                        "Name": self.run_config['chip_sn'],
                        "ChipId": au.ConfigDict(self.root.configuration_in.chip.settings[:])['chip_id'],
                        "InjCap": self.chip_cal['C_inj'],
                        "NfDSLDO": self.chip_cal['Nf_1'],
                        "NfASLDO": self.chip_cal['Nf_0'],
                        "NfACB": self.chip_cal['Nf_2'],
                        # "VcalPar": [
                        #     5.36,
                        #     0.216
                        # ],
                        # "IrefTrim": self.registers[],
                        "KSenseInA": self.chip_cal['shunt_sense_kInA'],
                        "KSenseInD": self.chip_cal['shunt_sense_kInD'],
                        "KSenseShuntA": self.chip_cal['shunt_sense_kA'],
                        "KSenseShuntD": self.chip_cal['shunt_sense_kD'],
                        "KShuntA": self.chip_cal['shunt_kA'],
                        "KShuntD": self.chip_cal['shunt_kD']
                    }
                }
            },
            "SoftwareType": "",
            "TimeStart": self.run_config['run_name'].split('_')[0] + '_' + self.run_config['run_name'].split('_')[1]
        }

        if self.scan_id in ['dac_linearity_scan', 'adc_tuning']:
            metadata_out['MonitorV'] = int(self.voltage_mux[self.scan_config['DAC']][4:])
            if self.scan_config['DAC'] in ['VCAL_HIGH', 'VCAL_HI', 'VCAL_MED']:
                if self.scan_config['vcal_range'].lower() == 'large':
                    metadata_out['ChipConfigs']["RD53B"]["GlobalConfig"]["InjVcalRange"] = 1
                else:
                    metadata_out['ChipConfigs']["RD53B"]["GlobalConfig"]["InjVcalRange"] = 0

        self.output['results']['Metadata'] = metadata_out


def get_h5_files(input_path, output_path):
    ignore = ['interpreted', 'digital_scan', 'analog_scan', 'threshold', 'efuse', 'qms']
    print(input_path)
    if os.path.isfile(input_path):
        if '.h5' in input_path:
            for i in ignore:
                if i in input_path:
                    return
            conv = JSONConverter(data_file=input_path, outpath=output_path)
            conv.convert_h5_to_json()
        else:
            return
    elif os.path.isdir(input_path):
        for p in os.listdir(input_path):
            get_h5_files(os.path.join(input_path, p), output_path)
    else:
        return


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument('-i', '--input-file', action='store', required=True, help='path to the input file')
    parser.add_argument('-o', '--output-dir', action='store', default=None, help='output directory')
    args = parser.parse_args()

    get_h5_files(args.input_file, args.output_dir)
