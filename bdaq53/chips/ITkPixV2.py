#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

from bdaq53.chips.ITkPixV1 import ITkPixV1


class ITkPixV2(ITkPixV1):
    '''
    Main class for ITkPixV2 chip
    '''
    chip_type = 'ITkPixV2'

    default_dac_values = {'DAC_PREAMP_L': 50,
                          'DAC_PREAMP_R': 50,
                          'DAC_PREAMP_TL': 50,
                          'DAC_PREAMP_TR': 50,
                          'DAC_PREAMP_T': 50,
                          'DAC_PREAMP_M': 50,
                          'DAC_PRECOMP': 50,
                          'DAC_COMP': 50,
                          'DAC_VFF': 100,
                          'DAC_TH1_L': 100,
                          'DAC_TH1_R': 100,
                          'DAC_TH1_M': 100,
                          'DAC_TH2': 0,
                          'DAC_LCC': 100,
                          'LEACKAGE_FEEDBACK': 0}

    # Inherits most funktionality from ITkPixV1

    # TODO: Override custom register usage:

    # DataMergingMux (renamed)
    def setup_aurora(self, tx_lanes=4, CB_Wait=255, CB_Send=1, only_cb=False, bypass_mode=False, receiver='rx0', lane_test=False, **_):
        if bypass_mode:
            self.log.info("Switching chip to BYPASS MODE.")
            self.registers['CdrConf'].set(7)
        elif self.bdaq['system']['AURORA_RX_640M'] == 1:
            self.log.info("Chip is running at 640Mb/s")
            self.registers['CdrConf'].set(1)
        else:
            self.log.info("Chip is running at 1.28Gb/s")
        if only_cb is False:
            self.log.debug("Aurora transmitter settings: Lanes=%u, CB_Wait=%u, CB_Send=%u", tx_lanes, CB_Wait, CB_Send)
            if tx_lanes == 4:
                self.log.info("4 Aurora lanes active")
                # Sets 4-lane-mode
                self.registers['AuroraConfig'].set(0b0111101101111)
                # Enable 4 CML outputs
                self.registers['CML_CONFIG'].set(0b00001111)
                self.registers['IOLaneMappingMux'].set(0b1110010000011011)
            elif tx_lanes == 3:
                self.log.info("3 Aurora lanes active")
                # Sets 3-lane-mode
                self.registers['AuroraConfig'].set(0b0011101101111)
                # Enable 2 CML outputs
                self.registers['CML_CONFIG'].set(0b00000111)
                self.registers['IOLaneMappingMux'].set(0b1110010000011011)
            elif tx_lanes == 2:
                self.log.info("2 Aurora lanes active")
                # Sets 2-lane-mode
                self.registers['AuroraConfig'].set(0b0001101101111)
                # Enable 2 CML outputs
                self.registers['CML_CONFIG'].set(0b00000011)
                self.registers['IOLaneMappingMux'].set(0b1110010000011011)
            elif tx_lanes == 1:
                self.log.info("1 Aurora lane active")
                # Sets 1-lane-mode
                if receiver == 'rx3' and lane_test:
                    self.log.info("Configuring aurora lane 3 on rx3")
                    self.registers['AuroraConfig'].set(0b0000101101111)
                    self.registers['CML_CONFIG'].set(0b01010001)
                    self.registers['IOLaneMappingMux'].set(0b1110010000011011)
                elif receiver == 'rx2' and lane_test:
                    self.log.info("Configuring aurora lane 2 on rx2")
                    self.registers['AuroraConfig'].set(0b0000101101111)
                    self.registers['CML_CONFIG'].set(0b01010010)
                    self.registers['IOLaneMappingMux'].set(0b1110010000000000)
                elif receiver == 'rx1' and lane_test:
                    self.log.info("Configuring aurora lane 1 on rx1")
                    self.registers['AuroraConfig'].set(0b0000101101111)
                    self.registers['CML_CONFIG'].set(0b01010100)
                    self.registers['IOLaneMappingMux'].set(0b1110010000000000)
                else:
                    self.log.info("Configuring aurora lane 0 for general single lane receiver")
                    self.registers['AuroraConfig'].set(0b0000101101111)
                    self.registers['CML_CONFIG'].set(0b01011000)
                    self.registers['IOLaneMappingMux'].set(0b1110010000011011)
            else:
                self.log.error("Aurora lane configuration (1,2,3,4) must be specified")
        else:
            self.log.debug("Aurora settings: CB_Wait=%u, CB_Send=%u", CB_Wait, CB_Send)

        # Set CB frame distance and number
        self.registers['AURORA_CB_CONFIG0'].set(((CB_Wait << 4) | CB_Send & 0xf) & 0xffff)
        self.registers['AURORA_CB_CONFIG1'].set((CB_Wait >> 12) & 0xff)  # Set CB frame distance and number
        self.registers.write_all()

        # Reset Aurora and serializers
        self.send_global_pulse(bitnames=['reset_serializer'], pulse_width=0xff)
        self.write_command(self.write_sync(write=True) * 16)
        self.send_global_pulse(bitnames=['reset_aurora'], pulse_width=0xff)

    # PIX_MODE
    def get_pixel_mode_command(self, auto_row=False, write_TDAC=True, broadcast=False, hit_sample_mode=0, count_seu_errors=False):
        EnSEUCount = 0b1 if count_seu_errors else 0b0
        HitSampleMode = 0b1 if hit_sample_mode else 0b0
        Broadcast = 0b1 if broadcast else 0b0
        ConfWrConfig = 0b1 if write_TDAC else 0b0
        AutoRow = 0b1 if auto_row else 0b0
        self.PIX_MODERegister = (0x00000
                                 | EnSEUCount << 4
                                 | HitSampleMode << 3
                                 | Broadcast << 2
                                 | ConfWrConfig << 1
                                 | AutoRow)
        self.registers['PIX_MODE'].set(self.PIX_MODERegister)
        return self.registers['PIX_MODE'].get_write_command(self.PIX_MODERegister)

    # CdrConf
    # No differnt behavior required

    # GlobalPulseWidth
    def write_global_pulse(self, width, write=True):
        # width: up to 9 bits in 25ns steps
        return super().write_global_pulse(width, write=write)

    def get_ring_oscillators(self, pulse_width=30, clock_freq=40e6):
        '''
            Returns data of ring oscillators:
            {
                RING_OSC_i: {
                    raw_data: Raw register data, consisting of a cycle counter and the actual counter,
                    counter: Value of the actual counter (last 12 bits of register value),
                    frequency: Counter value devided by pulse length. In Hz.
                }
            }
            Note: clock_freq changed from 20 MHz to 40 MHz
        '''
        return super().get_ring_oscillators(pulse_width, clock_freq=clock_freq)

    # ServiceDataConf
    def enable_monitor_data(self, autoread_registers={}):
        '''
            Enables autoreads while the the read registers are defined by the dict autoread_registers
            if kept empty the default regist registers are read out
            Othewise the register name has to be provided for each channel between 0 and 7
        '''
        for channel, register in autoread_registers.items():
            if channel in [0, 1, 2, 3, 4, 5, 6, 7]:
                self.registers[f'AutoRead{channel:d}'].write(self.registers[register]['address'])
        self.bdaq.set_monitor_filter(receivers=self.receiver, mode='filter')
        self.registers['ServiceDataConf'].write(0x1ff)
        self.log.debug('Monitor data enabled')

    def disable_monitor_data(self):
        # TODO: For now resets to '0b000110010', but chip default is '0b100110010'
        return super().disable_monitor_data()

    def reset(self, enable_core_columns=True, reset_monitor_data=False):
        return super().reset(enable_core_columns=enable_core_columns, reset_monitor_data=reset_monitor_data)

    # DataMerging
    def enable_data_merging(self, reset=False, en_clk_gating=0b0, clc=0b0, data_en=0b0001):
        # TODO: global pulse reset_data_merging not available anymore
        # if reset:
        #     self.send_global_pulse(bitnames=['reset_data_merging'], pulse_width=0xff)

        DataMergingInputPolarityInvert = 0b0000
        EnChipId = 0b0
        EnGatingDataMergeClk1280 = en_clk_gating  # 0: clock enable
        SelDataMergeClk = clc  # 0: 640 MHz, 1: 1280 MHz
        EnDataMerge = data_en
        MergeChBonding = 0b0
        DataMergingGpoSel = 0b1
        self.DataMergingRegister = (0x000
                                    | DataMergingInputPolarityInvert << 9
                                    | EnChipId << 8
                                    | EnGatingDataMergeClk1280 << 7
                                    | SelDataMergeClk << 6
                                    | EnDataMerge << 2
                                    | MergeChBonding << 1
                                    | DataMergingGpoSel)
        self.registers['DataMerging'].write(self.DataMergingRegister)

    # DataConcentratorConf
    # Not used in ITkPixV1 class

    # CoreColEncoderConf
    def use_hmap_compression(self, compress_hmap):
        self.CoreColEncoderConfRegister = (0b0
                                           | (not compress_hmap) << 12
                                           | 0b000000000000)
        self.registers['CoreColEncoderConf'].write(self.CoreColEncoderConfRegister)  # do not compress hmap

    # AuroraConfig
    # TODO: Check whether new bit should get implemnted (Send alternate output)

    # TODO: Add ITkPixV2 functionality

    def get_iref_trim_and_chip_id(self):
        data = self.registers['PadReadout'].read()
        iref_trim = data % 16
        chip_id = data // 16
        return int(iref_trim), int(chip_id)

    def configure_datamerging_deserializer(self, fixed_mode=0b0, manual_mode=0b0000, phase=0x00):
        self.PhaseDetectorConfigRegister = (0x0
                                            | fixed_mode << 12
                                            | manual_mode << 8
                                            | phase)
        print(f"{self.PhaseDetectorConfigRegister:013b}")
        self.registers['PhaseDetectorConfig'].write(self.PhaseDetectorConfigRegister)


if __name__ == '__main__':
    ITkPixV2_chip = ITkPixV2()
    ITkPixV2_chip.init()
