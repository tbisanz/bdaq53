#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#


'''
    This meta script performs a threshold scan for every parameter in a range defined py 'parameter_start', 'parameter'stop'
    and 'parameter_step'. In the end, the mean threshold and noise are plotted as a function of the parameter.
'''

import os
import time
import tables as tb
import matplotlib.pyplot as plt
from copy import deepcopy

from bdaq53.system import logger
from bdaq53.scans.scan_threshold_autorange import FastThrScan


scan_configuration = {
    'parameter_name': 'LDAC_LIN',
    'parameter_start': 100,
    'parameter_stop': 150,
    'parameter_step': 10,
}

threshold_scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    # Start threshold scan at injection setting where a fraction of min_response of the selected
    # pixels see the fraction of hits min_hits
    'min_response': 0.01,
    'min_hits': 0.2,
    # Start threshold scan at injection setting where a fraction of max_response of the selected
    # pixels see the fraction of hits max_hits
    'max_response': 0.99,
    'max_hits': 1.,

    'n_injections': 200,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 540,  # start value of injection
    'VCAL_HIGH_stop': 1500,  # maximum injection, can be None
    'VCAL_HIGH_step_fine': 8,  # step size during threshold scan
    'VCAL_HIGH_step_coarse': 100  # step when seaching start
}


class MetaDataTable(tb.IsDescription):
    parameter = tb.StringCol(64, pos=0)
    value = tb.StringCol(64, pos=1)


class DataTable(tb.IsDescription):
    parameter = tb.UInt32Col(pos=0)
    threshold = tb.Float64Col(pos=1)
    noise = tb.Float64Col(pos=2)


class MetaParamThrScn(object):
    def __init__(self):
        self.log = logger.setup_derived_logger('MetaParamThrScn')

        # Need ScanBase __init__ to obtain multi-chip/multi-module variable lists
        # TODO: temporary turn off logging here
        tmp_scan = FastThrScan()
        self._output_directories = tmp_scan._output_directories_per_scan
        self._scan_configuration_per_scan = tmp_scan._scan_configuration_per_scan
        del tmp_scan

        self._output_filename = time.strftime("%Y%m%d_%H%M%S") + '_meta_parametric_threshold_scan'

        self._output_filenames = [os.path.join(directory, self._output_filename) for directory in self._output_directories]

        self._h5_files = []
        self._meta_data_tables = []
        self._raw_data_tables = []

        for filename in self._output_filenames:
            h5file = tb.open_file(filename + '.h5', 'w')
            meta_data_table = h5file.create_table(h5file.root, name='meta_data', title='Meta Data', description=MetaDataTable)
            for p, v in scan_configuration.items():
                row = meta_data_table.row
                row['parameter'] = p
                row['value'] = v
                row.append()
            meta_data_table.flush()
            raw_data_table = h5file.create_table(h5file.root, name='data', title='Data', description=DataTable)

            self._h5_files.append(h5file)
            self._meta_data_tables.append(meta_data_table)
            self._raw_data_tables.append(raw_data_table)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            self.log.error(exc_value)
            self.log.error('Scan failed!')

    def scan(self, parameter_name='LDAC_LIN', parameter_start=50, parameter_stop=180, parameter_step=1, **_):
        param_range = range(parameter_start, parameter_stop, parameter_step)

        for param in param_range:
            self.log.info('Performing threshold scan for %s = %i' % (parameter_name, param))

            threshold_scan_config = deepcopy(threshold_scan_configuration)
            threshold_scan_config['chip'] = {'registers': {parameter_name: param}}

            with FastThrScan(scan_config=threshold_scan_config) as scn:
                scn.scan()
                results_dict = scn.analyze()

            # Create sorted list from return dict
            results_list = []
            for module, results in sorted(results_dict.items()):
                for chip, result in sorted(results.items()):
                    results_list.append(result)

            for raw_data_table, result in zip(self._raw_data_tables, results_list):
                mean_thr, mean_noise = result
                row = raw_data_table.row
                row['parameter'] = param
                row['threshold'] = mean_thr
                row['noise'] = mean_noise
                row.append()
                raw_data_table.flush()

        for h5file in self._h5_files:
            h5file.close()

    def analyze(self):
        for filename in self._output_filenames:
            with tb.open_file(filename + '.h5') as in_file:
                parameter_table = in_file.root.meta_data[:]
                data_table = in_file.root.data[:]

            parameters = {}
            for tup in parameter_table:
                key = tup[0].decode('utf-8')
                value = tup[1].decode('utf-8')
                if key in ['start_column', 'stop_column', 'start_row', 'stop_row', 'parameter_start', 'parameter_stop', 'parameter_step']:
                    parameters[key] = int(value)
                else:
                    parameters[key] = value

            x, threshold, noise = [], [], []
            for param, thr, n in data_table:
                x.append(param)
                threshold.append(thr)
                noise.append(n)

            fig = plt.figure()
            ax1 = fig.add_subplot(111)
            plt.title('BDAQ53 parametric threshold scan')
            ax1.set_xlabel(parameters['parameter_name'])

            lns1 = ax1.plot(x, threshold, 'bo', label='Threshold')
            ax1.tick_params('y', colors='b')
            ax1.set_ylabel('Threshold [Delta VCAL]', color='b')

            ax2 = ax1.twinx()
            lns2 = ax2.plot(x, noise, 'ro', label='Noise')
            ax2.tick_params('y', colors='r')
            ax2.set_ylabel('Noise [Delta VCAL]', color='r')

            ax1.grid()
            lns = lns1 + lns2
            labs = [l.get_label() for l in lns]
            ax1.legend(lns, labs, loc=0)

            plt.savefig(filename + '.pdf')


if __name__ == "__main__":
    with MetaParamThrScn() as scan:
        scan.scan(**scan_configuration)
        scan.analyze()
