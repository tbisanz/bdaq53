#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test scans over the values of the selected DAC and
    measures the resulting analog value with the chip's internal ADC.

    Version adapted to ITk_Pix_V1
'''
import time

import tables as tb
from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import plotting
from bdaq53.analysis import analysis_utils as au
from bdaq53.module_QC.QC_test import QC_Test
from bdaq53.analysis.convert_to_json import JSONConverter

from scipy.optimize import curve_fit
import traceback


scan_configuration = {
    'periphery_device': 'Multimeter',
    'DAC': 'VCAL_MED',
    'vcal_range': 'Large',  # only needed for Vcal Calibration. Accepts Large and Small.
    'measure': 'voltage',
    'value_start': 500,
    'value_stop': 3500,
    'value_step': 100,
    'type': 'U',  # needed since configure_in is written before configuring the chip
    'calibrate_ADC': True,  # set to true to use performed scan to calibrate the internal ADC
    'module_QC': {  # design parameters for module QC - is only checked if ADC calibration is performed
        'slope': 0.187,
        'slope_tolerance': 0.037,
        'offset': 11,
        'offset_tolerance': 20
    }
}


def linear(x, a, b):
    return (a * x) + b


class DacTable(tb.IsDescription):
    scan_param_id = tb.UInt32Col(pos=0)
    dac = tb.UInt32Col(pos=1)
    adc = tb.UInt32Col(pos=2)
    voltage = tb.Float32Col(pos=3)
    voltage_gnd = tb.Float32Col(pos=4)


class DACScan(ScanBase):
    scan_id = 'dac_linearity_scan'

    def _configure(self, DAC='VCAL_HIGH', measure='voltage', vcal_range=None, **_):
        if not self.periphery.enabled:
            raise IOError('Periphery has to be enabled and a multimeter configured correctly!')

        self.chip.set_mux(DAC, measure)

        if vcal_range:
            self._set_InjVcal_Range(vcal_range.lower())

        self.configuration['scan']['type'] = 'I' if DAC in self.chip.current_mux.keys() else 'U'
        # FIX ME: this step is too late to save type in configure_in.
        # Leads to analysis failing if it is not specified in scan_configuration

    def _scan(self, DAC='VCAL_HIGH', value_start=0, value_stop=4096, value_step=100, periphery_device='Multimeter', **_):
        '''
        DAC linearity scan main loop

        Parameters
        ----------
        DAC : str
            Name of the DAC to scan
        value_start : int
            First value for this DAC
        value_stop : int
            Last value of this DAC, exclusive
        value_step : int
            Steps between DAC values
        '''

        value_range = range(value_start, value_stop + 1, value_step)

        rows = []
        for scan_param_id, value in tqdm(enumerate(value_range), total=len(value_range), unit=' Values'):
            with self.readout(scan_param_id=scan_param_id):
                self.chip.registers[DAC].write(value)
                time.sleep(0.005)  # Sleep to ensure system has settled to the new voltage.
                adc_value = self.chip.get_ADC_value(DAC, measure='v')[0]

                voltage, voltage_gnd = self._get_voltage_with_gnd(DAC=DAC, periphery_device=periphery_device)

                rows.append((value, adc_value, voltage, voltage_gnd, scan_param_id))

        self.data.dac_data_table = self.h5_file.create_table(self.h5_file.root, name='dac_data',
                                                             title='dac_data', description=DacTable)
        row = self.data.dac_data_table.row
        for value, adc_value, voltage, voltage_gnd, scan_param_id in rows:
            row['dac'] = value
            row['adc'] = adc_value
            row['voltage'] = voltage
            row['voltage_gnd'] = voltage_gnd
            row['scan_param_id'] = scan_param_id
            row.append()
            self.data.dac_data_table.flush()

        self.log.success('Scan finished')

    def _get_voltage_with_gnd(self, DAC='VCAL_HIGH', measure='voltage', periphery_device='Multimeter'):
        self.chip.set_mux(DAC, measure)
        time.sleep(0.005)
        _voltage = self.periphery.aux_devices[periphery_device].get_voltage()

        if self.chip_settings['chip_type'].lower() in ['itkpixv1', 'crocv1', 'itkpixv2']:
            gnd_name = 'GNDA30'
        else:
            gnd_name = 'ground'

        self.chip.set_mux(gnd_name, measure)
        time.sleep(0.005)
        _voltage_gnd = self.periphery.aux_devices[periphery_device].get_voltage()

        return _voltage, _voltage_gnd

    def _set_InjVcal_Range(self, Vcal_range):
        if Vcal_range == 'large':
            _reg_val = int('0' + '0' + '1', 2)
        elif Vcal_range == 'small':
            _reg_val = int('0' + '0' + '0', 2)
        else:
            raise KeyError(f"{Vcal_range} is not an accepted InjVcalRange setting.")

        self.chip.registers['MEAS_CAP'].write(_reg_val)

    def _analyze(self, raw_data_file=None, analyzed_data_file=None):
        if not raw_data_file:
            raw_data_file = self.output_filename + '.h5'
        if not analyzed_data_file:
            analyzed_data_file = self.output_filename + '_interpreted.h5'

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=raw_data_file) as p:
                p.create_standard_plots()

        conv = JSONConverter(data_file=raw_data_file, outpath=self.working_dir)
        conv.convert_h5_to_json()

        if self.scan_config_par['calibrate_ADC']:
            try:
                adc_cal = self.calibrate_ADC(raw_data_file=raw_data_file, analyzed_data_file=analyzed_data_file)
            except Exception:
                self.log.error('Could not perform ADC calibration!')
                self.chip.QC_passed = False
                traceback.print_exc()
                adc_cal = None, None

            return adc_cal

    def calibrate_ADC(self, raw_data_file=None, analyzed_data_file=None):
        with tb.open_file(raw_data_file, 'r') as in_file:
            scan_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])
            DAC_data = in_file.root.dac_data

            ADC_data = DAC_data.cols.adc[:]
            voltage_data = DAC_data.cols.voltage[:] - DAC_data.cols.voltage_gnd[:]

            popt, pcov = curve_fit(linear, ADC_data, voltage_data)
            slope, offset = popt[0], popt[1]

            # convert from V/LSB to mV/LSB
            slope *= 1000
            offset *= 1000

            if 'module_QC' in scan_config:
                # if QC parameters are specified, perform QC test
                self.chip.QC_passed = True

                QC_slope = scan_config['module_QC']['slope']
                QC_slope_tolerance = scan_config['module_QC']['slope_tolerance']
                QC_offset = scan_config['module_QC']['offset']
                QC_offset_tolerance = scan_config['module_QC']['offset_tolerance']

                with QC_Test(output_file=self.output_filename) as QC:
                    passed = QC.QC_test('Slope', {'design_value': QC_slope, 'pass_tolerance': QC_slope_tolerance, 'measure': 'slope'}, slope)
                    self.chip.QC_passed = self.chip.QC_passed and passed
                    passed = passed and QC.QC_test('Offset', {'design_value': QC_offset, 'pass_tolerance': QC_offset_tolerance, 'measure': 'offset'}, offset)
                    self.chip.QC_passed = self.chip.QC_passed and passed

            with tb.open_file(analyzed_data_file, 'w', title=in_file.title) as out_file:
                out_file.create_group(out_file.root, name='configuration_in', title='Configuration after scan step')
                out_file.copy_children(in_file.root.configuration_out, out_file.root.configuration_in, recursive=True)
                in_file.root.dac_data.copy(out_file.root)

        # enter the ADC calibration into the chip configuration
        self.log.info('Changed ADC calibration (ADC_a, ADC_b) to (%.3f, %.3f)' % (slope, offset))
        self.chip.calibration.ADC_a = slope
        self.chip.calibration.ADC_b = offset

        return slope, offset


if __name__ == "__main__":
    with DACScan(scan_config=scan_configuration) as scan:
        scan.start()
