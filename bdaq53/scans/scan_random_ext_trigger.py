#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Randon trigger scan based on pulser in FPGA with configurable trigger frequency.
'''

import tables as tb

from bdaq53.utils import get_latest_h5file
from bdaq53.scans.calibrate_hitor import HitorCalib
from bdaq53.scans.scan_ext_trigger import ExtTriggerScan
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 384,

    # Stop conditions (choose one)
    'scan_timeout': 10 * 60,          # Timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': False,          # Number of maximum received triggers after stopping readout, if False no limit on received trigger

    'trigger_frequency': 50,   # Trigger frequency, 100 corresponds to 10 kHz
    'trigger_latency': 100,     # Latency of trigger in units of 25 ns (BCs), RD53A: 100, ITkPix: 98
    'trigger_delay': 57,        # Trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,       # Length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 50,         # Length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Increase by factor of number of connected chips/hitors. Should also be adjusted for longer trigger length.

    # Trigger configuration
    'bench': {'TLU': {
        'TRIGGER_MODE': 0,      # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 512      # Selecting trigger input: HitOR [DP_ML_5 and mDP] (3), HitOR [mDP only] (2), HitOR [DP_ML_5 only] (1), disabled (0)
    }
    }
}


class RandomExtTriggerScan(ExtTriggerScan):
    scan_id = 'random_ext_trigger_scan'

    def _configure(self, scan_timeout=10, max_triggers=False, trigger_frequency=100, trigger_length=32, trigger_delay=57, veto_length=500, trigger_latency=100, start_column=0, stop_column=400, start_row=0, stop_row=192, use_tdc=False, **kwargs):
        '''
        Parameters
        ----------
        max_triggers : int / False
            Maximum amount of triggers to record. Set to False for no limit.
        trigger_length : int
            Amount of BCIDs to read out on every trigger.
        trigger_delay : int
            Delay the trigger command by this amount in units of 25ns.
        veto_length : int
            Length of TLU veto in units of 25ns.
        trigger_latency : int
            Latency of trigger in units of 25ns.
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        super(RandomExtTriggerScan, self)._configure(scan_timeout=scan_timeout, max_triggers=max_triggers, trigger_length=trigger_length, trigger_delay=trigger_delay, veto_length=veto_length, trigger_latency=trigger_latency, start_column=start_column, stop_column=stop_column, start_row=start_row, stop_row=stop_row, use_tdc=use_tdc, **kwargs)
        self.chip.masks.update(force=True)

        self.bdaq['pulser_ext_trig'].set_delay(int(trigger_frequency * 160))
        self.bdaq['pulser_ext_trig'].set_width(4)
        self.bdaq['pulser_ext_trig'].set_repeat(0)

    def _scan(self, scan_timeout=10, max_triggers=False, use_tdc=False, **_):
        self.bdaq['pulser_ext_trig'].start()
        super(RandomExtTriggerScan, self)._scan(scan_timeout=scan_timeout, max_triggers=max_triggers, use_tdc=use_tdc, **_)
        self.bdaq['pulser_ext_trig'].reset()

    def _analyze(self):
        hitor_calib_file = self.configuration['scan'].get('hitor_calib_file', None)
        self.configuration['bench']['analysis']['cluster_hits'] = True
        self.configuration['bench']['analysis']['store_hits'] = True
        self.configuration['bench']['analysis']['analyze_tdc'] = self.configuration['scan'].get('use_tdc', False)

        if hitor_calib_file == 'auto':
            hitor_calib_file = get_latest_h5file(directory=self.output_directory, scan_pattern=HitorCalib.scan_id, interpreted=True)

        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', hitor_calib_file=hitor_calib_file, **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        try:
            with tb.open_file(a.analyzed_data_file) as in_file:
                clusters = in_file.root.Cluster[:]
                n_clusters = clusters.shape[0]

            if self.configuration['bench']['analysis']['create_pdf']:
                with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                    p.create_standard_plots()

            return n_clusters

        except IOError:
            self.log.warning("Skip plotting.")

        return


if __name__ == '__main__':
    with RandomExtTriggerScan(scan_config=scan_configuration) as scan:
        scan.start()
