#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This test measures the OVP performance. The input current is increased in steps and for
    each step, the resulting voltages VinA, VinD, Vref_pre and the currents
    Iref, IinA, IinD are measured.
'''

import time

import numpy as np
import tables as tb
import yaml
import traceback

from bdaq53.system.scan_base import ScanBase
from bdaq53.module_QC.QC_test import QC_Test
from bdaq53.analysis.convert_to_json import JSONConverter


scan_configuration = {
    'periphery_device': 'Multimeter',
    'Iin_OVP': 7,
    'OVP_voltage': 8.5,
    'Iin_nominal': 5.88,
    'voltage_limit': 3.0  # !!! Voltage limit to protect the chip. Be careful when setting it above 2.0V !!!
}


class DacTable(tb.IsDescription):
    scan_param_id = tb.UInt32Col(pos=0)
    Iin = tb.Float32Col(pos=1)
    VinA = tb.Float32Col(pos=2)
    VinD = tb.Float32Col(pos=3)
    VrefOVP = tb.Float32Col(pos=4)
    voltage_gnd = tb.Float32Col(pos=10)

    IinA = tb.Float32Col(pos=5)
    IinD = tb.Float32Col(pos=6)
    IinMeas = tb.Float32Col(pos=7)
    Iref = tb.Float32Col(pos=8)
    current_gnd = tb.Float32Col(pos=11)

    T_NTC = tb.Float32Col(pos=9)


class OVPAnalysedTable(tb.IsDescription):
    Iin = tb.Float32Col(pos=0)
    VinA = tb.Float32Col(pos=1)
    VinD = tb.Float32Col(pos=2)
    VrefOVP = tb.Float32Col(pos=3)
    VinTheo = tb.Float32Col(pos=4)

    Iref = tb.Float32Col(pos=5)
    IinA = tb.Float32Col(pos=6)
    IinD = tb.Float32Col(pos=7)
    IinMeas = tb.Float32Col(pos=8)


class OVPQCTable(tb.IsDescription):
    attribute = tb.StringCol(64, pos=0)
    value = tb.StringCol(64, pos=1)
    passed_QC = tb.StringCol(64, pos=2)


class OVP_Scan(ScanBase):
    scan_id = 'OVP_Scan'

    def _configure(self, Iin_OVP=7, OVP_voltage=8.5, **_):

        if not self.periphery.enabled:
            raise IOError('Periphery has to be enabled and a multimeter configured correctly!')

        # FIXME: move scan base
        if hasattr(self, 'module_slot'):
            self.ntc_slot = int(self.module_slot.split('_')[-1])
            self.ntc_slot -= 1
        else:
            self.ntc_slot = 0

    def _scan(self, Iin_OVP=7, OVP_voltage=8.5, periphery_device='Multimeter', NTC_periphery='Arduino', **_):
        # set Iin on Powersupply
        self.periphery.module_devices[self.module_settings['name']]['LV'].set_voltage(OVP_voltage)
        self.periphery.module_devices[self.module_settings['name']]['LV'].set_current_limit(Iin_OVP)
        time.sleep(0.5)

        self._set_mux_get_voltage(mux_select='VINA_HALF', measure='voltage', periphery_device=periphery_device)
        # FIX ME: Multimeter returns negative value on first measurement. On subsequent measurements the correct value is returned.

        self.data.dac_data_table = self.h5_file.create_table(self.h5_file.root, name='dac_data',
                                                             title='dac_data', description=DacTable)
        row = self.data.dac_data_table.row

        row['Iin'] = Iin_OVP
        row['IinMeas'] = self.periphery.module_devices[self.module_settings['name']]['LV'].get_current()
        # get voltages
        row['VinA'] = self._set_mux_get_voltage(mux_select='VINA_HALF', measure='voltage', periphery_device=periphery_device)
        row['VinD'] = self._set_mux_get_voltage(mux_select='VIND_HALF', measure='voltage', periphery_device=periphery_device)
        row['VrefOVP'] = self._set_mux_get_voltage(mux_select='VREF_PRE', measure='voltage', periphery_device=periphery_device)
        row['voltage_gnd'] = self._set_mux_get_voltage(mux_select='GNDA30', measure='voltage', periphery_device=periphery_device)

        # get currents
        row['Iref'] = self._set_mux_get_voltage(mux_select='IREF', measure='current', periphery_device=periphery_device)
        row['IinA'] = self._set_mux_get_voltage(mux_select='IINA', measure='current', periphery_device=periphery_device)
        row['IinD'] = self._set_mux_get_voltage(mux_select='IIND', measure='current', periphery_device=periphery_device)
        row['current_gnd'] = self._set_mux_get_voltage(mux_select='I_MUX', measure='voltage', periphery_device=periphery_device)

        if NTC_periphery in self.periphery.aux_devices:
            row['T_NTC'] = self.periphery.aux_devices[NTC_periphery].get_temperature(sensor=self.ntc_slot)[str(self.ntc_slot)]
        else:
            print(self.periphery.aux_devices)
            row['T_NTC'] = 0

        row.append()
        self.data.dac_data_table.flush()
        self.log.success('QC Scan finished')

    def _set_mux_get_voltage(self, mux_select='VINA_HALF', measure='voltage', periphery_device='Multimeter'):
        self.chip.set_mux(mux_select, measure)
        time.sleep(0.1)
        if measure in ('voltage', 'v'):
            return self.periphery.aux_devices[periphery_device].get_voltage()
        else:
            return self.periphery.aux_devices[periphery_device].get_voltage()

    def _analyze(self, raw_data_file=None, analyzed_data_file=None):
        if not raw_data_file:
            raw_data_file = self.output_filename + '.h5'
        if not analyzed_data_file:
            analyzed_data_file = self.output_filename + '_interpreted.h5'

        with tb.open_file(raw_data_file, 'r') as in_file:
            DAC_data = in_file.root.dac_data[:]
            with tb.open_file(analyzed_data_file, 'w', title=in_file.title) as self.out_file:
                self.out_file.create_group(self.out_file.root, name='configuration_in', title='Configuration after scan step')
                self.out_file.copy_children(in_file.root.configuration_out, self.out_file.root.configuration_in, recursive=True)

                self._process_raw_data(DAC_data)
                self._test_module_QC_working_point(analysis_parameter_path='../module_QC/QC_config_files/OVP_analog_readback_analysis.yaml')

        self._test_module_QC_IV(0.03)

        conv = JSONConverter(data_file=raw_data_file, outpath=self.working_dir)
        conv.convert_h5_to_json()

    def _process_raw_data(self, DAC_data):
        R_Imux = 10000
        m_Iin = 21000
        m_in = m_Iin / R_Imux

        self.data.Iin, self.data.IinMeas, self.data.VinTheo = [], [], []
        self.data.VinA, self.data.VinD, self.data.VrefOVP = [], [], []
        self.data.Iref, self.data.IinA, self.data.IinD = [], [], []

        output = self.out_file.create_table(self.out_file.root, name='ovp_qc_data', title='OVP_QC_data', description=OVPAnalysedTable)
        row = output.row
        for dac in DAC_data:
            row['Iin'] = dac[1]
            self.data.Iin.append(row['Iin'])
            row['IinMeas'] = dac[8]
            self.data.IinMeas.append(row['IinMeas'])
            row['VinTheo'] = self._SLDO_vin_theo(dac[8])
            self.data.VinTheo.append(row['VinTheo'])
            row['VinA'] = (dac[2] - dac[10]) * 4
            self.data.VinA.append(row['VinA'])
            row['VinD'] = (dac[3] - dac[10]) * 4
            self.data.VinD.append(row['VinD'])
            row['VrefOVP'] = (dac[4] - dac[10]) * 3.33
            self.data.VrefOVP.append(row['VrefOVP'])
            row['Iref'] = (dac[8] - dac[11]) / R_Imux
            self.data.Iref.append(row['Iref'])
            row['IinA'] = (dac[5] - dac[11]) * m_in
            self.data.IinA.append(row['IinA'])
            row['IinD'] = (dac[6] - dac[11]) * m_in
            self.data.IinD.append(row['IinD'])
            row.append()
            output.flush()

    def _SLDO_vin_theo(self, Iin, nr_of_modules=4):
        Vofs = 1  # FIX ME: Vofs = 1.1 for L0 modules
        RextA = 866 * 4 / nr_of_modules  # 575
        RextD = 590 * 4 / nr_of_modules  # 535
        kA = self.chip.calibration.shunt_kA  # FIX ME: Values from Waiver Probing!
        kD = self.chip.calibration.shunt_kA  # FIX ME: Values from Waiver Probing!

        Reff = 1 / ((kA / RextA) + (kD / RextD))

        return Reff * Iin / nr_of_modules + Vofs

    def _test_module_QC_IV(self, QC_tolerance):
        VinA = np.array(self.data.VinA)
        VinD = np.array(self.data.VinD)
        VinTheo = np.array(self.data.VinTheo)
        test_QC = np.logical_and((abs(VinA - VinTheo) < QC_tolerance), (abs(VinD - VinTheo) < QC_tolerance))
        print(test_QC)

        if all(test_QC):
            print('QC test passed')
        else:
            for i in range(len(test_QC)):
                if not test_QC[-(i + 1)]:
                    IinMin = self.data.IinMeas[-(i + 1)]
                    break

            print('--------------------')
            print('!!! Test Failed !!!')
            print('QC criteria failed for Iin < %f A' % IinMin)
            print('--------------------')

        self.chip.QC_passed = all(test_QC)

    def _test_module_QC_working_point(self, Iin_nominal=5.9, analysis_parameter_path=None):
        if analysis_parameter_path and analysis_parameter_path != '':
            try:
                with open(analysis_parameter_path, 'r') as analysis_parameter_file:
                    self.analysis_parameters = yaml.safe_load(analysis_parameter_file)

                Iin = np.array(self.data.IinMeas)
                index = (np.abs(Iin - Iin_nominal)).argmin()
                print('Nominal current of %f at step %i' % (self.data.IinMeas[index], index))

                QC_passed = True
                with QC_Test() as QC:
                    output = self.out_file.create_table(self.out_file.root, name='OVP_QC', title='OVP_Performance_at_Nominal_Iin', description=OVPQCTable)
                    row = output.row
                    for key, parameters in self.analysis_parameters.items():
                        if key == 'VinMatch':
                            parameters['measure'] = 'voltage'
                            test_passed = QC.QC_test(key, parameters, data=[self.data.VinA[index], self.data.VinD[index]])
                            test_value = abs(self.data.VinA[index] - self.data.VinD[index])

                        else:
                            data = getattr(self.data, key, False)
                            if data:
                                test_value = data[index]
                                test_passed = QC.QC_test(key, parameters, test_value)
                            else:
                                continue

                        QC_passed = QC_passed and test_passed
                        row['attribute'] = key
                        row['value'] = test_value
                        row['passed_QC'] = test_passed
                        row.append()
                        output.flush()
                return QC_passed

            except Exception:
                self.log.warning('Could not perform QC at nominal current')
                traceback.print_exc()

    def normal_config(self, Iin_nominal=5.88, voltage_limit=3.0, **_):
        self.bdaq.reset_lpmode_pulse()
        self.periphery.module_devices[self.module_settings['name']]['LV'].set_voltage(voltage_limit)
        self.periphery.module_devices[self.module_settings['name']]['LV'].set_current_limit(Iin_nominal)


if __name__ == "__main__":
    with OVP_Scan(scan_config=scan_configuration) as scan:
        scan.start()
        for chip in scan.chips.values():
            print('%s: %s' % (chip.name, chip.chip.QC_passed))
    scan.normal_config()
