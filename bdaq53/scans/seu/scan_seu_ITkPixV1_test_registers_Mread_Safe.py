#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test writes random data into the specified registers,
    then reads back the data and compares read and written data.
'''

import random
import time
import tables as tb
import numpy as np
import os

from bdaq53.system.scan_base import ScanBase

scan_parameters_itkpixv1 = {
    'maskfile': 'REF/ref_mask.h5',  # Use proper reference files after tuning
    # 'maskfile': None,  # Use None to find proper threshold
    'ignore': ['PIX_PORTAL',
               'SEU53',
               'CdrConf',
               'GlobalPulseConf',
               'GlobalPulseWidth',
               'MonitorConfig',
               'MON_SENS_SLDO',
               'MON_SENS_ACB',
               'ServiceDataConf'
               ]
}

scan_parameters = scan_parameters_itkpixv1


# H5 file table that will be filled with register values
class ValueTable(tb.IsDescription):
    register = tb.StringCol(64, pos=0)
    write = tb.UInt16Col(pos=1)
    readAfterW = tb.UInt16Col(pos=2)
    read = tb.UInt16Col(pos=3)
    read2 = tb.UInt16Col(pos=4)


class RegisterTest(ScanBase):
    scan_id = 'register_test'

    def _scan(self, ignore=[], **_):
        '''
        Register test main loop

        Parameters
        ----------
        addresses : list
            List of registers to ignore
        '''

        value_table = self.h5_file.create_table(self.h5_file.root, name='values', title='Values', description=ValueTable)
        if ReadDuringBeam == 1:
            readDurBeam = self.h5_file.create_group(self.h5_file.root, 'readDurBeam', 'Read reg')

        self.log.info('Starting scan... by writing default register values')

        reg_name = []
        reg_w = []
        reg_r_after_w = []
        reg_during_beam = []
        nb_of_reg = 0

        for reg in self.chip.registers.values():
            if reg['mode'] != 1 or reg['reset'] == 0 or reg['name'] in ignore or reg['address'] in ignore:
                continue

            value = int(reg['default'])

            # Recording the registers name and written value
            reg_name.append(reg['name'])
            reg_w.append(value)

            # Writing the default register value
            reg.write(value)

            nb_of_reg += 1  # getting the total number of registers configured

        # Reading-back registers after writing
        for reg in self.chip.registers.values():
            if reg['mode'] != 1 or reg['reset'] == 0 or reg['name'] in ignore or reg['address'] in ignore:
                continue
            reg_r_after_w.append(reg.read())

        # Option A : communication reset after the beam
        if WhichInit == 1:
            self.log.info("Communication initialisation after the beam")
            if ReadDuringBeam == 0:  # Case of no register read while beam is ON
                self.log.info('Press ENTER when beam is stopped')  # Allow the operator to press ENTER after a spill/trigger/beam off
            else:  # Case of register read while beam is ON
                self.log.info('Press ENTER when beam is ON')  # Allow the operator to press ENTER when beam is turned ON

            input()

            if ReadDuringBeam == 1:  # Read reg while beam is ON
                self.log.info("Global register read during beam (CTRL C to stop)")
                temp = 0
                while True:
                    try:
                        time.sleep(5)  # Wait 1mn (default) between each reading
                        data_file = open(self.output_filename + '_r' + str(temp + 1) + '.txt', 'a+')
                        data_file_name = self.output_filename + '_r' + str(temp + 1) + '.txt'
                        temp += 1
                        self.log.info("Reading register #{t}".format(t=temp))
                        ireg = 0
                        for reg in self.chip.registers.values():
                            if reg['mode'] != 1 or reg['reset'] == 0 or reg['name'] in ignore or reg['address'] in ignore:
                                continue
                            reg_during_beam.append(reg.read())
                            data_to_save = []
                            ireg_to_check = len(reg_during_beam) - 1
                            if reg_during_beam[ireg_to_check] != reg_w[ireg]:
                                SEU_spotted = 1
                                self.log.info("SEU spotted for register #{t}".format(t=reg['name']))
                            else:
                                SEU_spotted = 0
                            data_to_save.append(reg['name'] + "\t" + str(reg_w[ireg]) + "\t" + str(reg_r_after_w[ireg]) + "\t" + str(reg_during_beam[ireg]) + "\t" + str(SEU_spotted))
                            np.savetxt(data_file, data_to_save, fmt="%s")
                            ireg += 1
                            adc = self.chip.get_ADC_value('VDDA_HALF')
                            self.log.info('VDDA from ADC: {t}'.format(t=adc))
                            adc = self.chip.get_ADC_value('VDDD_HALF')
                            self.log.info('VDDD from ADC: {t}'.format(t=adc))
                            self.chip._get_diode_temperature_sensor('TEMPSENS_A', 1)
                            self.chip._get_diode_temperature_sensor('TEMPSENS_C', 1)
                            self.chip._get_diode_temperature_sensor('TEMPSENS_D', 1)
                            self.log.info("Data file saved in #{t}".format(t=data_file_name))
                            self.log.info("Read register #{t}, finished, you can eventually stop the script\n".format(t=temp))

                    except KeyboardInterrupt:
                        break

            self.log.info("Initializing communication")
            try:
                self.chip.init_communication(write_chip_reset=False)
            except RuntimeError:
                self.log.error("Communication initialization failed !!")

        # Option B : communication reset during beam (every 5 sec as default)
        elif WhichInit == 2:
            self.log.info('Press ENTER when beam is ON')
            input()
            self.log.info("Communication initialisation during beam (CTRL C to stop)")
            temp = 0
            while True:
                try:
                    time.sleep(5)
                    temp += 1
                    try:
                        self.chip.init_communication(write_chip_reset=False)
                    except RuntimeError:
                        self.log.error("Communication initialisation failed !!")
                    if ReadDuringBeam == 1:
                        # Reading-back registers while beam is ON
                        data_file = open(self.output_filename + '_r' + str(temp) + '.txt', 'a+')
                        data_file_name = self.output_filename + '_r' + str(temp) + '.txt'
                        self.log.info("Reading register #{t}".format(t=temp))
                        ireg = 0
                        for reg in self.chip.registers.values():
                            if reg['mode'] != 1 or reg['reset'] == 0 or reg['name'] in ignore or reg['address'] in ignore:
                                continue
                            reg_during_beam.append(reg.read())
                            data_to_save = []
                            ireg_to_check = len(reg_during_beam) - 1
                            if reg_during_beam[ireg_to_check] != reg_w[ireg]:
                                SEU_spotted = 1
                                self.log.info("SEU spotted for register #{t}".format(t=reg['name']))
                            else:
                                SEU_spotted = 0
                            data_to_save.append(reg['name'] + "\t" + str(reg_w[ireg]) + "\t" + str(reg_r_after_w[ireg]) + "\t" + str(reg_during_beam[ireg]) + "\t" + str(SEU_spotted))
                            np.savetxt(data_file, data_to_save, fmt="%s")
                            ireg += 1

                        adc = self.chip.get_ADC_value('VDDA_HALF', measure='v')
                        self.log.info('VDDA from ADC: {t}'.format(t=adc))
                        adc = self.chip.get_ADC_value('VDDD_HALF', measure='v')
                        self.log.info('VDDD from ADC: {t}'.format(t=adc))
                        adc = self.chip.get_ADC_value('TEMPSENS_A', measure='v')
                        self.chip._get_diode_temperature_sensor('TEMPSENS_A', 1)
                        self.chip._get_diode_temperature_sensor('TEMPSENS_C', 1)
                        self.chip._get_diode_temperature_sensor('TEMPSENS_D', 1)
                        self.log.info("Data file saved in #{t}".format(t=data_file_name))
                        self.log.info("Read register #{t}, finished, you can eventually stop the script\n".format(t=temp))

                except KeyboardInterrupt:
                    break

        # Option C : no communication reset
        elif WhichInit == 0:
            self.log.info("No intialisation of the communication")
            if ReadDuringBeam == 0:  # Case of no register read while beam is ON
                self.log.info('Press ENTER when beam is stopped')  # Allow the operator to press ENTER after a spill/trigger/beam off
            else:  # Case of register read while beam is ON
                self.log.info('Press ENTER when beam is ON')  # Allow the operator to press ENTER when beam is turned ON

            input()

            if ReadDuringBeam == 1:  # Read reg while beam is ON
                self.log.info("Global register read during beam (CTRL C to stop)")
                temp = 0
                while True:
                    try:
                        time.sleep(5)  # Wait 1mn (default) between each reading
                        data_file = open(self.output_filename + '_r' + str(temp + 1) + '.txt', 'a+')
                        data_file_name = self.output_filename + '_r' + str(temp + 1) + '.txt'
                        temp += 1
                        self.log.info("Reading register #{t}".format(t=temp))
                        ireg = 0
                        for reg in self.chip.registers.values():
                            if reg['mode'] != 1 or reg['reset'] == 0 or reg['name'] in ignore or reg['address'] in ignore:
                                continue
                            reg_during_beam.append(reg.read())
                            data_to_save = []
                            ireg_to_check = len(reg_during_beam) - 1
                            if reg_during_beam[ireg_to_check] != reg_w[ireg]:
                                SEU_spotted = 1
                                self.log.info("SEU spotted for register #{t}".format(t=reg['name']))
                            else:
                                SEU_spotted = 0
                            data_to_save.append(reg['name'] + "\t" + str(reg_w[ireg]) + "\t" + str(reg_r_after_w[ireg]) + "\t" + str(reg_during_beam[ireg]) + "\t" + str(SEU_spotted))
                            np.savetxt(data_file, data_to_save, fmt="%s")
                            ireg += 1
                        adc = self.chip.get_ADC_value('VDDA_HALF', measure='v')
                        self.log.info('VDDA from ADC: {t}'.format(t=adc))
                        adc = self.chip.get_ADC_value('VDDD_HALF', measure='v')
                        self.log.info('VDDD from ADC: {t}'.format(t=adc))
                        self.chip._get_diode_temperature_sensor('TEMPSENS_A', 1)
                        self.chip._get_diode_temperature_sensor('TEMPSENS_C', 1)
                        self.chip._get_diode_temperature_sensor('TEMPSENS_D', 1)
                        self.log.info("Data file saved in #{t}".format(t=data_file_name))
                        self.log.info("Read register #{t}, finished, you can eventually stop the script\n".format(t=temp))

                    except KeyboardInterrupt:
                        break

###################################
        self.log.info('Reading back register values twice')
        i_reg = 0

        for reg in self.chip.registers.values():
            if reg['mode'] != 1 or reg['reset'] == 0 or reg['name'] in ignore or reg['address'] in ignore:
                continue

            # Filling the h5 file "values" table with registers name, written and read value
            row = value_table.row
            time.sleep(0.01)
            row['register'] = reg_name[i_reg]
            row['write'] = reg_w[i_reg]  # Reg value writen
            row['readAfterW'] = reg_r_after_w[i_reg]  # Reg value read just after writing

            row['read'] = reg.read()  # Reg value after beam
            row['read2'] = reg.read()  # Reg value after beam 2nd time
            row.append()

            i_reg += 1

        if ReadDuringBeam == 1:  # Read reg while beam is ON
            nb_of_read_in_beam = int(len(reg_during_beam) / nb_of_reg)
            for i in range(nb_of_read_in_beam):
                row_name = "readInBeam" + str(i)
                print("Saving: ", row_name)
                temp_data = reg_during_beam[int(i * nb_of_reg):int((i + 1) * nb_of_reg)]
                self.h5_file.create_array(readDurBeam, row_name, obj=temp_data)

        self.chip.reset()
        self.log.info('Scan finished for %i registers' % (i_reg))

    def _analyze(self):
        result = True
        self.log.info('Comparing data...')
        with tb.open_file(self.output_filename + '.h5', 'r') as in_file:
            data = in_file.root.values[:]

        for res in data:

            # Introducing random error to simulate SEU ################
            if reponse == 2:
                err_bool = random.randrange(4)
                if err_bool == 3:  # if random number = 3 then we simulate an error by adding or substracting 1 to the read value
                    self.log.info("Adding random error")
                    if res[2] > 0:
                        res[2] -= 1
                    else:
                        res[2] += 1
            ###############################

            # Find on the values of the registers the SEU ###############

            if res[3] != res[1]:
                res2bin = "{0:b}".format(res[3])    # convert in to binary
                res1bin = "{0:b}".format(res[1])
                res1bin = res1bin.zfill(len(res2bin))   # 0-padding if needed
                res2bin = res2bin.zfill(len(res1bin))

                tab1 = []
                tab2 = []

                for val1 in res1bin:
                    tab1.append(val1)      # save in a table 1, the read values
                for val2 in res2bin:
                    tab2.append(val2)      # save in a table 2, the write values

                length = 0
                error_bit = len(tab2) - 1    # initialize the position of the SEU

                self.log.error('Register %s read back a wrong value: %i instead of %i' % (res[0], res[1], res[3]))

                while length < len(tab2):
                    if tab1[length] != tab2[length]:
                        self.log.error('Error on bit n°%i' % (error_bit))
                    error_bit -= 1    # count the position of the SEU
                    length += 1

            ###############################################
                result = False
            else:
                self.log.success('No read error for reg: %s' % (res[0]))

        self.log.success('Successfully tested %i registers.' % (len(data)))
        return result


if __name__ == '__main__':

    print('')
    print('Run decrease current routine ? (1 = yes, others = no)')
    answer = int(input('Yes (1) or No (0): '))
    if answer == 1:
        os.system('python scan_seu_ITkPixV1_decrease_current.py')

    with RegisterTest(scan_config=scan_parameters) as test:

        print('')
        print('Which option for the initialisation of communication ?')
        WhichInit = -99
        while WhichInit != 0 and WhichInit != 1 and WhichInit != 2:
            WhichInit = int(input('No initialisation (0), initialisation after beam (1) or initialisation during beam (2) : '))

        print('')
        print('Would you like to read the global registers while beam is ON ?')
        ReadDuringBeam = -99
        while ReadDuringBeam != 0 and ReadDuringBeam != 1:
            ReadDuringBeam = int(input('No (0) or Yes (1): '))

        while True:

            print('')
            print('                MENU')
            print('Test register without FAKE SEU ---------------- 1')
            print('Test register with FAKE SEU ------------------- 2')
            print('Leave the test ----------------- Other value')

            reponse = int(input('Your choice : '))

            if reponse == 1 or reponse == 2:
                test.start()
                os.system('mv output_data/module_0/chip_0/chip_0.log output_data/module_0/chip_0/' + test.run_name + '.log')
            else:
                break
