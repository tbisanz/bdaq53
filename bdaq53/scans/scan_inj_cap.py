#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This test measures the SLDO performance. The input current is increased in steps and for
    each step, the resulting voltages VinA, VinD, VDDA, VDDD, Vofs, Vref_pre and the currents
    Iref, IinA, IshuntA, IinD, IshuntD are measured.
'''

import time

import numpy as np
import tables as tb
from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis_utils as au
from bdaq53.module_QC.QC_test import QC_Test
from bdaq53.analysis.convert_to_json import JSONConverter

scan_configuration = {
    'periphery_device': 'Multimeter',
    'reps': 10,
    'module_QC': {
        'design_value': 7.87e-15,
        'pass_tolerance': 1.13e-15,
        'measure': 'capacitance'
    }
}

analysis_parameters = {
    'frequency': 10 * 10 ** 6,
    'delta_C': 0.48 * 10 ** -15,
    'R_Imux': 10000,
    'm_Imux': 1
}


class DacTable(tb.IsDescription):
    scan_param_id = tb.UInt32Col(pos=0)
    I_cap = tb.Float32Col(pos=1)
    I_cap_gnd = tb.Float32Col(pos=2)
    VDDA_cap = tb.Float32Col(pos=3)
    VDDA_cap_gnd = tb.Float32Col(pos=4)
    I_para = tb.Float32Col(pos=5)
    I_para_gnd = tb.Float32Col(pos=6)
    VDDA_para = tb.Float32Col(pos=7)
    VDDA_para_gnd = tb.Float32Col(pos=8)


class InjCapTable(tb.IsDescription):
    attribute = tb.StringCol(64, pos=0)
    value = tb.Float64Col(pos=1)


class Inj_Cap_Meas(ScanBase):
    scan_id = 'meas_inj_cap'

    def _configure(self, **_):
        if not self.periphery.enabled:
            raise IOError('Periphery has to be enabled and a multimeter configured correctly!')

    def _scan(self, reps=10, periphery_device='Multimeter', **_):
        self.chip.send_global_pulse('cal_reset_pulse', pulse_width=0x3)

        rows_cap = []
        rows_para = []
        for scan_param_id in tqdm(range(reps)):
            self.chip.registers['MEAS_CAP'].write(0b010)
            self.chip.send_global_pulse('cal_reset_pulse', pulse_width=0x3)
            time.sleep(0.1)

            I_meas = self._set_mux_get_voltage(mux_select='CAP', measure='current', periphery_device=periphery_device)
            I_gnd = self._set_mux_get_voltage(mux_select='I_MUX', measure='voltage', periphery_device=periphery_device)

            V_meas = self._set_mux_get_voltage(mux_select='VDDA_HALF_CAP', measure='voltage', periphery_device=periphery_device)
            V_gnd = self._set_mux_get_voltage(mux_select='GNDA19', measure='voltage', periphery_device=periphery_device)

            rows_cap.append((scan_param_id, I_meas, I_gnd, V_meas, V_gnd))

        self.chip.send_global_pulse('cal_reset_pulse', pulse_width=0x3)

        for scan_param_id in tqdm(range(reps)):
            self.chip.registers['MEAS_CAP'].write(0b100)
            self.chip.send_global_pulse('cal_reset_pulse', pulse_width=0x3)
            time.sleep(0.1)

            I_meas = self._set_mux_get_voltage(mux_select='CAP_PARASIT', measure='current',
                                               periphery_device=periphery_device)
            I_gnd = self._set_mux_get_voltage(mux_select='I_MUX', measure='voltage',
                                              periphery_device=periphery_device)

            V_meas = self._set_mux_get_voltage(mux_select='VDDA_HALF_CAP', measure='voltage',
                                               periphery_device=periphery_device)
            V_gnd = self._set_mux_get_voltage(mux_select='GNDA19', measure='voltage',
                                              periphery_device=periphery_device)

            rows_para.append((scan_param_id, I_meas, I_gnd, V_meas, V_gnd))

        self.data.dac_data_table = self.h5_file.create_table(self.h5_file.root, name='cap_meas_data',
                                                             title='cap_meas_data', description=DacTable)
        row = self.data.dac_data_table.row
        for row_cap, row_para in zip(rows_cap, rows_para):
            row['I_cap'] = row_cap[1]
            row['I_cap_gnd'] = row_cap[2]
            row['VDDA_cap'] = row_cap[3]
            row['VDDA_cap_gnd'] = row_cap[4]
            row['I_para'] = row_para[1]
            row['I_para_gnd'] = row_para[2]
            row['VDDA_para'] = row_para[3]
            row['VDDA_para_gnd'] = row_para[4]
            row['scan_param_id'] = row_cap[0]
            row.append()
            self.data.dac_data_table.flush()
        self.log.success('Scan finished')

    def _set_mux_get_voltage(self, mux_select='VINA_HALF', measure='voltage', periphery_device='Multimeter'):
        self.chip.set_mux(mux_select, measure)
        time.sleep(0.1)
        if measure in ('voltage', 'v'):
            return self.periphery.aux_devices[periphery_device].get_voltage()
        else:
            return self.periphery.aux_devices[periphery_device].get_voltage()

    def _analyze(self, raw_data_file=None, analyzed_data_file=None):
        if not raw_data_file:
            raw_data_file = self.output_filename + '.h5'
        if not analyzed_data_file:
            analyzed_data_file = self.output_filename + '_interpreted.h5'

        with tb.open_file(raw_data_file, 'r') as in_file:
            calc_values, calc_errors = self._calculate_capacitance(in_file)
            scan_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])

            with tb.open_file(analyzed_data_file, 'w', title=in_file.title) as out_file:
                out_file.create_group(out_file.root, name='configuration_in', title='Configuration after scan step')
                out_file.copy_children(in_file.root.configuration_out, out_file.root.configuration_in, recursive=True)

                output = out_file.create_table(out_file.root, name='Inj_Cap', title='Inj_Cap', description=InjCapTable)
                row = output.row
                for attribute, value in zip(['I_cap', 'VDDA_cap', 'I_para', 'VDDA_para', 'C_cap', 'C_para', 'C_pix'], calc_values):
                    row['attribute'] = attribute
                    row['value'] = value
                    row.append()
                    output.flush()

        self.log.info('InjCap changed from %E to %E' % (self.chip.calibration.C_inj, calc_values[6]))
        self.chip.calibration.C_inj = calc_values[6]
        self.chip.calibration.C_inj_error = calc_errors[6]

        if 'module_QC' in scan_config:
            with QC_Test(output_file=self.output_filename) as QC:
                self.chip.QC_passed = QC.QC_test('C_inj', scan_config['module_QC'], self.chip.calibration.C_inj)

        conv = JSONConverter(data_file=raw_data_file, outpath=self.working_dir)
        conv.convert_h5_to_json()

        return calc_values[6]

    def _calculate_capacitance(self, file):
        data_set = file.root.cap_meas_data

        I_cap = data_set.cols.I_cap[:]
        I_cap_gnd = data_set.cols.I_cap_gnd[:]
        VDDA_cap = data_set.cols.VDDA_cap[:]
        VDDA_cap_gnd = data_set.cols.VDDA_cap_gnd[:]

        I_para = data_set.cols.I_para[:]
        I_para_gnd = data_set.cols.I_para_gnd[:]
        VDDA_para = data_set.cols.VDDA_para[:]
        VDDA_para_gnd = data_set.cols.VDDA_para_gnd[:]

        m = analysis_parameters['m_Imux'] / analysis_parameters['R_Imux']

        Value_I_cap = np.mean(I_cap - I_cap_gnd) * m
        Error_I_cap = np.std(I_cap - I_cap_gnd) * m
        Value_VDDA_cap = np.mean(VDDA_cap - VDDA_cap_gnd) * 2
        Error_VDDA_cap = np.std(VDDA_cap - VDDA_cap_gnd) * 2
        Value_I_para = np.mean(I_para - I_para_gnd) * m
        Error_I_para = np.std(I_para - I_para_gnd) * m
        Value_VDDA_para = np.mean(VDDA_para - VDDA_para_gnd) * 2
        Error_VDDA_para = np.std(VDDA_para - VDDA_para_gnd) * 2

        f = analysis_parameters['frequency']
        C_cap = Value_I_cap / (Value_VDDA_cap * f)
        Error_C_cap = (Error_I_cap / (Value_VDDA_cap * f)) ** 2 + \
                      (Value_I_cap * Error_VDDA_cap / (Value_VDDA_cap ** 2 * f)) ** 2
        Error_C_cap = Error_C_cap ** 0.5

        C_para = Value_I_para / (Value_VDDA_para * f)
        Error_C_para = (Error_I_para / (Value_VDDA_para * f)) ** 2 + \
                       (Value_I_para * Error_VDDA_para / (Value_VDDA_para ** 2 * f)) ** 2
        Error_C_para = Error_C_para ** 0.5

        C_pix = (C_cap - C_para) / 100 - analysis_parameters['delta_C']
        Error_C_pix = ((Error_C_cap / 100) ** 2 + (Error_C_para / 100) ** 2) ** 0.5

        return [Value_I_cap, Value_VDDA_cap, Value_I_para, Value_VDDA_para, C_cap, C_para, C_pix], [Error_I_cap, Error_VDDA_cap, Error_I_para, Error_VDDA_para, Error_C_cap, Error_C_para, Error_C_pix]


if __name__ == "__main__":
    with Inj_Cap_Meas(scan_config=scan_configuration) as scan:
        scan.start()
        for chip in scan.chips.values():
            print('%s: %s' % (chip.name, chip.chip.QC_passed))
