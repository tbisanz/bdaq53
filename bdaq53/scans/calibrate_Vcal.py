#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Calibration of Vcal by performing DAC linearity scans on VCAL_MED and VCAL_HIGH in both high and low voltage range.
'''

import time

import tables as tb
from tqdm import tqdm
import numpy as np
from scipy.optimize import curve_fit

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import plotting
from bdaq53.analysis import analysis_utils as au
from bdaq53.module_QC.QC_test import QC_Test
from bdaq53.analysis.convert_to_json import JSONConverter


e = 1.6021766208e-19  # Electron charge


scan_configuration = {
    'periphery_device': 'Multimeter',
    'value_start': 100,
    'value_stop': 4000,
    'value_step': 100,
    'type': 'U',  # needed since configure_in is written before configuring the chip
    'module_QC': {
        'slope_lr': 0.2,
        'slope_lr_tolerance': 0.04,
        'offset_lr': -3,
        'offset_lr_tolerance': 20,
        'slope_sr_factor': 0.5,
        'slope_sr_factor_tolerance': 0.01
    }
}


class DacTable(tb.IsDescription):
    scan_param_id = tb.UInt32Col(pos=0)
    dac = tb.UInt32Col(pos=1)
    adc = tb.UInt32Col(pos=2)
    voltage = tb.Float32Col(pos=3)
    voltage_gnd = tb.Float32Col(pos=4)


class VCalFits(tb.IsDescription):
    dac_name = tb.StringCol(64, pos=0)
    slope = tb.Float64Col(pos=1)
    slope_error = tb.Float64Col(pos=2)
    offset = tb.Float64Col(pos=3)
    offset_error = tb.Float64Col(pos=4)
    chi2 = tb.Float64Col(pos=5)


def linear(x, a, b):
    return (a * x) + b


class VcalCalibration(ScanBase):
    scan_id = 'vcal_calibration'

    def _configure(self, **_):
        if not self.periphery.enabled:
            raise IOError('Periphery has to be enabled and a multimeter configured correctly!')

        self.configuration['scan']['type'] = 'U'
        self.range_register_value = self.chip.registers['MEAS_CAP'].get()  # save register value to reset chip after scan
        # FIX ME: this step is too late to save type in configure_in.
        # Leads to analysis failing if it is not specified in scan_configuration

    def _scan(self, value_start=0, value_stop=4096, value_step=100, periphery_device='Multimeter', **_):
        '''
        Vcal-calibration main loop

        Parameters
        ----------
        value_start : int
            First value for this DAC
        value_stop : int
            Last value of this DAC, exclusive
        value_step : int
            Steps between DAC values
        '''

        value_range = list(range(value_start, value_stop + 1, value_step))
        value_range.append(500)

        self._perform_individual_scan(value_range, DAC='VCAL_MED', VCal_Range='Small',
                                      periphery_device=periphery_device, tqdm_step=0)
        self._perform_individual_scan(value_range, DAC='VCAL_HIGH', VCal_Range='Small',
                                      periphery_device=periphery_device, tqdm_step=1)
        self._perform_individual_scan(value_range, DAC='VCAL_MED', VCal_Range='Large',
                                      periphery_device=periphery_device, tqdm_step=2)
        self._perform_individual_scan(value_range, DAC='VCAL_HIGH', VCal_Range='Large',
                                      periphery_device=periphery_device, tqdm_step=3)

        self.log.success('Scan finished')
        self.chip.registers['MEAS_CAP'].write(self.range_register_value)  # reset register to old value

    def _perform_individual_scan(self, value_range, DAC='VCAL_HIGH', VCal_Range='Large', periphery_device='Multimeter', tqdm_step=0):
        self.chip.set_mux(DAC, 'voltage')
        VCal_Range = VCal_Range.lower()
        self._set_InjVcal_Range(VCal_Range)
        time.sleep(0.1)

        rows = []
        for scan_param_id, value in tqdm(enumerate(value_range), initial=tqdm_step * len(value_range), total=len(value_range) * 4, unit=' DAC Steps'):
            with self.readout(scan_param_id=scan_param_id):
                self.chip.registers[DAC].write(value)
                time.sleep(0.005)  # Sleep to ensure system has settled to the new voltage.
                adc_value = self.chip.get_ADC_value(DAC, measure='v')[0]

                voltage, voltage_gnd = self._get_voltage_with_gnd(DAC=DAC, periphery_device=periphery_device)

                rows.append((value, adc_value, voltage, voltage_gnd, scan_param_id))

        self.data.dac_data_table = self.h5_file.create_table(self.h5_file.root, name=DAC + '_' + VCal_Range + 'Range_data',
                                                             title=DAC + '_' + VCal_Range + 'Range', description=DacTable)
        row = self.data.dac_data_table.row
        for value, adc_value, voltage, voltage_gnd, scan_param_id in rows:
            row['dac'] = value
            row['adc'] = adc_value
            row['voltage'] = voltage
            row['voltage_gnd'] = voltage_gnd
            row['scan_param_id'] = scan_param_id
            row.append()
            self.data.dac_data_table.flush()

    def _get_voltage_with_gnd(self, DAC='VCAL_HIGH', measure='voltage', periphery_device='Multimeter'):
        self.chip.set_mux(DAC, measure)
        time.sleep(0.005)
        _voltage = self.periphery.aux_devices[periphery_device].get_voltage()
        self.chip.set_mux('GNDA30', measure)
        time.sleep(0.005)
        _voltage_gnd = self.periphery.aux_devices[periphery_device].get_voltage()

        return _voltage, _voltage_gnd

    def _set_InjVcal_Range(self, Vcal_range):
        if Vcal_range == 'large':
            _reg_val = int('0' + '0' + '1', 2)
        elif Vcal_range == 'small':
            _reg_val = int('0' + '0' + '0', 2)
        else:
            raise KeyError(f"{Vcal_range} is not an accepted InjVcalRange setting.")

        self.chip.registers['MEAS_CAP'].write(_reg_val)

    def _analyze(self, raw_data_file=None, analyzed_data_file=None, range_used_for_cal='small'):
        '''
        performs analysis step and calibrates e-conversion

        Parameters:
        -----------
        raw_data_file: str
            filename of file with data that is to be analysed if used outside a scan
        analysed_data_file: str
            filename of output file - if none, filename_interpreted.h5 is used
        range_used_for_cal: str
            range to be used for calibration of e-conversion. Accepts "small" and "large".
            Depends on chip settings that are to be used for later scans. Default in BDAQ is small.
            If using analysis in scan routine, the default value has to be changed!
        '''
        self.range_used_for_cal = range_used_for_cal + 'Range'

        if not raw_data_file:
            raw_data_file = self.output_filename + '.h5'
        if not analyzed_data_file:
            analyzed_data_file = self.output_filename + '_interpreted.h5'

        with tb.open_file(raw_data_file, 'r') as in_file:
            fit_values, VCal_MED_default, VCal_MED_default_setting = self._fit_to_dac_data(in_file)
            scan_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])

            if 'module_QC' in scan_config:
                self.QC_parameters = scan_config['module_QC']
                self._QC_test(fit_values=fit_values)

            with tb.open_file(analyzed_data_file, 'w', title=in_file.title) as out_file:
                out_file.create_group(out_file.root, name='configuration_in', title='Configuration after scan step')
                out_file.copy_children(in_file.root.configuration_out, out_file.root.configuration_in, recursive=True)

                output = out_file.create_table(out_file.root, name='VCal_Fits', title='VCal_Fits', description=VCalFits)
                row = output.row
                for dac in fit_values.keys():
                    row['dac_name'] = dac
                    row['slope'] = fit_values[dac]['slope']
                    row['slope_error'] = fit_values[dac]['slope_error']
                    row['offset'] = fit_values[dac]['offset']
                    row['offset_error'] = fit_values[dac]['offset_error']
                    row.append()
                    output.flush()

        e_conversion = self._calculate_e_conversion(
            VCal_High_slope=fit_values['VCAL_HIGH_' + self.range_used_for_cal]['slope'],
            VCal_High_slope_err=fit_values['VCAL_HIGH_' + self.range_used_for_cal]['slope_error'],
            VCal_High_offset=fit_values['VCAL_HIGH_' + self.range_used_for_cal]['offset'],
            VCal_High_offset_err=fit_values['VCAL_HIGH_' + self.range_used_for_cal]['offset_error'],
            VCal_MED_voltage=VCal_MED_default, VCal_MED_setting=VCal_MED_default_setting)

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=self.output_filename + '.h5') as p:
                p.create_standard_plots()

        conv = JSONConverter(data_file=raw_data_file, outpath=self.working_dir)
        conv.convert_h5_to_json()

        return e_conversion

    def _fit_to_dac_data(self, in_file):
        root = in_file.root
        output = {}

        for dataset in [root.VCAL_MED_smallRange_data, root.VCAL_MED_largeRange_data, root.VCAL_HIGH_smallRange_data, root.VCAL_HIGH_largeRange_data]:
            data_name = dataset.title

            dac = dataset.cols.dac[:]
            voltage = dataset.cols.voltage[:]
            voltage_gnd = dataset.cols.voltage_gnd[:]

            voltage_eff = voltage - voltage_gnd

            popt, pcov = curve_fit(linear, dac, voltage_eff)
            perr = np.sqrt(np.diag(pcov))

            output[data_name] = {'slope': popt[0], 'slope_error': perr[0], 'offset': popt[1], 'offset_error': perr[1]}

            if data_name == 'VCAL_MED_' + self.range_used_for_cal:
                VCal_MED_default = voltage_eff[-1]
                VCal_MED_default_setting = dac[-1]

        return output, VCal_MED_default, VCal_MED_default_setting

    def _calculate_e_conversion(self, VCal_High_slope, VCal_High_slope_err,
                                VCal_High_offset, VCal_High_offset_err, VCal_MED_voltage, VCal_MED_setting):
        C_inj = float(self.chip.calibration.C_inj)
        C_inj_err = float(self.chip.calibration.C_inj_error)

        self.chip.calibration.e_conversion['slope'] = VCal_High_slope * C_inj / e
        self.chip.calibration.e_conversion['slope_error'] = ((VCal_High_slope_err * C_inj / e)**2 + (VCal_High_slope * C_inj_err / e)**2)**0.5

        self.chip.calibration.e_conversion['offset'] = (VCal_High_offset - VCal_MED_voltage + (VCal_High_slope * VCal_MED_setting)) * C_inj / e
        self.chip.calibration.e_conversion['offset_error'] = ((VCal_High_offset_err * C_inj / e)**2 + ((VCal_High_offset - VCal_MED_voltage) * C_inj_err / e)**2)**0.5
        self.log.info('e conversion changed to %f +- %f * dVcal + %f +- %f' % (
            self.chip.calibration.e_conversion['slope'], self.chip.calibration.e_conversion['slope_error'],
            self.chip.calibration.e_conversion['offset'], self.chip.calibration.e_conversion['offset_error']))

        return (self.chip.calibration.e_conversion['slope'], self.chip.calibration.e_conversion['offset'])

    def _QC_test(self, fit_values):
        self.chip.QC_passed = True
        with QC_Test(output_file=self.output_filename) as QC:
            for name in ['VCAL_MED', 'VCAL_HIGH']:
                slope_lr = fit_values[name + '_largeRange']['slope'] * 1000
                offset_lr = fit_values[name + '_largeRange']['offset'] * 1000
                slope_sr = fit_values[name + '_smallRange']['slope'] * 1000
                slope_factor = slope_sr / slope_lr

                passed = QC.QC_test(name + '_SLOPE', {'design_value': float(self.QC_parameters['slope_lr']), 'pass_tolerance': float(self.QC_parameters['slope_lr_tolerance']), 'measure': 'slope'}, slope_lr)
                self.chip.QC_passed = self.chip.QC_passed and passed
                passed = QC.QC_test(name + '_OFFSET', {'design_value': float(self.QC_parameters['offset_lr']), 'pass_tolerance': float(self.QC_parameters['offset_lr_tolerance']), 'measure': 'offset'}, offset_lr)
                self.chip.QC_passed = self.chip.QC_passed and passed
                passed = QC.QC_test(name + '_SLOPE_RATIO', {'design_value': float(self.QC_parameters['slope_sr_factor']), 'pass_tolerance': float(self.QC_parameters['slope_sr_factor_tolerance']), 'measure': 'ratio'}, slope_factor)
                self.chip.QC_passed = self.chip.QC_passed and passed


if __name__ == "__main__":
    with VcalCalibration(scan_config=scan_configuration) as scan:
        scan.start()
