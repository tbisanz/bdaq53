#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Injection tuning:
    Iteratively inject target charge and evaluate if more or less than 50% of expected hits are seen in any pixel
'''

from tqdm import tqdm
import numpy as np

from bdaq53.system.scan_base import ScanBase
from bdaq53.chips.shift_and_inject import shift_and_inject, get_scan_loop_mask_steps
from bdaq53.analysis import online as oa

scan_configuration = {
    'start_column': 128,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 384,

    'n_injections': 100,

    # Target threshold
    'VCAL_MED': 500,
    'VCAL_HIGH': 940
}


class TDACTuning(ScanBase):
    scan_id = 'local_threshold_tuning'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, VCAL_MED=1000, VCAL_HIGH=4000, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            VCAL_HIGH DAC value.
        '''

        self.data.flavors = []
        if self.chip.chip_type.lower() == 'rd53a':
            # Ignore SYNC flavor
            start_column = np.clip(start_column, 128, None)

            if start_column < 264:
                self.data.flavors.append('LIN')
            if stop_column > 264:
                self.data.flavors.append('DIFF')
            self.tdac_sign = np.zeros(shape=self.chip.masks['enable'].shape, dtype=int)
            self.tdac_sign[128:264, :] = -1  # LIN: Higher TDAC setting --> Lower threshold
            self.tdac_sign[264:, :] = 1  # DIFF: Higher TDAC setting --> Higher threshold
            self.n_loops = 2  # Injection loop for TDAC + stuck pixel loop
        else:
            self.tdac_sign = np.ones_like(self.chip.masks['enable'])  # Higher TDAC setting --> Higher threshold
            self.data.flavors.append('ITkPixV1')
            self.n_loops = 1  # Only injection loop for TDAC

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)

        # Assume hmap is not compressed unless chip has respective attribute; if so read that back
        hmap_is_compressed = False
        if hasattr(self.chip, 'compress_hmap'):
            hmap_is_compressed = self.chip.compress_hmap

        self.data.hist_occ = oa.OccupancyHistogramming(chip_type=self.chip.chip_type.lower(), hmap_is_compressed=hmap_is_compressed, rx_id=int(self.chip.receiver[-1]))

    def _scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, n_injections=100, **_):
        '''
        Global threshold tuning main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.
        '''

        if self.chip.chip_type.lower() == 'rd53a':
            # Ignore SYNC flavor
            start_column = np.clip(start_column, 128, None)

        target_occ = n_injections / 2

        self.data.tdac_map = np.zeros_like(self.chip.masks['tdac'])
        best_results_map = np.zeros((self.chip.masks['tdac'].shape[0], self.chip.masks['tdac'].shape[1], 2), dtype=float)
        retune = False  # Default is to start with default TDAC mask

        # Check if re-tune: In case one TDAC is not the default one.
        tdacs = self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row]
        if np.any(np.logical_and(tdacs != 0, tdacs != 7)):
            self.data.tdac_map[:] = self.chip.masks['tdac'][:]
            retune = True
            steps = [1, 1, 1, 1]
            self.log.info('Use existing TDAC mask (TDAC steps = {0})'.format(steps))

        # Define stepsizes and startvalues for TDAC in case of new tuning
        # Binary search will not converge if all TDACs are centered, so set
        #   half to 7 and half to 8 for LIN
        #   leave half at 0 and divide the other half between +1 and -1 for DIFF/ITkPixV1
        if not retune:
            if 'LIN' in self.data.flavors:
                steps = [4, 2, 1, 1]
                self.data.tdac_map[max(128, start_column):min(264, stop_column), start_row:stop_row] = 7
                self.data.tdac_map[max(128, start_column):min(264, stop_column), start_row:stop_row:2] = 8
            if 'DIFF' in self.data.flavors:
                steps = [8, 4, 2, 1, 1, 1]
                self.data.tdac_map[max(264, start_column):min(400, stop_column), start_row:stop_row:2] = 1
                self.data.tdac_map[max(264, start_column):min(400, stop_column), start_row:stop_row:4] = -1
            if 'ITkPixV1' in self.data.flavors:
                steps = [8, 4, 2, 1, 1, 1]
                self.data.tdac_map[start_column:stop_column, start_row:stop_row:2] = 1
                self.data.tdac_map[start_column:stop_column, start_row:stop_row:4] = -1
            self.log.info('Use default TDAC mask (TDAC steps = {0})'.format(steps))

        self.log.info('Searching optimal local threshold settings for {0}'.format(', '.join(self.data.flavors)))
        pbar = tqdm(total=get_scan_loop_mask_steps(scan=self) * len(steps * self.n_loops), unit=' Mask steps')
        for scan_param, step in enumerate(steps):
            # Revive chip
            if self.chip.chip_type.lower() == 'rd53a':  # FIXME: makes ITkPixV1 stuck
                self.chip.revive()
            # Set new TDACs
            self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = self.data.tdac_map[start_column:stop_column, start_row:stop_row]
            self.chip.masks.update()
            # Inject target charge
            with self.readout(scan_param_id=scan_param, callback=self.analyze_data_online):
                shift_and_inject(scan=self, n_injections=n_injections, pbar=pbar, scan_param_id=scan_param)
            # Get hit occupancy using online analysis
            occupancy = self.data.hist_occ.get()
            # Revive chip
            if self.chip.chip_type.lower() == 'rd53a':  # FIXME: makes ITkPixV1 stuck
                self.chip.revive()

            # Scan stuck pixels. TODO: check if this makes sense for ITkPixV1
            if self.chip.chip_type.lower() == 'rd53a':
                with self.readout(scan_param_id=scan_param, callback=self.analyze_data_online_no_save):
                    for fe, _ in self.chip.masks.shift(masks=['enable']):
                        if not fe == 'skipped':
                            self.chip.toggle_output_select(repetitions=10, send_ecr=False)
                        pbar.update(1)
                stuck = self.data.hist_occ.get()
            else:
                stuck = np.zeros_like(self.chip.masks['tdac'])

            # Calculate best (closest to target) TDAC setting and update TDAC setting according to hit occupancy
            diff = np.abs(occupancy - target_occ)  # Actual (absolute) difference to target occupancy
            update_sel = np.logical_or(diff <= best_results_map[:, :, 1], best_results_map[:, :, 1] == 0)  # Closer to target than before
            best_results_map[update_sel, 0] = self.data.tdac_map[update_sel]  # Update best TDAC
            best_results_map[update_sel, 1] = diff[update_sel]  # Update smallest (absolute) difference to target occupancy (n_injections / 2)
            larger_occ_sel = (occupancy > target_occ + round(target_occ * 0.02))  # Hit occupancy larger than target
            self.data.tdac_map[larger_occ_sel] += (step * self.tdac_sign[larger_occ_sel])  # Increase threshold
            smaller_occ_and_stuck_sel = np.logical_and((occupancy < target_occ - round(target_occ * 0.02)), stuck)  # Hit occupancy smaller than target and stuck
            smaller_occ_and_not_stuck_sel = np.logical_and((occupancy < target_occ - round(target_occ * 0.02)), ~stuck)  # Hit occupancy smaller than target
            self.data.tdac_map[smaller_occ_and_stuck_sel] += (step * self.tdac_sign[smaller_occ_and_stuck_sel])  # Increase threshold
            self.data.tdac_map[smaller_occ_and_not_stuck_sel] -= (step * self.tdac_sign[smaller_occ_and_not_stuck_sel])  # Decrease threshold

            # Make sure no invalid TDACs are used. TODO: Why does this happen?
            if self.chip.chip_type.lower() == 'rd53a':
                self.data.tdac_map[128:264, :] = np.clip(self.data.tdac_map[128:264, :], 0, 15)
                self.data.tdac_map[264:400, :] = np.clip(self.data.tdac_map[264:400, :], -15, 15)
            if self.chip.chip_type.lower() in ['itkpixv1', 'itkpixv2']:
                self.data.tdac_map = np.clip(self.data.tdac_map, -15, 15)

        # Finally use TDAC value which yielded the closest to target occupancy
        self.data.tdac_map[:, :] = best_results_map[:, :, 0]

        pbar.close()
        self.data.hist_occ.close()  # stop analysis process
        self.log.success('Scan finished')

        if len(self.data.flavors) == 1:
            enable_mask = self.chip.masks['enable'][start_column:stop_column, start_row:stop_row]
            tdac_mask = self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row]
            mean_tdac = np.mean(tdac_mask[enable_mask])
            self.log.success('Mean TDAC is {0:1.2f}.'.format(mean_tdac))
        else:
            enable_mask_lin = self.chip.masks['enable'][max(128, start_column):min(264, stop_column), start_row:stop_row]
            tdac_mask_lin = self.chip.masks['tdac'][max(128, start_column):min(264, stop_column), start_row:stop_row]
            mean_tdac_lin = np.mean(tdac_mask_lin[enable_mask_lin])
            enable_mask_diff = self.chip.masks['enable'][max(264, start_column):min(400, stop_column), start_row:stop_row]
            tdac_mask_diff = self.chip.masks['tdac'][max(264, start_column):min(400, stop_column), start_row:stop_row]
            mean_tdac_diff = np.mean(tdac_mask_diff[enable_mask_diff])
            self.log.success('Mean TDAC is {0:1.2f} for LIN and {1:1.2f} for DIFF.'.format(mean_tdac_lin, mean_tdac_diff))
        self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = self.data.tdac_map[start_column:stop_column, start_row:stop_row]

    def analyze_data_online(self, data_tuple, receiver=None):
        raw_data = data_tuple[0]
        if self.chip.chip_type.lower() in ['itkpixv1', 'itkpixv2']:  # ITkPixv1 needs ptot table for analysis
            self.data.hist_occ.add(raw_data, self.ptot_table[:])
        else:
            self.data.hist_occ.add(raw_data)
        super(TDACTuning, self).handle_data(data_tuple, receiver)

    def analyze_data_online_no_save(self, data_tuple, receiver=None):
        raw_data = data_tuple[0]
        if self.chip.chip_type.lower() in ['itkpixv1', 'itkpixv2']:  # ITkPixv1 needs ptot table for analysis
            self.data.hist_occ.add(raw_data, self.ptot_table[:])
        else:
            self.data.hist_occ.add(raw_data)

    def _analyze(self):
        pass


if __name__ == '__main__':
    with TDACTuning(scan_config=scan_configuration) as tuning:
        tuning.start()
