general: # General configuration
  output_directory:  # Top-level output data directory, default is the current folder where the bdaq script is started
  use_run_number:  False  # if True name files after run number (eg. run_1_digital_scan), required for QC scans. If False name files after timestamp (eg. 20230829_174739_analog_scan)
  use_database: False # Check if chip is in data base, log error if not
  abort_on_rx_error: True # Abort scan when RX error occurs
  interlock: False #  If True, DCS data is written and scans abort if interlock is triggered.
  max_reg_read_failures:  # Number of register read failures each chip can have in total (empty for no limit)
periphery: # Configuration of the BDAQ53 Periphery module
  enable_periphery: False # Set to true if BDAQ Periphery is used for Module Power or to control measuring devices
  monitoring: False # Monitor all connected powersupplies and sensors regularly FIX ME: This doesn't wort for the new periphery
  monitoring_interval: 10 # Interval for DCS monitoring in seconds
  analog_monitoring_board: False

# Connected Modules
modules:
  module_0: # Arbitrary name of module, defines folder name with chip sub folders
    identifier: "unknown" # Module/wafer/PCB identifier, has to be given (e.g. SCC number)
    module_type: # Module_type defined in modules/module_types.yaml (e.g. dual, common_quad, SPQ). Leave empty for bare chip.
    powersupply:
      lv_name: LV-0
      lv_voltage: 1.7 # Default None, set value if LV shall be powered through .the interlock environment
      lv_channel:   # define the channel to which the PS for the module is connected, if None, channel 2 will be used
      lv_current_limit: 2.0
      lv_current:  # Default None, set value if LV shall be powered through .the interlock environment
    #   hv_name: HV-0
    #   hv_voltage: 5 # Default None, set value if HV shall be powered through the interlock environment
    #   hv_current_limit: 1e-5 # Default 1e-5, set different value neeeded
    power_cycle: False # power cycle all chip of this module before scan start
    chip_0: # Arbitrary name of chip, defines folder name with chip data
      chip_sn: "0x0001"
      chip_type: "rd53a"
      chip_id: 0
      receiver: "rx0" # Aurora receiver channel (ranges from 'rx0' to 'rxN', N board-dependent)
      chip_config_file: # If defined: use config from in file (either .cfg.yaml or .h5). If not defined use chip config of latest scan and std. config if no previous scan exists
      record_chip_status: True # Add chip statuses to the output files after the scan (link errors and powering infos)
      use_good_pixels_diff: False
      send_data: "tcp://127.0.0.1:5500" # Socket address of online monitor
      # disable_pixel: # disable single pixel or areas on the chip
      #   - 15, 17 # single pixel
      #   - 10:50, 0:100 # large area

hardware: # Setup-specific hardware settings
  bypass_mode: False # Configure chip and BDAQ board for bypass mode. You have to provide all clocks externally!
  enable_NTC: False # Only enable if you know you have the correct resistors mounted on the BDAQ board!
  use_QMS_card: False # Enable if QMS Card is used. This activates V_mux switching between chips.
  qms_dict:  # Dictionary that relates the slots on the QMS card with the arbitrary module names specified in testbench. This needs to be available to ensure correct module and data_mux switching.
    module_1: 'module_0'
    module_2: None
    module_3: None
    module_4: None
  LP_enable: False

TLU:
  TRIGGER_MODE: 0 # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
  TRIGGER_SELECT: 1 # Selecting trigger input: HitOr (individual, TDC loop-through) (16), RX1 (multi purpose) (8), RX0 (TDC loop-trough) (4), HitOR [DP_ML_5 and mDP] (logical OR of all eight lines) (3), HitOR [mDP only] (logical OR of all four lines) (2), HitOR [DP_ML_5 only] (logical OR of all four lines) (1), disabled (0)
  TRIGGER_INVERT: 0 # Inverting trigger input: HitOr (individual, TDC loop-through) (16), RX1 (multi purpose) (8), RX0 (TDC loop-trough) (4), HitOR [DP_ML_5 and mDP] (logical OR of all eight lines) (3), HitOR [mDP only] (logical OR of all four lines) (2), HitOR [DP_ML_5 only] (logical OR of all four lines) (1), disabled (0)
  TRIGGER_LOW_TIMEOUT: 0 # Maximum wait cycles for TLU trigger low.
  TRIGGER_VETO_SELECT: 0 # Selecting trigger veto: AZ VETO (2), RX FIFO full (1), disabled (0). Set to (2) if SYNC FE is enabled.
  TRIGGER_HANDSHAKE_ACCEPT_WAIT_CYCLES: 5 # TLU trigger minimum length in TLU clock cycles
  DATA_FORMAT: 0 # Select trigger data format: only trigger number (0), only time stamp (1), combined, 15 bit time stamp + 16 bit trigger number (2)
  EN_TLU_VETO: 0 # Assert TLU veto when external veto. Activate this in order to VETO triggers if SYNC FE is enabled.
  TRIGGER_DATA_DELAY: 31 # Depends on the cable length and should be adjusted (run scan/tune_tlu.py)

TDC:
  EN_WRITE_TIMESTAMP: 1 # Writing trigger timestamp
  EN_TRIGGER_DIST: 1 # Measuring trigger to TDC delay with 640MHz clock
  EN_NO_WRITE_TRIG_ERR: 1 # Writing TDC word only if valid trigger occurred
  EN_INVERT_TDC: 0 # Inverting TDC input
  EN_INVERT_TRIGGER: 0 # Inverting trigger input, e.g. for using Test output from EUDET TLU.

calibration: # Setup-specific calibration constants
  bdaq_ntc: # Resistors on BDAQ board for NTC readout
    R16: 200.00e3
    R17: 4.75e3
    R19: 2.50e3

notifications: # Notification settings
  enable_notifications: False
  slack_token: "~/slack_api_token"
  slack_users:
    - AAAAAA123

# Standard analysis settings
# Scans might overwrite these settings if needed.
# Detailed description of parameters in bdaq53/analysis/analysis.py
analysis:
  skip: False # Omit analysis in scans
  create_pdf: True # Create analysis summary pdf
  module_plotting: True # Create combined plots for chip in a module
  store_hits: False # store hit table
  cluster_hits: False # store cluster data
  analyze_tdc: False # analyze TDC words
  analyze_ptot: False # analyze PTOT words (only possible for RD53B)
  use_tdc_trigger_dist: False # analyze TDC to TRG distance
  upper_chi2_ndf: 50  # Scurves with chi2/ndf larger than this value count as failed fits and threshold is set to zero.
  chunk_size: 1000000 # scales amount of data in RAM (~150 MB)
  blocking: True # block main process during analysis

