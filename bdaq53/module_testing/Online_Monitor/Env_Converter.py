import numpy as np

from online_monitor.utils import utils
from online_monitor.converter.transceiver import Transceiver


class Env_Converter(Transceiver):
    def setup_transceiver(self):
        """ Called at the beginning
            We want to be able to change the histogrammmer settings
            thus bidirectional communication needed
        """
        self.set_bidirectional_communication()

    def deserialize_data(self, data):
        dat = utils.simple_dec(data)
        return dat

    def setup_interpretation(self):
        # Maximum amount of values before override
        self.n_values = 7200
        self.last_timestamp = 0

        # Init result hists
        self.reset()

    def interpret_data(self, data):
        data, meta_data = data[0][1]
        self.update_arrays(data, meta_data)
        # Calculate recent average of curves (timespan can be defined in GUI)
        interpreted_data = {
            "temp_1": self.temp_arrays,
            "temp_2": self.temp_arrays_2,
            "humidity_chuck": self.humidity_arrays_chuck,
            "humidity_2": self.humidity_arrays_2,
            "temp_NTC" : self.temp_NTC_array,
            "dew_1": self.dewpoint,
            "dew_2": self.dewpoint_2,
            "Vin": self.vin,
            "Iin": self.iin,
            "HV_V": self.hv_v,
            "HV_leak": self.hv_leak,
            "vacuum_pressure": self.vacuum_pressure,
            "measuring_time": self.measuring_time,
            "time": self.timestamps,
            "last_timestamp": self.last_timestamp,
            "module_name": self.module_name,
            "module_list": self.module_list,
            "set_values": self.set_values,
            "interlock_temp_soft": self.interlock_temp_soft,
            "interlock_temp_hard": self.interlock_temp_hard,
            "interlock_humid_soft": self.interlock_humid_soft,
            "interlock_humid_hard": self.interlock_humid_hard,
            "interlock_pressure": self.interlock_pressure
        }

        return [interpreted_data]

    def serialize_data(self, data):
        return utils.simple_enc(None, data)

    def handle_command(self, command):
        # received signal is 'ACTIVETAB tab' where tab is the name (str) of the selected tab in online monitor
        if command[0] == "RESET":
            self.reset()
        else:
            self.avg_window = int(command[0])

    def reset(self):
        self.temp_arrays = np.full(self.n_values, np.nan)
        self.humidity_arrays_chuck = np.full(self.n_values, np.nan)
        self.timestamps = np.full(self.n_values, np.nan)
        self.dewpoint = np.full(self.n_values, np.nan)
        self.temp_arrays_2 = np.full(self.n_values, np.nan)
        self.humidity_arrays_2 = np.full(self.n_values, np.nan)
        self.dewpoint_2 = np.full(self.n_values, np.nan)
        self.vin = np.full(self.n_values, np.nan)
        self.iin = np.full(self.n_values, np.nan)
        self.hv_v = np.full(self.n_values, np.nan)
        self.hv_leak = np.full(self.n_values, np.nan)
        self.temp_NTC_array = np.full(self.n_values, np.nan)
        self.vacuum_pressure = np.full(self.n_values, np.nan)
        self.measuring_time = np.full(self.n_values, np.nan)

        self.last_timestamp = 0
        self.module_name = 'Not Available'
        self.module_list = ['Not Available']
        self.set_values = {'HV_voltage': '--', 'LV_voltage': '--', 'LV_current': '--', 'Temp': '--'}
        self.interlock_temp_soft = False
        self.interlock_temp_hard = False
        self.interlock_humid_soft = False
        self.interlock_humid_hard = False
        self.interlock_pressure = False

    def update_arrays(self, data, meta_data):
        self.temp_arrays = np.roll(self.temp_arrays, -1)
        self.temp_arrays[-1] = data[0]

        self.temp_arrays_2 = np.roll(self.temp_arrays_2, -1)
        self.temp_arrays_2[-1] = data[1]

        self.humidity_arrays_chuck = np.roll(self.humidity_arrays_chuck, -1)
        self.humidity_arrays_chuck[-1] = data[2]

        self.humidity_arrays_2 = np.roll(self.humidity_arrays_2, -1)
        self.humidity_arrays_2[-1] = data[3]

        self.dewpoint = np.roll(self.dewpoint, -1)
        self.dewpoint[-1] = data[4]
        self.dewpoint_2 = np.roll(self.dewpoint_2, -1)
        self.dewpoint_2[-1] = data[5]

        self.vin = np.roll(self.vin, -1)
        self.vin[-1] = data[9]

        self.iin = np.roll(self.iin, -1)
        self.iin[-1] = data[10]

        self.hv_v = np.roll(self.hv_v, -1)
        self.hv_v[-1] = data[7]

        self.hv_leak = np.roll(self.hv_leak, -1)
        self.hv_leak[-1] = data[8]

        self.temp_NTC_array = np.roll(self.temp_NTC_array, -1)
        self.temp_NTC_array[-1] = data[6]

        self.vacuum_pressure = np.roll(self.vacuum_pressure, -1)
        self.vacuum_pressure[-1] = data[11]

        self.measuring_time = np.roll(self.measuring_time, -1)
        self.measuring_time[-1] = data[12]

        self.timestamps = np.roll(self.timestamps, -1)
        self.timestamps[-1] = meta_data["timestamp"]
        self.last_timestamp = meta_data["timestamp"]
        self.interlock_temp_soft = meta_data["interlock"].get('temp_soft')
        self.interlock_temp_hard = meta_data["interlock"].get('temp_hard')
        self.interlock_humid_soft = meta_data["interlock"].get('humidity_soft')
        self.interlock_humid_hard = meta_data["interlock"].get('humidity_hard')
        self.interlock_pressure = meta_data["interlock"].get('pressure')
        self.module_name = meta_data.get('module', 'Unknown')
        self.module_list = meta_data.get('module_list', ['Not Available'])
        self.set_values = meta_data.get('set_values', {'HV_voltage': '--', 'LV_voltage': '--', 'LV_current': '--', 'Temp': '--'})
