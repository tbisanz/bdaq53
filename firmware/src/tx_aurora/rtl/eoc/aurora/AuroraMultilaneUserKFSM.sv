
`ifndef AURORA_MULTILANE_USERK_FSM__SV   // include guard
`define AURORA_MULTILANE_USERK_FSM__SV

`timescale 1ns / 1ps

`include "rtl/eoc/aurora/AuroraDefines.sv"

module AuroraMultilaneUserKFSM (
   input wire LaneReady,
   input wire FIFO_Empty,
   input wire [3:0][63:0] FIFO_UserK,
   input wire BlockSent,

   input wire Clk,
   input wire Rst_b,

   output logic FIFO_Read,
   output logic [3:0][63:0] UserKToSend,
   output logic SendBlock);

   enum 				{NOT_READY, NO_DATA, PREPARE_READ, READ_DATA} userk_fsm_state;//synopsys keep_signal_name "userk_fsm_state"
   logic [3:0][63:0] 			userk_word;
   logic 				userk_fill;
   logic 				userk_busy;

   //synopsys sync_set_reset "Rst_b"
   
     always_ff @(posedge Clk) begin
      if (Rst_b == 1'b0) begin
	 userk_fsm_state <= NOT_READY;
	 FIFO_Read <= 1'b0;
	 userk_busy <= 1'b0;
	 userk_fill <= 1'b0;
	 userk_word <= '0;
	 UserKToSend <= '0;
	 SendBlock <= 1'b0;
      end else begin
	 if (BlockSent)
	   SendBlock <= 1'b0;
	 FIFO_Read <= 1'b0;
	 
	 if (LaneReady) begin
	    case(userk_fsm_state)
	      NOT_READY : begin
		 if (FIFO_Empty) begin
		    userk_fsm_state <= NO_DATA;
		 end else begin
		    userk_fsm_state <= PREPARE_READ;
		    FIFO_Read <= 1'b1;
		 end
	      end

	      NO_DATA : begin
		 if (~FIFO_Empty & ~SendBlock) begin
		    userk_fsm_state <= PREPARE_READ;
		    FIFO_Read <= 1'b1;
		 end 
	      end

	      PREPARE_READ : begin
		 // Data will be available at the next cycle
		 userk_fsm_state <= READ_DATA;
	      end	      

	      READ_DATA : begin
		 SendBlock <= 1'b1;
		 UserKToSend <= FIFO_UserK;
		 userk_fsm_state <= NO_DATA;
	      end	      
	    endcase
	 end else begin
	    userk_fsm_state <= NOT_READY;
	 end
      end
   end // always_ff @ (posedge Clk or posedge Rst)

//---------------------------------//Assertions and properties to test//---------------------------------//---------------------------------
  //synopsys translate_off
default clocking @(posedge Clk); endclocking

    property S_DEFAULT  ;
    disable iff (Rst_b)     (Rst_b == 1'b0)                                    |-> ##1 (userk_fsm_state == NOT_READY) ;  
    endproperty 
    
    property S_LANEREADY_drop  ;
    disable iff (Rst_b)     $fell(LaneReady)                                 |-> ##1 (userk_fsm_state == NOT_READY) ;  
    endproperty 
     
    property S_NOT_READY ;
    disable iff (Rst_b == 1'b0) (LaneReady == 1'b1)  ##0 (userk_fsm_state == NOT_READY)  ##0 (FIFO_Empty== 1'b1) |-> ##1 (userk_fsm_state == NO_DATA) ;  
    endproperty  
     
    property S_NOT_READY_ELSE ;
    disable iff (Rst_b == 1'b0) (LaneReady == 1'b1)  ##0 (userk_fsm_state == NOT_READY)  ##0 (FIFO_Empty== 1'b0) |-> ##1 (userk_fsm_state == PREPARE_READ) ;  
    endproperty 
    
    property S_NO_DATA ;
    disable iff (Rst_b == 1'b0)   (userk_fsm_state == NO_DATA)  ##0 (FIFO_Empty== 1'b0) ##0 (~SendBlock) |-> ##1 (userk_fsm_state == PREPARE_READ) ;  
    endproperty 
    
    property S_NO_DATA_ELSE ;
    disable iff (Rst_b == 1'b0)   (userk_fsm_state == NO_DATA)  ##0 (FIFO_Empty== 1'b1 | SendBlock) |-> (userk_fsm_state == NO_DATA) ;  
    endproperty 
     
    property S_PREPARE_READ  ;
    disable iff (Rst_b == 1'b0)   (userk_fsm_state == PREPARE_READ) |-> ##1 (userk_fsm_state == READ_DATA) ; 
    endproperty                       
      
    property S_READ_DATA  ;
    disable iff (Rst_b == 1'b0)   (userk_fsm_state == READ_DATA) |-> ##1 (userk_fsm_state == NO_DATA) ;  
    endproperty                        
  
//---------------------------------//---------------------------------//---------------------------------//---------------------------------
 `ifdef SVA_EN
   generate
    begin : SVA_MLUSERK_FSM     
     ERR_S_DEFAULT_STATE    : assert property (S_DEFAULT); // Reset msut get the state machine back to default "NOT_READY" in here      
     ERR_S_LANE_READY       : assert property (S_LANEREADY_drop); // Any time LANE_READY signal dropped it must go to NOT_READY state     
     ERR_S_WAIT_READY       : assert property (S_NOT_READY); // Lane is ready abut fifo is empty so move to NO_DATA
     ERR_S_WAIT_READY_ElSE  : assert property (S_NOT_READY_ELSE); // Fifo is not empty , trnasition to PREPARE_READ
     ERR_S_NO_DATA          : assert property (S_NO_DATA);  // Data available (fifo not empty) move to PREPARE_READ
     ERR_S_NO_DATA_ELSE     : assert property (S_NO_DATA_ELSE); // Stay in NO_DATA as long as fifo is empty and lane ready. 
     ERR_S_PREPARE_READ     : assert property (S_PREPARE_READ); // From PREPARE_READ to READ_DATA in one cycle    
     ERR_S_READ_DATA        : assert property (S_READ_DATA); // while reading data is fifo is empty move to NO_DATA    
//---------------------------------//---------------------------------//---------------------------------//---------------------------------
   end
   endgenerate
  `endif
  //synopsys translate_on


endmodule : AuroraMultilaneUserKFSM

`endif

