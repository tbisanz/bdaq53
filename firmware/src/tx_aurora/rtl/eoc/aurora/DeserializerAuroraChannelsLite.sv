`ifndef DESERIALIZER_AURORA_CHANNELS_LITE__SV
 `define DESERIALIZER_AURORA_CHANNELS_LITE__SV

`include "rtl/eoc/aurora/DeserializerAuroraBonder.sv"
`include "rtl/eoc/aurora/DeserializerAuroraLane.sv"
`include "rtl/eoc/aurora/DeserializerShiftRTap.sv"
`include "rtl/eoc/aurora/DeserializerFindCB.sv"


module DeserializerAuroraChannelsLite
   (
    input wire 		  Reset_b,
    input wire 		  ResetDes_b,
    input wire 		  ClkDes,
    input wire [3:0][3:0] Q [4],
    input wire [3:0][1:0] SwitchSel,
    
    input wire [3:0] 	  ActiveLanes,
    input wire 		  Bonding, 
    input wire 		  ClkAurora,
    output logic 	  ChannelUp,

    output logic 	  Error,
			  AuroraDataInterface.out DataOutput[4],
			  AuroraDataInterface.out MonitorOutput[4],
			  
			  AuroraDataInterface.out BondedDataOutput,
			  AuroraDataInterface.out BondedMonitorOutput
    );
   


   logic [65:0] 	  blockData [4];
   logic 		  blockValid [4];
   logic 		  blockRead [4];

   logic [65:0] 	  blockData_preswitch [4];
   logic 		  blockValid_preswitch [4];
   logic 		  blockRead_preswitch [4];
   
   logic [4:0] 		  laneUp;
   logic [4:0] 		  laneUp_preswitch;
   
   logic [131:0]      bondedData;
   logic 	      bondedValid;

   logic  	      isData  [4];
   logic  	      isKWord [4];
   
   logic [1:0] 	      isBondedData;
   logic [1:0] 	      isBondedKWord;
   
   logic [3:0] 	      numBondedKWord [2];
   logic [3:0] 	      numKWord [4];
   
   logic 	      ClkBonding;
   logic 	      ClkLane [4];
   logic 	      ClkLaneOut [4];

//   AuroraDataInterface intDataOutput [4] ( );
//   AuroraDataInterface intMonitorOutput [4] ( );
   genvar 		      channel;

   // Clock Gating
   //
   // remove clock gating
   /*
   
   assign ClkBonding = (Bonding | ~Reset_b) ? ClkAurora : 1'b0;

   for (channel = 0; channel < 2; channel++)
     begin
	assign ClkLane[channel] = (ActiveLanes[channel] | Bonding | ~Reset_b) ? ClkAurora : 1'b0;
	assign ClkLaneOut[channel] = (ActiveLanes[channel] | ~Reset_b) ? ClkAurora : 1'b0;
     end

   for (channel = 2; channel < 4; channel++)
     begin
	assign ClkLane[channel] = (ActiveLanes[channel] | ~Reset_b) ? ClkAurora : 1'b0;
	assign ClkLaneOut[channel] = (ActiveLanes[channel] | ~Reset_b) ? ClkAurora : 1'b0;
     end
    */

   for (channel = 0; channel < 4; channel++) begin
	assign ClkLane[channel] = ClkAurora;
	assign ClkLaneOut[channel] = ClkAurora;      
   end
   assign ClkBonding = ClkAurora;

   SwitchMatrixUnpacked #(.IOS(4), .WIDTH(66)) SwitchData
     (.Sel(SwitchSel),
      .In(blockData_preswitch),
      .Out(blockData)
      );
   SwitchMatrixUnpacked #(.IOS(4), .WIDTH(1)) SwitchValid
     (.Sel(SwitchSel),
      .In(blockValid_preswitch),
      .Out(blockValid)
      );
   SwitchMatrixUnpacked #(.IOS(4), .WIDTH(1)) SwitchRead
     (.Sel(SwitchSel),
      .In(blockRead_preswitch),
      .Out(blockRead)
      );
   
   SwitchMatrix #(.IOS(4), .WIDTH(1)) SwitchLaneUp
     (.Sel(SwitchSel),
      .In(laneUp_preswitch),
      .Out(laneUp)
      );
   
   generate
      for (channel = 0; channel < 4; channel++) begin
	 DeserializerAuroraLane LaneUnit
		     (
		      .Reset_b(Reset_b),
		      .ResetDes_b(ResetDes_b),
		      .ClkDes(ClkDes),
		      .Q(Q[channel]),
		      .ClkAurora(ClkLane[channel]),
		      .LaneUp(laneUp_preswitch[channel]),
		      .BlockOut(blockData_preswitch[channel]),
		      .BlockValid(blockValid_preswitch[channel])
		      );
	 assign isData[channel]  = blockValid[channel] & (blockData[channel][65-:2] == 2'b01) & Reset_b & ActiveLanes[channel];
	 logic [0:7] local_reverted_btf;	 
	 assign local_reverted_btf = blockData[channel][63-:8];

	   always_comb
	     begin
		case(local_reverted_btf)
		  8'hD2: numKWord[channel] = 4'd0;
		  8'h99: numKWord[channel] = 4'd1;
		  8'h55: numKWord[channel] = 4'd2;
		  8'hB4: numKWord[channel] = 4'd3;
		  8'hCC: numKWord[channel] = 4'd4;
		  default: numKWord[channel] = 4'b1111;	  
		endcase // case (AuroraBlock[9:2]	      
	     end

	 assign isKWord[channel]  = blockValid[channel] & (blockData[channel][65-:2] == 2'b10) & (numKWord[channel] != 4'b1111) & Reset_b & ActiveLanes[channel];

	 wire unconnected_wfull;
   //
	 OutputCdcFifoResetB #( .DSIZE((64*4)+(2*4)), .ASIZE(3) ) OutputCdcFifoData 
	   (
	    .rempty ( DataOutput[channel].DataEOC_empty                           ),
	    .wdata  ( {8'h03, {192{1'b0}}, blockData[channel][0+:64]}             ),
	    .winc   ( isData[channel] & ActiveLanes[channel]                      ),
	    .wclk   ( ClkLaneOut[channel]                                         ),
	    .wrst_b ( Reset_b                                                     ),
	    .rdata  ( {DataOutput[channel].DataMask, DataOutput[channel].DataEOC} ), 
	    .wfull  ( unconnected_wfull                                           ),
	    .rinc   ( DataOutput[channel].DataEOC_read                            ),
	    .rclk   ( ClkLaneOut[channel]                                         ),
	    .rrst_b ( Reset_b                                                     )  
	    ) ;

	 OutputCdcFifoResetB #( .DSIZE(64*4), .ASIZE(2) ) OutputCdcFifoMonitor 
	   (
            .rempty ( MonitorOutput[channel].DataEOC_empty ),
            .wdata  ( {blockData[channel][0+:64],blockData[channel][0+:64],blockData[channel][0+:64],blockData[channel][0+:64]} ),
            .winc   ( isKWord[channel] ),
            .wclk   ( ClkLaneOut[channel] ),
            .wrst_b   ( Reset_b ),
            .rdata  ( MonitorOutput[channel].DataEOC ), 
            .wfull  ( ),
            .rinc   ( MonitorOutput[channel].DataEOC_read ),
            .rclk   ( ClkLaneOut[channel] ),
            .rrst_b   ( Reset_b )  
	    ) ;
      end
   endgenerate


`ifdef FAST_FLOW
  assign BondedDataOutput.DataEOC_empty = 0;
  assign BondedMonitorOutput.DataEOC_empty = 0;
  assign BondedDataOutput.DataMask = '0;
  assign BondedDataOutput.DataEOC = '0;
  assign BondedMonitorOutput.DataMask = '0;
  assign BondedMonitorOutput.DataEOC = '0;
`else

   logic unconected_blockread [2];
   DeserializerAuroraBonder #(.CHANNELS(2)) Bonder
     (
      .Reset_b(Reset_b),
      .Clk(ClkBonding),
      .ActiveLanes(2'b11),
      .LaneUp(laneUp[0+:2]),
      .BlockData(blockData[0+:2]),
      .BlockValid(blockValid[0+:2]),
      .BondedData(bondedData),
      .BondedValid(bondedValid),
      .BondedChannel(ChannelUp)
      );

   logic [7:0] bondedMask;
   logic       writeData;
   logic       writeMonitor;
   logic [255:0] strippedData;
   logic [0:7] 	 reverted_btf [2];
   

   generate
      for (channel = 0; channel < 2; channel ++)
	begin
	   assign isBondedData[channel]  = bondedValid ? (bondedData[((channel*66)+65)-:2] == 2'b01) : 1'b0;
	 

	   assign reverted_btf[channel] = bondedData[((channel*66)+63)-:8];

	   always_comb
	     begin
		case(reverted_btf[channel])
		  8'hD2: numBondedKWord[channel] = 4'd0;
		  8'h99: numBondedKWord[channel] = 4'd1;
		  8'h55: numBondedKWord[channel] = 4'd2;
		  8'hB4: numBondedKWord[channel] = 4'd3;
		  8'hCC: numBondedKWord[channel] = 4'd4;
		  default: numBondedKWord[channel] = 4'b1111;	  
		endcase // case (AuroraBlock[9:2]	      
	     end

	 
	   assign isBondedKWord[channel] = (bondedData[((channel*66)+65)-:2] == 2'b10) && 
				   (numBondedKWord[channel] != 4'b1111) && 
				   bondedValid;
	 
	   assign bondedMask[(channel*2)+:2] = isBondedData[channel] ? 2'b11 : 2'b00;
	   assign strippedData[(64*channel)+:64] = bondedData[((66*channel))+:64];	   
	end
   endgenerate

   // Fill the rest of the word accordingly, we could pack two 128 data here?
   
   generate
      for (channel = 2; channel < 4; channel++) begin
	 assign bondedMask[(channel*2)+:2] = 2'b00;
	 assign strippedData[(64*channel)+:64] = isBondedKWord[channel-2] ? strippedData[(64*(channel-2))+:64] : '0;
      end
   endgenerate

   // CDC FIFO reads
   generate
      for (channel = 0; channel < 4; channel++) begin
	 always_ff @(posedge ClkLane[channel])
	   if (Reset_b == 1'b0)
	     blockRead_preswitch[channel] <= 1'b0;
	   else
	     blockRead_preswitch[channel] <= 1'b1;
      end
   endgenerate

   assign writeData = (| (isBondedData)) & Bonding;
   assign writeMonitor = (| (isBondedKWord)) & Bonding;
   
/* -----\/----- EXCLUDED -----\/-----
   EocMemFifoResetB #( .DSIZE((64*4)+(2*4)), .ASIZE(2), .TMR_MSB_SIZE(1) ) OutputFifoBondedData (
        .Empty ( BondedDataOutput.DataEOC_empty ),
        .DataWr  ( {bondedMask, strippedData} ),
        .EnWr   ( writeData ),
        .Clk   ( ClkBonding ),
        .Reset_b   ( Reset_b ),
        .DataRd  ( {BondedDataOutput.DataMask, BondedDataOutput.DataEOC} ), 
        .Full  ( ),
        .EnRd   ( BondedDataOutput.DataEOC_read )

    ) ;

   EocMemFifoResetB #( .DSIZE(64*4), .ASIZE(2), .TMR_MSB_SIZE(1) ) OutputFifoBondedMonitor (
        .Empty ( BondedMonitorOutput.DataEOC_empty ),
        .DataWr  ( strippedData ),
        .EnWr   ( writeMonitor ),
        .Clk   ( ClkBonding ),
        .Reset_b   ( Reset_b ),
        .DataRd  ( BondedMonitorOutput.DataEOC ), 
        .Full  ( ),
        .EnRd   ( BondedMonitorOutput.DataEOC_read )

    ) ;
 -----/\----- EXCLUDED -----/\----- */
   OutputCdcFifoResetB #( .DSIZE((64*4)+(2*4)), .ASIZE(3) ) OutputCdcFifoBondedData (
        .rempty ( BondedDataOutput.DataEOC_empty ),
        .wdata  ( {bondedMask, strippedData} ),
        .winc   ( writeData ),
        .wclk   ( ClkBonding ),
        .wrst_b   ( Reset_b ),
        .rdata  ( {BondedDataOutput.DataMask, BondedDataOutput.DataEOC} ), 
        .wfull  ( ),
        .rinc   ( BondedDataOutput.DataEOC_read ),
        .rclk   ( ClkBonding ),
        .rrst_b   ( Reset_b )  

    ) ;

   OutputCdcFifoResetB #( .DSIZE(64*4), .ASIZE(2) ) OutputCdcFifoBondedMonitor (
        .rempty ( BondedMonitorOutput.DataEOC_empty ),
        .wdata  ( strippedData ),
        .winc   ( writeMonitor ),
        .wclk   ( ClkBonding ),
        .wrst_b   ( Reset_b ),
        .rdata  ( BondedMonitorOutput.DataEOC ), 
        .wfull  ( ),
        .rinc   ( BondedMonitorOutput.DataEOC_read ),
        .rclk   ( ClkBonding ),
        .rrst_b   ( Reset_b )  

    ) ;

`endif

endmodule // DeserializerAuroraChannel

`endif
