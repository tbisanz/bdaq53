`ifndef MERGER_UPSCALE_BLOCKS
 `define MERGER_UPSCALE_BLOCKS

module MergerUpscaleBlocks (
			    input wire Reset_b,
			    input wire Clock,
			    input wire [3:0] ActiveLanes,
			    input wire [63:0] DataIn,
			    input wire DataValidIn,

			    output logic [7:0][31:0] DataOut,
			    output logic [7:0] DataMask,
			    output logic DataEmpty,
			    input wire DataRead
			    );

   enum logic [2:0] {RESET, EMPTY, FILL0, FILL1, FILL2, FILL3} state;

   logic empty;

   logic [7:0][31:0] upscaled_data;
   logic 	    upscaled_valid;
   logic [7:0] 	    upscaled_mask;
   
   

   OutputCdcFifo #( .DSIZE(264), .ASIZE(5) ) OutputCdcFifoAurora (
        .rempty ( DataEmpty ),
        .wdata  ( {upscaled_mask, upscaled_data} ),
        .winc   ( upscaled_valid ),
        .wclk   ( Clock ),
        .wrst   ( Reset_b ),
        .rdata  ( {DataMask, DataOut} ), 
        .wfull  ( ),
        .rinc   ( DataRead ),
        .rclk   ( Clock ),
        .rrst   ( Reset_b )  

    ) ;

   
   always_ff @(posedge Clock)
     begin
	if (Reset_b == 1'b0) begin
	   state <= RESET;
	   upscaled_valid <= 1'b0;
	   upscaled_mask <= 8'b00000000;
	   upscaled_data[0] <= 32'h00000000;
	   upscaled_data[1] <= 32'h00000000;
	   upscaled_data[2] <= 32'h00000000;
	   upscaled_data[3] <= 32'h00000000;	       
	   upscaled_data[4] <= 32'h00000000;
	   upscaled_data[5] <= 32'h00000000;
	   upscaled_data[6] <= 32'h00000000;
	   upscaled_data[7] <= 32'h00000000;	       
	end else
	  case(state)
	    RESET: begin
	       empty <= 1'b1;
	       upscaled_valid <= 1'b0;
	       upscaled_mask <= 8'b00000000;
	       upscaled_data[0] <= 32'h00000000;
	       upscaled_data[1] <= 32'h00000000;
	       upscaled_data[2] <= 32'h00000000;
	       upscaled_data[3] <= 32'h00000000;	       
	       upscaled_data[4] <= 32'h00000000;
	       upscaled_data[5] <= 32'h00000000;
	       upscaled_data[6] <= 32'h00000000;
	       upscaled_data[7] <= 32'h00000000;	       
	       if (ActiveLanes[0] == 1'b1) 
		 state <= FILL0;
	       else if (ActiveLanes[1] == 1'b1) 
		 state <= FILL1;
	       else if (ActiveLanes[2] == 1'b1)
		 state <= FILL2;
	       else if (ActiveLanes[3] == 1'b1)
		 state <= FILL3;
	    end // case: RESET

	    FILL0: begin
	       upscaled_valid <= 1'b0;
	       upscaled_mask <= 8'b00000000;
	       if (DataValidIn == 1'b1) begin
		  upscaled_mask[1:0] <= 2'b11;
		  upscaled_data[1:0] <= DataIn;
		  if ((|ActiveLanes[3:1]) == 1'b0) begin
		     upscaled_valid <= 1'b1;
		  end else if (ActiveLanes[1] == 1'b1) 
		    state <= FILL1;
		  else if (ActiveLanes[2] == 1'b1)
		    state <= FILL2;
		  else if (ActiveLanes[3] == 1'b1)
		    state <= FILL3;
	       end 	       
	    end

	    FILL1: begin
	       upscaled_mask[7:2] <= 6'b000000;
	       upscaled_valid <= 1'b0;
	       if (DataValidIn == 1'b1) begin
		  upscaled_data[3:2] <= DataIn;
		  upscaled_mask[3:2] <= 2'b11;
		  if ((|ActiveLanes[3:2]) == 1'b0) begin
		     upscaled_valid <= 1'b1;
		     if (ActiveLanes[0] == 1'b1)
		       state <= FILL0;
		  end else if (ActiveLanes[2] == 1'b1)
		    state <= FILL2;
		  else if (ActiveLanes[3] == 1'b1)
		    state <= FILL3;
	       end else if (upscaled_mask[1:0] == 2'b11) begin // if (DataValidIn == 1'b1)
		  upscaled_valid <= 1'b1;
		  state <= FILL0;
	       end
		 
	    end

	    FILL2: begin
	       upscaled_valid <= 1'b0;
	       upscaled_mask[7:4] <= 4'b0000;
	       if (DataValidIn == 1'b1) begin
		  upscaled_data[5:4] <= DataIn;
		  upscaled_mask[5:4] <= 2'b11;		
		  if ((|ActiveLanes[3]) == 1'b0) begin
		     upscaled_valid <= 1'b1;
		     if (ActiveLanes[0] == 1'b1)
		       state <= FILL0;
		     else if (ActiveLanes[1] == 1'b1)
		       state <= FILL1;
		  end else if (ActiveLanes[3] == 1'b1)
		    state <= FILL3;
	       end else if ((|upscaled_mask[3:0]) == 1'b1) begin // if (DataValidIn == 1'b1)
		  upscaled_valid <= 1'b1;
		  if (ActiveLanes[0] == 1'b1)
		    state <= FILL0;
		  else if (ActiveLanes[1] == 1'b1)
		    state <= FILL1;
	       end
	    end

	    FILL3: begin
	       upscaled_valid <= 1'b0;
	       upscaled_mask[7:6] <= 2'b00;
	       if (DataValidIn == 1'b1) begin
		  upscaled_data[7:6] <= DataIn;
		  upscaled_mask[7:6] <= 2'b11;
		  upscaled_valid <= 1'b1;
		  if (ActiveLanes[0] == 1'b1)
		    state <= FILL0;
		  else if (ActiveLanes[1] == 1'b1)
		    state <= FILL1;
		  else if (ActiveLanes[2] == 1'b1)
		    state <= FILL2;
	       end else if ((|upscaled_mask[5:0]) == 1'b1) begin // if (DataValidIn == 1'b1)
		  upscaled_valid <= 1'b1;
		  if (ActiveLanes[0] == 1'b1)
		    state <= FILL0;
		  else if (ActiveLanes[1] == 1'b1)
		    state <= FILL1;
		  else if (ActiveLanes[2] == 1'b1)
		    state <= FILL2;
	       end
	    end
	    
	  endcase // case (state)
     end

endmodule // MergerUpscaleBlocks

`endif
